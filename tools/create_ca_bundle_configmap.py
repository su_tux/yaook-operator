#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import certifi
import typing
import yaml
import argparse


def read_cert_file(cf):
    lines: typing.List[str] = []
    certs = set()

    with open(cf) as f:
        for line in f:
            line = line.strip()
            if line.startswith("#"):
                continue
            if line == "-----BEGIN CERTIFICATE-----":
                lines = []
            lines.append(line)
            if line == "-----END CERTIFICATE-----":
                certs.add('\n'.join(lines))

    return '\n'.join(sorted(certs))


parser = argparse.ArgumentParser(description='Output a yaml manifest for a '
                                             'Kubernetes ConfigMap which '
                                             'contains a whole CA certificate '
                                             'bundle appended by a custom CA '
                                             'certificate')

parser.add_argument('configmap_name', type=str,
                    help='The name of the Kubernetes ConfigMap resource')

parser.add_argument('ca_certificate', type=open,
                    help='A certificate file which is appended to the '
                    'certificate bundle from certifi')

args = parser.parse_args()

# Certifi provides us with the Mozilla root CA list
output = {"ca-bundle.crt": read_cert_file(certifi.where()) +
          '\n' + read_cert_file(args.ca_certificate.name)}

config = {
    "apiVersion": "v1",
    "kind": "ConfigMap",
    "metadata": {"name": args.configmap_name},
    "immutable": True,
    "data": output,
}

print(yaml.dump(config))
