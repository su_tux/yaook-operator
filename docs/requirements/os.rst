.. _requirements.os:

OS Requirements
########################

Realtime Scheduling
===================

The cluster SHOULD run a kernel configured **without**
``CONFIG_RT_GROUP_SCHED``. The cluster MUST run with a kernel configured
**without** ``CONFIG_RT_GROUP_SCHED`` or with the
``kernel.sched_rt_runtime_us`` sysctl set to ``-1``.

Setting the ``kernel.sched_rt_runtime_us`` sysctl to ``-1`` is NOT RECOMMENDED
for production use.

For more discussion on this see
`issue #121 <https://gitlab.com/yaook/operator/-/issues/121>`_.

While docker supports using realtime features on systems with
``CONFIG_RT_GROUP_SCHED`` and a non-``-1`` value for
``kernel.sched_rt_runtime_us``, Kubernetes does currently not support that.
Once support for that lands, we may increase our k8s version requirements in
favour of this requirement.

Below, we describe how to :ref:`check<realtime_scheduling_check>` whether your os fulfills this requirement and, if it does not, :ref:`how to get around it<realtime_scheduling_workaround>`.


.. _realtime_scheduling_check:

Check
-----------------------

You can check whether you are running a kernel built with ``CONFIG_RT_GROUP_SCHED`` enabled by executing:

.. code-block:: bash

    grep CONFIG_RT_GROUP_SCHED /boot/config-$(uname -r)

.. _realtime_scheduling_workaround:

Workaround
-----------------------

You can run the following daemon as a workaround for this problem.

.. code-block:: bash

    kubectl -n $YAOOK_OP_NAMESPACE apply -f ci/devel_integration_tests/deploy/realtime-hack.yaml

