##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -e -x

#Set global variables. Feel free to adjust them to your needs
export NAMESPACE=$YAOOK_OP_NAMESPACE
export MYPYPATH="$(pwd)/.local/mypy-stubs"

#Install requirements
pip install -r requirements-build.txt
pip install -e .

stubgen -p kubernetes_asyncio -o "$MYPYPATH"

# Generate default policy files for the openstack components.
# TODO: For now fixed default policy files for queens are used
# under yaook/op/$os_component/static/default_policies.yaml.
# As soon as we support deploying other releases of openstack
# the next line should be commented in and
# the location for the default policy files will be
# yaook/op/$os_component/generated/default_policies.yaml.
# ./generate_default_policies.py

#Generate all generated data like CRDs, operator-deployments and cuelang schema
make all

# Create namespace if not exists
kubectl get namespace $NAMESPACE 2>/dev/null || kubectl create namespace $NAMESPACE

# Deploy helm charts for central services
helm repo add stable https://charts.helm.sh/stable
helm repo update

# Deploy custom resources, roles for operators, openstack service deployments
HELMFLAGS="--namespace $NAMESPACE --set operator.pythonOptimize=false --set operator.image.tag=devel"
helm upgrade --install --namespace $NAMESPACE yaook-crds ./yaook/helm_builder/Charts/crds

