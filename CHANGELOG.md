# Changelog

All notable changes to this project will be documented in this file.

## [release-20230504 - now]
### Changed
- We introduced network.yaook.cloud/neutron-bgp-agent as label to schedule neutron-ovn-agents separately from neutron network nodes(network.yaook.cloud/neutron-ovn-agent).
  **ovn-bgp-agents will be deleted if no action is taken:** The node label needs to be added, where you want to run bgp agents before updating.
