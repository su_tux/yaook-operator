##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ "%s-db" | format(labels["state.yaook.cloud/parent-name"]) }}
spec:
  replicas: {{ crd_spec.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  podManagementPolicy: OrderedReady
  serviceName: {{ dependencies['unready_service'].resource_name() }}
  template:
    metadata:
      annotations:
        operator.yaook.cloud/replication-cert-not-before: {{ vars.replication_cert_not_before }}
        operator.yaook.cloud/frontend-cert-not-before: {{ vars.frontend_cert_not_before }}
        operator.yaook.cloud/exporter-cert-not-before: {{ vars.exporter_cert_not_before }}
      labels: {{ pod_labels }}
    spec:
      automountServiceAccountToken: false
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
      securityContext:
        # we might want to choose this one randomly per cluster in a future
        # version
        fsGroup: 2500010
      shareProcessNamespace: true
      # set here as we already sleep for 60 in the lifecycle hook below
      # therefore the normal 30 seconds are not enough
      terminationGracePeriodSeconds: 180
      containers:
        - name: mariadb-galera
          image: {{ versioned_dependencies['mariadb_image'] }}
          imagePullPolicy: IfNotPresent
          command:
            - setpriv
            # raise CAP_SYS_NICE in ambient to control galera thread scheduling
            - "--ambient-caps"
            - "+sys_nice"
            # drop UID/GID to non-privileged one
            - "--reuid"
            - "2500010"
            # this is what k8s does with runAsUser and fsGroup. not ideal, but
            # the image expects to run with gid 0 for some tempfiles.
            - "--regid"
            - "0"
            - "--groups"
            - "2500010"
            # drop all capabilities except SYS_NICE
            # we cannot use `-all` here because that requires a libcap-ng
            # matching the host kernel, which we cannot guarantee; however, as
            # we let docker drop everything else, the explicit list here is
            # safe.
            - "--bounding-set"
            - "-setuid,-setgid,-setpcap,+sys_nice"
            - "--inh-caps"
            - "-setuid,-setgid,-setpcap,+sys_nice"
            - "--"
            # Ready, set, go!
            - "bash"
            - -ec
            - |
              set -xeuo pipefail
              myname="$(hostname -f)"
              myservice="$(echo "$myname" | cut -d'.' -f2-)"
              echo "checking kubedns to ensure IST will be working"
              for i in `seq 1 10`; do
                getent hosts "$myservice." | grep "$MY_POD_IP"
              done
              peers=()
              while IFS= read -r peer; do
                peer_ip="$(echo "$peer" | awk '{print $1}')"
                peer_hostname="$(getent hosts "$peer_ip" | awk '{print $2}')"
                if [ "$peer_hostname" != "$myname" ]; then
                  peers+=( "$peer_hostname" )
                fi
              done <<< "$(getent hosts "$myservice.")"
              peers_str="$(printf ',%s' "${peers[@]}")"
              addr="$(printf 'gcomm://%s' "${peers_str:1}")"
              export MARIADB_GALERA_CLUSTER_ADDRESS="$addr"
              export MARIADB_GALERA_NODE_NAME="$myname"
              export MARIADB_EXTRA_FLAGS="--wsrep-sst-receive-address=$myname"
              setpriv --dump
              exec /opt/bitnami/scripts/mariadb-galera/entrypoint.sh /opt/bitnami/scripts/mariadb-galera/run.sh
          securityContext:
            runAsUser: 0
            allowPrivilegeEscalation: false
            capabilities:
              add:
              # Required to use realtime round-robin scheduling for the galera
              # replication threads.
              - SYS_NICE
              # Required to raise SYS_NICE to ambient caps
              - SETPCAP
              # Required to change user ID to drop further FS privileges
              - SETUID
              - SETGID
              drop:
              # That’s all, kthxbai.
              - all
          env:
            - name: MY_POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: MY_POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: BITNAMI_DEBUG
              value: "false"
            - name: MARIADB_GALERA_CLUSTER_NAME
              value: {{ labels["state.yaook.cloud/parent-name"] }}
            - name: MARIADB_ROOT_USER
              value: {{ params.mariadb_root_username }}
            - name: MARIADB_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ dependencies['passwords'].resource_name() }}
                  key: mariadb-root-password
            - name: MARIADB_DATABASE
              value: {{ crd_spec.database }}
            - name: MARIADB_GALERA_MARIABACKUP_USER
              value: "mariabackup"
            - name: MARIADB_GALERA_MARIABACKUP_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ dependencies['passwords'].resource_name() }}
                  key: mariadb-galera-mariabackup-password
            - name: MARIADB_UPGRADE
              value: {{ versioned_dependencies['mariadb_upgrade'] }}
          ports:
            - name: mysql
              containerPort: 3306
              protocol: TCP
            - name: galera
              containerPort: 4567
              protocol: TCP
            - name: ist
              containerPort: 4568
              protocol: TCP
            - name: sst
              containerPort: 4444
              protocol: TCP
          livenessProbe:
            exec:
              command:
                - bash
                - -ec
                - |
                  exec mysqladmin status -u$MARIADB_ROOT_USER -p$MARIADB_ROOT_PASSWORD
            periodSeconds: 10
            timeoutSeconds: 1
            successThreshold: 1
            failureThreshold: 3
          readinessProbe:
            exec:
              command:
                - bash
                - -ec
                - |
                  exec mysqladmin status -u$MARIADB_ROOT_USER -p$MARIADB_ROOT_PASSWORD
            periodSeconds: 10
            timeoutSeconds: 1
            successThreshold: 1
            failureThreshold: 3
          startupProbe:
            exec:
              command:
                - bash
                - -ec
                # The setpriv part is stolen from above. Make sure that both
                # are adjusted if there should be changes!
                - >-
                  mysqladmin status -u$MARIADB_ROOT_USER -p$MARIADB_ROOT_PASSWORD &&
                  setpriv
                  --ambient-caps
                  +sys_nice
                  --reuid
                  2500010
                  --regid
                  0
                  --groups
                  2500010
                  --bounding-set
                  -setuid,-setgid,-setpcap,+sys_nice
                  --inh-caps
                  -setuid,-setgid,-setpcap,+sys_nice
                  --
                  $MARIADB_UPGRADE --skip-write-binlog -u$MARIADB_ROOT_USER -p$MARIADB_ROOT_PASSWORD
            periodSeconds: 10
            timeoutSeconds: 120
            successThreshold: 1
            failureThreshold: 20
          lifecycle:
            preStop:
              exec:
                command:
                - /bin/sh
                - -c
                # 60 seconds is 2x the dns hold time of haproxy dns resolution (10s)
                # + 30 seconds of ttl in coredns (default) + a small buffer
                #
                # So haproxy has plenty of time to notice that mysqld is
                # terminating before it is actually gone. This allows us to
                # have downtime free rollouts
                # NOTE: if you change the time, you also need to update
                # terminationGracePeriodSeconds
                - sleep 60; mysqladmin -u$MARIADB_ROOT_USER -p$MARIADB_ROOT_PASSWORD shutdown
          volumeMounts:
            - name: previous-boot
              mountPath: /opt/bitnami/mariadb/.bootstrap
            - name: data
              mountPath: /bitnami/mariadb
            - name: config
              mountPath: /opt/bitnami/mariadb/conf/my.cnf
              subPath: my.cnf
            - name: certificates
              mountPath: /bitnami/mariadb/certs
            - name: run
              mountPath: /opt/bitnami/mariadb/tmp/
          resources: {{ crd_spec | resources('mariadb-galera') }}
        - name: backup-creator
          image: {{ versioned_dependencies['backup_creator_image'] }}
          imagePullPolicy: Always
          env:
            - name: YAOOK_BACKUP_CREATOR_POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            # We are using the root user here instead of the backup user
            # as the backup user does not have the necessary permissions for
            # a mysqldump.
            - name: YAOOK_BACKUP_CREATOR_MARIADB_USERNAME
              value: {{ params.mariadb_root_username }}
            - name: YAOOK_BACKUP_CREATOR_MARIADB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ dependencies['passwords'].resource_name() }}
                  key: mariadb-root-password
            - name: YAOOK_BACKUP_CREATOR_MARIADB_SOCKET
              value: /opt/bitnami/mariadb/tmp/mysql.sock
            - name: YAOOK_BACKUP_CREATOR_MARIADB_DATABASE
              value: {{ crd_spec.database }}
            - name: YAOOK_BACKUP_CREATOR_MARIADB_MYSQLDUMP
              value: {{ crd_spec.backup.mysqldump | string }}
            - name: YAOOK_BACKUP_CREATOR_BACKUP_MODULE_NAME
              value: backup_creator.services.mariadbservice
            - name: YAOOK_BACKUP_CREATOR_OUT_PATH
              value: /backup/new
            - name: YAOOK_BACKUP_CREATOR_SCHEDULE
              value: {{ crd_spec.backup.schedule }}
          volumeMounts:
            - name: backup-scratch
              mountPath: /backup
            - name: data
              mountPath: /bitnami/mariadb
            - name: config
              mountPath: /opt/bitnami/mariadb/conf/my.cnf
              subPath: my.cnf
            - name: certificates
              mountPath: /bitnami/mariadb/certs
            - name: run
              mountPath: /opt/bitnami/mariadb/tmp/
          resources: {{ crd_spec | resources('backup-creator') }}
        - name: backup-shifter
          image: {{ versioned_dependencies['backup_shifter_image'] }}
          imagePullPolicy: Always
          env:
            - name: YAOOK_BACKUP_SHIFTER_WORK_DIR
              value: /backup
            - name: YAOOK_BACKUP_SHIFTER_SHIFTERS
{% if not (crd_spec.backup | default(False) and crd_spec.backup.targets | default(False)) %}
              value: dumpinfo
{% else %}
              value: dumpinfo,{% if crd_spec.backup.targets.s3 | default(False) %}s3_upload{% endif %}
{% if crd_spec.backup.targets.s3 | default(False) %}
            - name: YAOOK_BACKUP_SHIFTER_S3_URL
              value: {{ crd_spec.backup.targets.s3.endpoint }}
            - name: YAOOK_BACKUP_SHIFTER_S3_BUCKET
              value: {{ crd_spec.backup.targets.s3.bucket }}
            - name: YAOOK_BACKUP_SHIFTER_S3_ACCESS_KEY
              valueFrom:
                secretKeyRef:
                  name: {{ crd_spec.backup.targets.s3.credentialRef.name }}
                  key: access
            - name: YAOOK_BACKUP_SHIFTER_S3_SECRET_KEY
              valueFrom:
                secretKeyRef:
                  name: {{ crd_spec.backup.targets.s3.credentialRef.name }}
                  key: secret
            - name: YAOOK_BACKUP_SHIFTER_S3_ADDRESSING_STYLE
              value: {{ crd_spec.backup.targets.s3.addressingStyle }}
            - name: YAOOK_BACKUP_SHIFTER_S3_CACERT
              value: /certificates/ca-bundle.crt
{% if crd_spec.backup.targets.s3.filePrefix %}
            - name: YAOOK_BACKUP_SHIFTER_S3_FILE_PREFIX
              value: {{ crd_spec.backup.targets.s3.filePrefix }}
{% endif %}
{% endif %}
{% endif %}
          volumeMounts:
            - name: backup-scratch
              mountPath: /backup
            - name: ca-certs
              mountPath: /certificates
          resources: {{ crd_spec | resources('backup-shifter') }}
        - name: mysqld-exporter
          image: {{ versioned_dependencies['mysqld_exporter_image'] }}
          imagePullPolicy: Always
          args:
            - "--web.config.file=/config/mysql-exporter.yaml"
          env:
            - name: DATA_SOURCE_NAME
              valueFrom:
                secretKeyRef:
                  name: {{ dependencies['passwords'].resource_name() }}
                  key: exporter_data_source_name
          ports:
            - name: prometheus
              containerPort: 9104
              protocol: TCP
          volumeMounts:
            - name: certificates
              mountPath: /certs
            - name: exporter-config
              mountPath: /config
          resources: {{ crd_spec | resources('mysqld-exporter') }}
        - name: "ssl-terminator"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "9101"
            - name: LOCAL_PORT
              value: "9100"
            - name: METRICS_PORT
              value: "9092"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          ports:
            - name: backup-metrics
              containerPort: 9101
              protocol: TCP
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 9101
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 9101
              scheme: HTTPS
          resources: {{ crd_spec | resources('ssl-terminator') }}
      volumes:
        - name: previous-boot
          emptyDir: {}
        - name: backup-scratch
          emptyDir: {}
        - name: config
          configMap:
            name: {{ dependencies['db_config'].resource_name() }}
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_exporter_certificate_secret'].resource_name() }}
        - name: ssl-terminator-config
          emptyDir: {}
        - name: run
          emptyDir:
            medium: Memory
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: exporter-config
          configMap:
            name: {{ dependencies['mysql_exporter_config'].resource_name() }}
        - name: certificates
          projected:
            sources:
              - secret:
                  name: {{ dependencies['ready_frontend_certificate_secret'].resource_name() }}
                  items:
                    - key: tls.crt
                      path: frontend/tls.crt
                      mode: {{ "u=r" | chmod }}
                    - key: tls.key
                      path: frontend/tls.key
                      mode: {{ "u=r" | chmod }}
                    - key: ca.crt
                      path: frontend/ca.crt
                      mode: {{ "u=r" | chmod }}
              - secret:
                  name: {{ dependencies['ready_replication_certificate_secret'].resource_name() }}
                  items:
                    - key: tls.crt
                      path: replication/tls.crt
                      mode: {{ "u=r" | chmod }}
                    - key: tls.key
                      path: replication/tls.key
                      mode: {{ "u=r" | chmod }}
                    - key: ca.crt
                      path: replication/ca.crt
                      mode: {{ "u=r" | chmod }}
              - secret:
                  name: {{ dependencies['ready_exporter_certificate_secret'].resource_name() }}
                  items:
                    - key: tls.crt
                      path: exporter/tls.crt
                      mode: {{ "u=r" | chmod }}
                    - key: tls.key
                      path: exporter/tls.key
                      mode: {{ "u=r" | chmod }}
                    - key: ca.crt
                      path: exporter/ca.crt
                      mode: {{ "u=r" | chmod }}
  volumeClaimTemplates:
    - metadata:
        name: data
        labels: {{ pod_labels }}
      spec:
        accessModes:
        - ReadWriteOnce
        volumeMode: Filesystem
        resources:
          requests:
            storage: {{ crd_spec.storageSize }}
{% if crd_spec.storageClassName | default(False) %}
        storageClassName: {{ crd_spec.storageClassName }}
{% endif %}
