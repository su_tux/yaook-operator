#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import kubernetes_asyncio.client as kclient

import typing

import yaook.common.config
import yaook.op.common
import yaook.statemachine as sm
import yaook.statemachine.resources as resources
import yaook.statemachine.registry
from yaook.statemachine.resources.k8s_service import TemplatedService

T = typing.TypeVar("T")


def deploy_ovn_relay(ctx: sm.Context) -> bool:
    return "ovnRelay" in ctx.parent_spec


def db_replicas(ctx: sm.Context) -> int:
    return ctx.parent_spec["replicas"]


def get_ovsdb_port(ctx: sm.Context) -> int:
    if "northbound" in ctx.parent_name:
        return yaook.op.common.OVSDB_NORTHBOUND_PORT
    return yaook.op.common.OVSDB_SOUTHBOUND_PORT


class OVSDBStatefulSet(sm.TemplatedStatefulSet):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: resources.DependencyMap,
            ) -> resources.TemplateParameters:
        param = await super()._get_template_parameters(ctx, dependencies)
        param["db_service_namespace"] = ctx.namespace
        param["deploy_ovn_relay"] = deploy_ovn_relay(ctx)

        dns_domain = sm.api_utils.get_cluster_domain()
        headless_service_dep = typing.cast(
            sm.Service,
            dependencies["headless_service"],
        )
        service_names = []
        headless_svc_ref = await headless_service_dep.get(ctx)
        for r in range(db_replicas(ctx)):
            dns_name = ".".join([
                f"{ctx.parent_name}-ovsdb-{r}",
                headless_svc_ref.name,
                headless_svc_ref.namespace,
                "svc",
                dns_domain
                ])
            port = get_ovsdb_port(ctx)
            service_names.append(":".join([
                "ssl",
                dns_name,
                str(port)
                ]))
        param["service_names"] = ",".join(service_names)
        return param


class OVNRelayStatefulSet(sm.TemplatedStatefulSet):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: resources.DependencyMap,
            ) -> resources.TemplateParameters:
        param = await super()._get_template_parameters(ctx, dependencies)
        service_depencency = typing.cast(
            sm.Service,
            dependencies["services_per_replica"],
        )
        service_ref = \
            await service_depencency.get_all(ctx)
        port = get_ovsdb_port(ctx)
        ovsdb_service_list = \
            await yaook.op.common.get_ovn_db_servers(
                ctx, service_ref, port)
        param["ovsdb_servers"] = ",".join(ovsdb_service_list)
        return param


class PerReplicasInCR(sm.StatelessInstancedResource[T]):
    async def get_target_instances(
            self,
            ctx: sm.Context,
            ) -> typing.Mapping[str, typing.Any]:
        replicas = db_replicas(ctx)
        return {f"{ctx.parent_name}-ovsdb-{i}": None
                for i in range(replicas)}


class TemplatedCertificateWithServices(sm.TemplatedCertificate):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: resources.DependencyMap
            ) -> resources.TemplateParameters:
        param = await super()._get_template_parameters(ctx, dependencies)
        v1 = kclient.CoreV1Api(ctx.api_client)
        dns_domain = sm.api_utils.get_cluster_domain()

        dns_names = []

        services_per_replica_dep = typing.cast(
            sm.Service,
            dependencies.get(
                yaook.op.common.OVSDB_DATABASE_SERVICE_COMPONENT),
        )

        headless_service_dep = typing.cast(
            sm.Service,
            dependencies["headless_service"],
        )

        access_service_dep = typing.cast(
            sm.Service,
            dependencies.get(yaook.op.common.OVN_ACCESS_SERVICE_COMPONENT),
        )

        svc_per_replica_ref = await services_per_replica_dep.get_all(ctx)

        for k8s_svc in svc_per_replica_ref.values():
            fqsn = f"{k8s_svc.name}.{k8s_svc.namespace}.svc.{dns_domain}"
            dns_names.append(fqsn)

            k8s_has_svc_now = await v1.read_namespaced_service(
                k8s_svc.name,
                k8s_svc.namespace,
                )
            dns_names.append(k8s_has_svc_now.spec.cluster_ip)

        # Also IP of access service must be in cert, as we use only IPs of
        # the access service and this forwards to relay or ovsdb, if no relay
        # deployed
        access_svc_ref = \
            await access_service_dep.get(ctx)
        fqsn = f"{access_svc_ref.name}.{access_svc_ref.namespace}.svc." \
            f"{dns_domain}"
        dns_names.append(fqsn)
        k8s_access_svc = await v1.read_namespaced_service(
            access_svc_ref.name,
            access_svc_ref.namespace,
            )
        dns_names.append(k8s_access_svc.spec.cluster_ip)

        headless_svc_ref = await headless_service_dep.get(ctx)

        # We use the headless service names for raft (ovsdb cluster)
        # connection. So this is only needed for ovsdb, not relay service
        for r in range(ctx.parent_spec["replicas"]):
            dns_names.append(".".join([
                f"{ctx.parent_name}-ovsdb-{r}",
                headless_svc_ref.name,
                headless_svc_ref.namespace,
                "svc",
                dns_domain
                ]))
        dns_names.append(headless_svc_ref.name)

        if self.component == "direct_relay_certificate":
            direct_relay_service_dep = typing.cast(
                sm.Service,
                dependencies["headless_relay_service"],
            )
            direct_relay_service_ref = await direct_relay_service_dep.get(ctx)
            dns_names.append(direct_relay_service_ref.name)

        param["dns_names"] = dns_names
        return param


class OVSDBService(sm.CustomResource):
    API_GROUP = "infra.yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "ovsdbservices"
    KIND = "OVSDBService"

    # The DNS names of the pods of this headless service
    # are used for the raft names of the cluster.
    headless_service = TemplatedService(
        params={
            "ovsdb_component": "ovsdb",
        },
        template="ovsdb-headless-service.yaml",
    )

    # The neutron operator will find the ClusterIP
    # from all these k8s services (one per ovsdb-server pod)
    # and construct the list of tcp:<ClusterIP>:<664x>.
    services_per_replica = PerReplicasInCR(
        wrapped_state=TemplatedService(
            component=yaook.op.common.OVSDB_DATABASE_SERVICE_COMPONENT,
            params={
                "ovsdb_component": "ovsdb",
            },
            template="ovsdb-per-pod-service.yaml",
        ),
    )

    # points to relay or ovsdb, if there is no relay
    access_service = TemplatedService(
        component=yaook.op.common.OVN_ACCESS_SERVICE_COMPONENT,
        template="ovsdb-access-service.yaml",
    )

    certificate_secret = sm.EmptyTlsSecret(
        metadata=("ovsdb-server-certificate-", True),
    )

    # Creates a signable certificate to be used by all pods.
    # Add all ClusterIPs from the services per pod,
    # FQDNs of services per pod and all constructed FQDNs
    # of the pods beneath the single headless service as DNS names.
    certificate = TemplatedCertificateWithServices(
        template="ovsdb-server-certificate.yaml",
        add_dependencies=[
            access_service,
            headless_service,
            services_per_replica,
            certificate_secret,
        ],
    )

    # This waits for cert-manager to sign our cert.
    ready_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=certificate,
    )

    backup_creator_image = sm.ConfigurableVersionedDockerImage(
        'backup-creator/backup-creator',
        sm.YaookSemVerSelector(),
    )

    backup_shifter_image = sm.ConfigurableVersionedDockerImage(
        'backup-shifter/backup-shifter',
        sm.YaookSemVerSelector(),
    )

    ovsdb_monitoring_image = sm.ConfigurableVersionedDockerImage(
        'ovsdb-monitoring/ovsdb-monitoring',
        sm.YaookSemVerSelector(),
    )

    ssl_terminator_image = sm.ConfigurableVersionedDockerImage(
        'ssl-terminator/ssl-terminator',
        sm.YaookSemVerSelector(),
    )

    service_reload_image = sm.ConfigurableVersionedDockerImage(
        'service-reload/service-reload',
        sm.YaookSemVerSelector(),
    )

    ca_certs = sm.CAConfigMap(
        metadata=lambda ctx: (f"{ctx.parent_name}-db-ca-certificates-", True),
        usercerts_spec_key="caCertificates",
        certificate_secrets_states=[
            ready_certificate_secret,
        ]
    )

    headless_relay_service = sm.OptionalKubernetesReference(
        condition=deploy_ovn_relay,
        wrapped_state=TemplatedService(
            params={
                "ovsdb_component": "ovn-relay",
            },
            template="ovsdb-headless-service.yaml",
        )
    )

    direct_relay_certificate_secret = sm.OptionalKubernetesReference(
        condition=deploy_ovn_relay,
        wrapped_state=sm.EmptyTlsSecret(
            metadata=("ovsdb-relay-certificate-", True),
        )
    )

    # This certificate is used for accessing the pod ips of the relay
    # directly (for example: prometheus).
    direct_relay_certificate = sm.OptionalKubernetesReference(
        condition=deploy_ovn_relay,
        wrapped_state=TemplatedCertificateWithServices(
            template="ovsdb-relay-certificate.yaml",
            add_dependencies=[
                access_service,
                headless_service,
                services_per_replica,
                headless_relay_service,
                direct_relay_certificate_secret,
            ],
        )
    )

    ready_direct_relay_certificate_secret = sm.OptionalKubernetesReference(
        condition=deploy_ovn_relay,
        wrapped_state=sm.ReadyCertificateSecretReference(
            certificate_reference=direct_relay_certificate,
        )
    )

    ovn_relay = sm.OptionalKubernetesReference(
        condition=deploy_ovn_relay,
        wrapped_state=OVNRelayStatefulSet(
            component="ovn-relay",
            template="ovn-relay-statefulset.yaml",
            add_dependencies=[
                ca_certs,
                headless_relay_service,
                ready_direct_relay_certificate_secret,
                services_per_replica,
            ],
            scheduling_keys=[
                yaook.op.common.SchedulingKey.INFRA_OVSDB_CLUSTER.value,
                yaook.op.common.SchedulingKey.ANY_INFRA.value,
            ],
            versioned_dependencies=[
                ssl_terminator_image,
                service_reload_image,
            ]
        )
    )

    ovsdb = OVSDBStatefulSet(
        component="ovsdb",
        template="ovsdb-statefulset.yaml",
        add_dependencies=[
            headless_service,
            ready_certificate_secret,
            ca_certs,
        ],
        scheduling_keys=[
            yaook.op.common.SchedulingKey.INFRA_OVSDB_CLUSTER.value,
            yaook.op.common.SchedulingKey.ANY_INFRA.value,
        ],
        versioned_dependencies=[
            backup_creator_image,
            backup_shifter_image,
            ovsdb_monitoring_image,
            ssl_terminator_image,
            service_reload_image,
        ]
    )

    ovsdb_pdb = sm.QuorumPodDisruptionBudget(
        metadata=lambda ctx: f"{ctx.parent_name}-db-pdb",
        replicated=ovsdb,
    )

    ovsdb_service_monitor = sm.GeneratedStatefulsetServiceMonitor(
        metadata=lambda ctx: (f"{ctx.parent_name}-ovsdb-service-monitor-",
                              True),
        service=headless_service,
        certificate=ready_certificate_secret,
        endpoints=["prometheus", "prometheus-ovsdb", "ovsdb-metrics"],
    )

    ovn_relay_pdb = sm.Optional(
        condition=deploy_ovn_relay,
        wrapped_state=sm.QuorumPodDisruptionBudget(
            metadata=lambda ctx: f"{ctx.parent_name}-ovn-relay-pdb",
            replicated=ovn_relay,
        )
    )

    ovn_relay_service_monitor = sm.Optional(
        condition=deploy_ovn_relay,
        wrapped_state=sm.GeneratedStatefulsetServiceMonitor(
            metadata=lambda ctx: (
                f"{ctx.parent_name}-ovn-relay-service-monitor-",
                True),
            service=headless_relay_service,
            certificate=ready_direct_relay_certificate_secret,
            endpoints=["prometheus-ovsdb"],
        )
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)


yaook.statemachine.registry.register(OVSDBService)
