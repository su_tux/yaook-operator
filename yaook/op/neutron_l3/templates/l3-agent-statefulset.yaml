##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ dependencies['l3_service'].resource_name() }}
spec:
  serviceName: {{ dependencies['l3_service'].resource_name() }}
  selector:
    matchLabels: {{ labels }}
  template:
    metadata:
      labels: {{ labels }}
    spec:
      automountServiceAccountToken: false
      tolerations: {{ params["tolerations"] }}
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchFields:
              - key: metadata.name
                operator: In
                values:
                # The name of the NeutronL3Agent resource must always be the
                # name of the Kubernetes node it runs on. So we use that here
                # to schedule the pod.
                # A bit of a hack, admittedly, but covered by tests, sooo...
                - {{ labels['state.yaook.cloud/parent-name'] }}
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchLabels:
                "network.yaook.cloud/provides-l2": "true"
            topologyKey: kubernetes.io/hostname
      hostNetwork: true
      hostPID: true
      dnsPolicy: ClusterFirstWithHostNet
      containers:
        - name: "neutron-l3-agent"
          image:  {{ versioned_dependencies['neutron_l3_agent_docker_image'] }}
          imagePullPolicy: Always
          command: ["/neutron-l3-agent-runner.sh"]
          securityContext:
            privileged: true
          volumeMounts:
            - name: neutron-l3-config-volume
              mountPath: /etc/neutron/neutron.conf
              subPath: neutron.conf
            - name: run-netns
              mountPath: /run/netns
              mountPropagation: Bidirectional
            - name: run-openvswitch
              mountPath: /run/openvswitch
            - name: iptables-lockfile
              mountPath: /run/xtables.lock
            - name: run-neutron
              mountPath: /run/neutron
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
          envFrom:
          - secretRef:
              name: {{ dependencies['startup_user_password'].resource_name() }}
          - configMapRef:
              name: {{ dependencies['keystone_internal_api'].resource_name() }}
          lifecycle:
            preStop:
              exec:
                command:
                  - sh
                  - -c
                  - |
                    set -xeu;
                    p=$(pidof -x /usr/local/bin/neutron-l3-agent);
                    kill -SIGSTOP ${p};
                    neutron-netns-cleanup --agent-type l3 -d --force --config-file /etc/neutron/neutron.conf --config-file /etc/neutron/l3_agent.ini;
                    kill -9 ${p};
                    kill -SIGCONT ${p};
          startupProbe:
            exec:
              command:
              - sh
              - -c
              - python3 /startup_wait_for_ns.py
            timeoutSeconds: 60
            periodSeconds: 60
            failureThreshold: {{ crd_spec.startupLimitMinutes }}
          livenessProbe:
            exec:
              command:
              - sh
              - -c
              - ps aux | grep privsep-helper | grep /etc/neutron/l3_agent.ini
          readinessProbe:
            exec:
              command:
              - sh
              - -c
              - ps aux | grep privsep-helper | grep /etc/neutron/l3_agent.ini
          resources: {{ crd_spec | resources('neutron-l3-agent') }}
        - name: "neutron-metadata-agent"
          image:  {{ versioned_dependencies['neutron_agent_docker_image'] }}
          imagePullPolicy: Always
          command: ["/neutron-metadata-agent-runner.sh"]
          volumeMounts:
            - name: neutron-metadata-config-volume
              mountPath: /etc/neutron/neutron.conf
              subPath: neutron.conf
            - name: run-neutron
              mountPath: /run/neutron
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
          livenessProbe:
            exec:
              command:
              - sh
              - -c
              - echo "GET /" | nc -U /run/neutron/metadata_proxy_socket  | grep HTTP
          readinessProbe:
            exec:
              command:
              - sh
              - -c
              - echo "GET /" | nc -U /run/neutron/metadata_proxy_socket  | grep HTTP
          resources: {{ crd_spec | resources('neutron-metadata-agent') }}
      volumes:
        - name: neutron-l3-config-volume
          secret:
            secretName: {{ dependencies['l3_config'].resource_name() }}
            items:
            - key: neutron_l3_agent.conf
              path: neutron.conf
        - name: neutron-metadata-config-volume
          secret:
            secretName: {{ dependencies['l3_config'].resource_name() }}
            items:
            - key: neutron_metadata_agent.conf
              path: neutron.conf
        - name: run-openvswitch
          hostPath:
            path: /run/openvswitch
        - name: run-netns
          hostPath:
            path: /run/netns
        - name: iptables-lockfile
          hostPath:
            path: /run/xtables.lock
        - name: run-neutron
          emptyDir: {}
        - name: ca-certs
          configMap:
            name: {{ crd_spec.caConfigMapName }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
