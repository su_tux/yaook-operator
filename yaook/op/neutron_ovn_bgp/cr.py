#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import environ
import ipaddress
import typing

import kubernetes_asyncio.client as kclient

import yaook.common
import yaook.common.config
import yaook.op.common
import yaook.statemachine as sm
import yaook.statemachine.resources.openstack as resource
from . import resources


class BridgeInterfaceMapping(typing.NamedTuple):
    """
    Mapping of the OVS bridge to the interface on which BGP will be peered
    """
    bridge_name: str
    interface_name: str
    interface_ip: str


class BGPConfigmapTemplate(sm.TemplatedConfigMap):
    def __init__(
            self,
            **kwargs: typing.Any):
        super().__init__(**kwargs)

    def _extract_interface_mappings(
            self,
            ctx: sm.Context,
            node_info: kclient.models.v1_node.V1Node,
            ) -> BridgeInterfaceMapping:
        bgp_interface = 'bgp-' + ctx.parent_uid[:11]
        ip_annotation = sm.context.ANNOTATION_BGP_INTERFACE_IP + \
            ctx.parent_spec["bgpNodeAnnotationSuffix"]
        ip = node_info.metadata.annotations.get(ip_annotation)
        try:
            ipaddress.ip_interface(ip)
        except ValueError:
            raise sm.ConfigurationInvalid(
                f"The given IP '{str(ip)}' via annotation "
                f"'{str(ip_annotation)}' is not a valid interface IP."
            )

        return BridgeInterfaceMapping(
            bridge_name=ctx.parent_spec["bridgeName"],
            interface_name=bgp_interface,
            interface_ip=ip,
        )

    async def _extract_router_id(
            self,
            ctx: sm.Context,
            node_info: kclient.models.v1_node.V1Node,
            ) -> str:
        router_id_annotation = sm.context.ANNOTATION_BGP_ROUTER_ID + \
            ctx.parent_spec["bgpNodeAnnotationSuffix"]

        router_id = node_info.metadata.annotations.get(router_id_annotation)

        try:
            # BGP router ids are UInt32 (4 byte unsigned integer), so you
            # could use a IPv4 address or just an integer.
            # This validates both.
            ipaddress.IPv4Address(router_id)
        except ValueError:
            raise sm.ConfigurationInvalid(
                f"The given ID '{router_id}' via annotation "
                f"'{router_id_annotation}' is not a valid router ID."
            )

        return router_id

    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap
            ) -> yaook.statemachine.resources.TemplateParameters:
        params = await super()._get_template_parameters(ctx, dependencies)
        v1 = kclient.CoreV1Api(ctx.api_client)
        node_info = await v1.read_node(ctx.parent_spec['hostname'])
        br_mapping = self._extract_interface_mappings(
            ctx,
            node_info,
        )

        router_id = await self._extract_router_id(
            ctx,
            node_info,
        )

        params["vars"]["ovs_bridge_name"] = br_mapping.bridge_name
        params["vars"]["bgp_interface"] = br_mapping.interface_name
        params["vars"]["local_peer_ip"] = br_mapping.interface_ip
        params["vars"]["router_id"] = router_id
        params["vars"]["bgp_vrf"] = f"vrf-{ctx.parent_uid[:8]}"
        params["vars"]["bgp_vrf_nic"] = f"vrfnic-{ctx.parent_uid[:8]}"

        return params


@environ.config(prefix="YAOOK_NEUTRON_OVN_BGP_AGENT_OP")
class OpNeutronOVNBGPAgentConfig:
    interface = environ.var("internal")
    job_image = environ.var()


class NeutronOVNBGPAgent(sm.CustomResource):
    API_GROUP = "network.yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "neutronovnbgpagents"
    KIND = "NeutronOVNBGPAgent"
    ADDITIONAL_PERMISSIONS = (
        # Required to read the node state for judging the next steps in
        # OVNBGPStateResource
        (True, "", "nodes", {"get", "patch"}),
    )

    ovn_bgp_agent_image = sm.ConfigurableVersionedDockerImage(
        "ovn-bgp-agent/ovn-bgp-agent",
        sm.YaookSemVerSelector(),
    )

    bgp_config = BGPConfigmapTemplate(
        template="ovn-bgp-config.yaml",
        copy_on_write=True,
    )

    bgp_service = sm.TemplatedService(
        template="ovn-bgp-service.yaml",
    )

    ovn_lock = resources.OVNBGPLock()

    certificate_secret = sm.EmptyTlsSecret(
        metadata=("ovn-bgp-certificate-", True),
    )

    certificate = sm.TemplatedCertificate(
        template="ovn-bgp-certificate.yaml",
        add_dependencies=[
            certificate_secret,
            bgp_service,
        ],
    )

    ready_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=certificate,
    )

    bgp_agent = sm.TemplatedRecreatingStatefulSet(
        template="ovn-bgp-statefulset.yaml",
        add_dependencies=[
            bgp_config,
            bgp_service,
            ready_certificate_secret,
            ovn_lock,
        ],
        versioned_dependencies=[
            ovn_bgp_agent_image,
        ],
        params={
            "tolerations": list(map(
                sm.api_utils.make_toleration,
                yaook.op.common.OVN_BGP_SCHEDULING_KEYS,
            )),
        },
    )

    bgp_agent_pdb = sm.DisallowedPodDisruptionBudget(
         metadata=("ovn-bgp-agent-pdb-", True),
         replicated=bgp_agent,
    )

    resource_state = resources.OVNBGPResource(
        finalizer="network.yaook.cloud/neutron-bgp-dragent",
        add_dependencies=[bgp_agent],
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)

    def _get_successful_update_patch(self,
                                     ctx: sm.Context,
                                     ) -> typing.Dict[str, str]:
        return {
            "state": resource.ResourceStatusState.ENABLED.value
        }


sm.register(NeutronOVNBGPAgent)
