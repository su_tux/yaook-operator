#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import base64
import itertools
import logging
import typing
import openstack
import asyncio

import kubernetes_asyncio.client
import kubernetes_asyncio.client as kclient

import yaook.common.config
import yaook.op.common
import yaook.statemachine as sm
import yaook.statemachine.api_utils as api_utils
import yaook.statemachine.resources
from yaook.statemachine.resources.openstack import _OpaqueParameters
from yaook.statemachine import context

# TODO(resource-refactor): clean up imports
import yaook.statemachine.resources.openstack as resource
from yaook.statemachine import interfaces

global_logger = logging.getLogger(__name__)


def _get_per_node_ceph_config(
        config_templates: typing.List[typing.Mapping],
        node_labels: typing.Mapping[str, str]) -> typing.Sequence:
    extracted_configs = yaook.op.common.extract_labeled_configs(
        config_templates,
        node_labels,
    )
    global_logger.debug(
        "extracted configs %r from %r for labels %r",
        extracted_configs,
        config_templates,
        node_labels,
    )
    configs = [x["volumeBackends"]["ceph"] for x in extracted_configs
               if x.get("volumeBackends", {}).get("ceph", {}).
               get("enabled", False)]
    return configs


def create_or_update_hostaggregate(
    client: openstack.connection.Connection,
    logger: logging.Logger,
    aggregate: str,
    spec: typing.Mapping[str, typing.Any],
) -> None:
    hostaggregate = client.compute.find_aggregate(
        aggregate,
    )
    if not hostaggregate:
        hostaggregate = client.compute.create_aggregate(
            name=aggregate, availability_zone=spec.get("zone", None)
        )
        logger.info(
            f"Nova Aggregate {hostaggregate.name}({hostaggregate.id}) created."
        )

    if spec.get("zone") != hostaggregate.availability_zone:
        hostaggregate = client.compute.update_aggregate(
            hostaggregate.id, availability_zone=spec.get("zone")
        )
        logger.info(
            f"Nova Aggregate {hostaggregate.name}({hostaggregate.id})"
            " availability_zone updated."
        )

    cr_value = spec.get("properties", {})
    api_value = hostaggregate.metadata or {}
    api_value.pop("availability_zone", None)
    # as every availability zone is handled as a metadata
    # (the openstack cli just filters it)
    # we remove availability_zone from the properties
    # returned by the API so we do not trigger permanent updates
    # as we do not specify it as property within the NovaHostAggregate CR

    if api_utils.deep_has_changes(api_value, cr_value):
        combined = set(api_value.keys()).union(set(cr_value.keys()))
        final_metadata = {}
        for key in combined:
            final_metadata[key] = cr_value.get(key, None)

        hostaggregate = client.compute.set_aggregate_metadata(
            hostaggregate.id, metadata=final_metadata
        )
        logger.info(
            f"Nova Aggregate {hostaggregate.name}({hostaggregate.id}) metadata"
            " updated."
        )


def delete_hostaggregate(
    client: openstack.connection.Connection,
    logger: logging.Logger,
    aggregate_name: str,
) -> None:
    hostaggregate = client.compute.find_aggregate(
        aggregate_name,
    )
    if not hostaggregate:
        return

    if hostaggregate.hosts:
        logger.info(
            f'Removing {len(hostaggregate.hosts)} hypervisors from '
            f'the aggregate, as the aggregate "{aggregate_name}" gets '
            'deleted.'
        )
        logger.debug(
            "The following hosts are removed from the aggregate: "
            f"{', '.join(hostaggregate.hosts)}"
        )
        for hypervisor in hostaggregate.hosts:
            client.compute.remove_host_from_aggregate(
                aggregate=hostaggregate,
                host=hypervisor,
            )

    client.compute.delete_aggregate(hostaggregate.id)

    logger.info(
        f"Nova Aggregate {hostaggregate.name}({hostaggregate.id}) deleted."
    )


class NovaHostAggregateResource(resource.KeystoneResource):
    def __init__(
        self,
        *,
        admin_credentials: sm.KubernetesReference[kclient.V1Secret],
        endpoint_config: sm.KubernetesReference[kclient.V1ConfigMap],
        **kwargs: typing.Any,
    ):
        super().__init__(
            admin_credentials=admin_credentials,
            endpoint_config=endpoint_config,
            **kwargs,
        )

    def _reconcile_aggregate(
        self,
        logger: logging.Logger,
        connection_parameters: resource._OpaqueParameters,
        name: str,
        spec: typing.Mapping[str, typing.Any],
    ) -> None:
        client = openstack.connect(**connection_parameters.params)

        create_or_update_hostaggregate(
            logger=logger,
            client=client,
            aggregate=name,
            spec=spec,
        )

    def _delete_hostaggregate(
        self,
        logger: logging.Logger,
        connection_parameters: resource._OpaqueParameters,
        aggregate_name: str,
    ) -> None:
        client = openstack.connect(**connection_parameters.params)

        delete_hostaggregate(
            logger=logger,
            client=client,
            aggregate_name=aggregate_name,
        )

    async def get_connection_params(self, ctx: context.Context) \
            -> _OpaqueParameters:
        nova_interface = interfaces.novadeployment_interface(ctx.api_client)
        novad = await nova_interface.read(
            ctx.namespace, ctx.parent_spec["novaRef"]["name"]
        )
        return await super()._get_connection_params(
            ctx, region=novad["spec"]["region"]["name"])

    async def update(
        self, ctx: sm.Context,
        dependencies: sm.DependencyMap,
    ) -> None:
        connection_parameters = await self.get_connection_params(ctx)

        await asyncio.get_event_loop().run_in_executor(
            None,
            self._reconcile_aggregate,
            ctx.logger,
            connection_parameters,
            ctx.parent_name,
            ctx.parent_spec,
        )

    async def delete(
        self, ctx: sm.Context,
        dependencies: sm.DependencyMap,
    ) -> None:
        connection_parameters = await self.get_connection_params(ctx)

        await asyncio.get_event_loop().run_in_executor(
            None,
            self._delete_hostaggregate,
            ctx.logger,
            connection_parameters,
            ctx.parent_name,
        )


class MetadataProxySecretLayer(sm.CueLayer):
    def __init__(
            self, *,
            metadata_password_state: sm.Secret,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._declare_dependencies(
            metadata_password_state,
        )
        self._metadata_password_state = metadata_password_state

    async def get_layer(
            self,
            ctx: sm.Context) -> sm.cue.CueInput:
        metadata_password = await sm.extract_password(
            ctx,
            self._metadata_password_state,
        )
        return {
            "nova": yaook.common.config.OSLO_CONFIG.declare([{
                "neutron": {
                    "metadata_proxy_shared_secret": metadata_password,
                },
            }]),
        }


class AutogeneratedSSHKey(sm.Secret):
    def __init__(self,
                 *,
                 metadata: sm.MetadataProvider,
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata = metadata

    async def _make_body(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> sm.ResourceBody:
        priv, pub = yaook.common.ssh.generate_ssh_keypair()
        priv_coded = base64.b64encode(priv.encode('ascii')).decode('ascii')
        pub_coded = base64.b64encode(pub.encode('ascii')).decode('ascii')

        return {
            "apiVersion": "v1",
            "kind": "Secret",
            "metadata": sm.evaluate_metadata(ctx, self._metadata),
            "type": "Opaque",
            "data": {
                "private_key": priv_coded,
                "public_key": pub_coded,
            }
        }

    def _needs_update(self,
                      current: kubernetes_asyncio.client.V1Secret,
                      new: typing.Mapping) -> bool:
        return (
            (("private_key" in current.data) !=
             ("private_key" in new["data"])) or
            (("public_key" in current.data) !=
                ("public_key" in new["data"]))
        )


class ComputePublicKeys(sm.Secret):
    def __init__(self, *,
                 metadata: sm.MetadataProvider,
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata = metadata

    def get_listeners(self) -> typing.List[sm.Listener]:
        return super().get_listeners() + [
            sm.KubernetesListener(
                api_group="compute.yaook.cloud",
                version="v1",
                plural="sshidentities",
                listener=self._handle_sshidentity_event,
                broadcast=True,
            ),
        ]

    def _handle_sshidentity_event(
            self,
            ctx: sm.Context,
            event: sm.watcher.StatefulWatchEvent[typing.Mapping],
            ) -> bool:
        metadata = sm.extract_metadata(event.object_)
        labels = metadata.get("labels") or {}
        try:
            component = labels[sm.context.LABEL_COMPONENT]
            parent_plural = labels[sm.context.LABEL_PARENT_PLURAL]
            parent_group = labels[sm.context.LABEL_PARENT_GROUP]
        except KeyError:
            # unrelated object
            return False

        if parent_group != "compute.yaook.cloud":
            return False
        if parent_plural != "novacomputenodes":
            return False
        if component != yaook.op.common.NOVA_COMPUTE_PUBLIC_KEYS_COMPONENT:
            return False
        if (event.type_ == sm.watcher.EventType.MODIFIED and
                event.old_object is not None):
            current_keys = (event.object_.get("status") or {}).get("keys", {})
            old_keys = (event.old_object.get("status") or {}).get("keys", {})
            return old_keys != current_keys
        return True

    async def _collect_key_material(
            self,
            ctx: sm.Context,
            ) -> typing.Mapping[str, typing.Mapping[str, typing.List[str]]]:
        """
        Fetch and collect all known hosts and authorized keys
        """
        sshidentities = sm.sshidentity_interface(ctx.api_client)
        selector = sm.LabelSelector(
            match_labels={
                sm.context.LABEL_COMPONENT:
                    yaook.op.common.NOVA_COMPUTE_PUBLIC_KEYS_COMPONENT,
                sm.context.LABEL_PARENT_GROUP: "compute.yaook.cloud",
                sm.context.LABEL_PARENT_PLURAL: "novacomputenodes",
            },
            match_expressions=[
                sm.LabelExpression(
                    key=sm.context.LABEL_ORPHANED,
                    operator=sm.SelectorOperator.NOT_EXISTS,
                    values=None,
                ),
                sm.LabelExpression(
                    key=sm.context.LABEL_PARENT_NAME,
                    operator=sm.SelectorOperator.EXISTS,
                    values=None,
                ),
            ],
        )

        result: typing.Dict[str, typing.Dict[str, typing.List[str]]] = {}
        for sshidentity in await sshidentities.list_(
                ctx.namespace,
                label_selector=selector.as_api_selector()):
            node_name = sshidentity["metadata"]["labels"][
                sm.context.LABEL_PARENT_NAME
            ]
            keys = (sshidentity.get("status") or {}).get("keys") or {}
            ctx.logger.debug(
                "found keys %r for node %s",
                keys, node_name,
            )
            result[node_name] = keys
        ctx.logger.debug("found keys for %d nodes", len(result))
        return result

    async def _process_public_keys(
            self,
            ctx: sm.Context,
            keymap: typing.Mapping[
                str,
                typing.Mapping[str, typing.Collection[str]]
            ]) -> typing.Mapping[str, typing.List[str]]:
        port = 8022
        core_api = kclient.CoreV1Api(ctx.api_client)
        nodes = await core_api.list_node()
        addrmap = {
            node.metadata.name: node.status.addresses
            for node in nodes.items
        }
        del nodes

        known_hosts: typing.List[str] = []
        authorized_keys: typing.List[str] = []

        for node_name, keys in keymap.items():
            try:
                addrs = addrmap[node_name]
            except KeyError:
                ctx.logger.debug(
                    "ignoring keys for node %s because "
                    "the node does not exist",
                    node_name,
                )
                continue
            addr_selectors = []
            auth_from = []
            for addr in addrs:
                if addr.type == "Hostname":
                    addr_selectors.append(f"[{addr.address}]:{port}")
                else:
                    addr_selectors.append(f"[{addr.address}]:{port}")
                    auth_from.append(addr.address)

            if addr_selectors:
                addr_prefix = ",".join(addr_selectors)

                known_hosts.extend(
                    f"{addr_prefix} {key}"
                    for key in keys.get("host", [])
                )
            else:
                ctx.logger.warning(
                    "ignoring host keys for node %s because no addresses for "
                    "that node could be found!",
                    node_name,
                )

            if auth_from:
                key_prefix = f'command="/usr/lib/restricted-ssh-commands",' \
                             f'restrict,from="{",".join(auth_from)}"'
                authorized_keys.extend(
                    f"{key_prefix} {key}"
                    for key in keys.get("user", [])
                )
            else:
                ctx.logger.warning(
                    "ignoring user keys for node %s because no IP addresses "
                    "for that node could be found!",
                    node_name,
                )

        return {
            "known_hosts": known_hosts,
            "authorized_keys": authorized_keys,
        }

    async def _make_body(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> sm.ResourceBody:
        collected_keys = await self._collect_key_material(ctx)
        key_material = await self._process_public_keys(ctx, collected_keys)
        known_hosts = key_material.get("known_hosts", [])
        known_hosts.append("")  # for a trailing newline
        authorized_keys = key_material.get("authorized_keys", [])
        authorized_keys.append("")

        return {
            "apiVersion": "v1",
            "kind": "Secret",
            "metadata": sm.evaluate_metadata(ctx, self._metadata),
            "type": "compute.yaook.cloud/ssh-public-keys",
            "data": api_utils.encode_secret_data({
                "known_hosts": "\n".join(known_hosts),
                "authorized_keys": "\n".join(authorized_keys),
            }),
        }


class NovaComputeNode(
        sm.SingleObject[typing.Mapping],
        ):
    def _create_resource_interface(
            self,
            api_client: kubernetes_asyncio.client.ApiClient,
            ) -> sm.ResourceInterfaceWithStatus[typing.Mapping]:
        return sm.nova_compute_node_interface(api_client)

    def _needs_update(self, old: typing.Mapping, new: typing.Mapping) -> bool:
        relevant_keys = [
            "keystoneRef",
            "publicKeysSecretRef",
            "novaConfig",
            "libvirtConfig",
            "caConfigMapName",
            "cephBackend",
            "ids",
            "imagePullSecrets",
            "vnc",
            "messageQueue",
            "targetRelease",
            "region",
            "hostAggregates",
        ]
        old_spec = old["spec"]
        old_spec_pruned = {
            k: v for k, v in old_spec.items()
            if k in relevant_keys
        }
        new_spec = new["spec"]
        new_spec_pruned = {
            k: v for k, v in new_spec.items()
            if k in relevant_keys
        }
        return (super()._needs_update(old, new) or
                old_spec_pruned != new_spec_pruned)

    def _collect_nova_compute_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        extracted_configs = yaook.op.common.extract_labeled_configs(
            ctx.parent_spec.get("compute", {}).get("configTemplates", []),
            node_labels,
        )
        ctx.logger.debug(
            "extracted configs %r from %r for labels %r",
            extracted_configs,
            ctx.parent_spec.get("compute", {}).get("configTemplates", []),
            node_labels,
        )
        configs = yaook.op.common.transpose_config(
            extracted_configs,
            ["novaComputeConfig"],
        )
        ctx.logger.debug(
            "transposed configs %r from %r",
            configs,
            extracted_configs,
        )
        return configs.pop("novaComputeConfig")

    async def _collect_nova_hostaggregate_config(
        self,
        ctx: sm.Context,
        node_labels: typing.Mapping[str, str],
    ) -> typing.Optional[typing.Collection[typing.Mapping]]:
        interface = sm.nova_compute_host_aggregate_interface(ctx.api_client)

        extracted_configs = yaook.op.common.extract_labeled_configs(
            ctx.parent_spec.get("compute", {}).get("configTemplates", []),
            node_labels,
        )
        ctx.logger.debug(
            "extracted configs %r from %r for labels %r",
            extracted_configs,
            ctx.parent_spec.get("compute", {}).get("configTemplates", []),
            node_labels,
        )
        configs = yaook.op.common.transpose_config(
            extracted_configs,
            ["hostAggregates"],
        )
        ctx.logger.debug(
            "transposed configs %r from %r",
            configs,
            extracted_configs,
        )
        aggregates = set(itertools.chain(*configs.get("hostAggregates", [])))

        if not aggregates:
            return None
        cr_aggregates = typing.cast(
            typing.Iterable,
            await interface.list_(namespace=ctx.namespace)
        )
        cr_aggregates = filter(
            lambda x: x["spec"]["novaRef"]["name"] == ctx.parent_name,
            cr_aggregates
        )
        cr_aggregates = {
            aggregate["metadata"]["name"]:
            aggregate for aggregate in cr_aggregates
        }

        invalid_aggregates = aggregates.difference(cr_aggregates.keys())

        if invalid_aggregates:
            raise sm.ConfigurationInvalid(
                "Aggregate specified in labels is not managed by yaook. Please"
                " choose only HostAggregates created by NovaHostAggregate"
                " Resource. The following aggregates are not managed by"
                f" yaook: {', '.join(invalid_aggregates)}"
            )

        # Having the same zone multiple times is fine
        aggregate_zones = set()
        for az in aggregates:
            zone = cr_aggregates[az]["spec"].get("zone", None)
            aggregate_zones.add(zone) if zone else None

        if len(aggregate_zones) > 1:
            raise sm.ConfigurationInvalid(
                "Projected configuration contains more than a single"
                " Availability Zone. Projected Zones"
                f" {', '.join(aggregate_zones)}. This is not valid."
                " Please verify to only include HostAggregates to place the"
                " node in a single Availability Zone."
            )

        return list(aggregates)

    def _collect_ceph_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Mapping:
        configs = _get_per_node_ceph_config(
            ctx.parent_spec.get("compute", {}).get("configTemplates", []),
            node_labels,
        )
        if not configs:
            return {
                "enabled": False,
            }

        primary = configs[0]
        for cfg in configs[1:]:
            if cfg != primary:
                raise sm.ConfigurationInvalid(
                    "found multiple conflicting ceph configurations for "
                    f"{ctx.instance}: {primary} != {cfg}\n%s" % repr(configs)
                )

        return configs[0]

    async def get_used_resources(
            self,
            ctx: sm.Context,
            ) -> typing.Iterable[sm.ResourceReference]:
        result = list(await super().get_used_resources(ctx))
        try:
            instance = await self._get_current(ctx, True)
        except sm.ResourceNotPresent:
            return result

        result.append(
            api_utils.ResourceReference(
                api_version="v1", plural="configmaps", namespace=ctx.namespace,
                name=instance["spec"]["publicKeysSecretRef"]["name"],
            ),
        )

        ceph_backend = instance["spec"].get("cephBackend", {})
        if ceph_backend.get("enabled", False):
            result.append(
                api_utils.ResourceReference(
                    api_version="v1", plural="secrets",
                    namespace=ctx.namespace,
                    name=ceph_backend["keyringSecretName"],
                )
            )

        pods = sm.pod_interface(ctx.api_client)
        label_selector = sm.LabelSelector(
            match_labels={
                sm.context.LABEL_PARENT_PLURAL: "novacomputenodes",
                sm.context.LABEL_PARENT_GROUP: "compute.yaook.cloud",
                sm.context.LABEL_PARENT_NAME: instance["metadata"]["name"],
            }
        )
        for pod in await pods.list_(
                ctx.namespace,
                label_selector=label_selector.as_api_selector()):
            result.extend(api_utils.extract_pod_references(
                pod.spec, ctx.namespace,
            ))
        return result


class TemplatedNovaComputeNode(
        yaook.statemachine.resources.BodyTemplateMixin,
        NovaComputeNode):
    async def _get_template_parameters(
        self,
        ctx: sm.Context,
        dependencies: sm.DependencyMap,
    ) -> yaook.statemachine.resources.TemplateParameters:
        result = await super()._get_template_parameters(ctx, dependencies)
        node_labels = await yaook.op.common.get_node_labels_from_instance(ctx)
        result["vars"]["nova_config"] = \
            self._collect_nova_compute_config(ctx, node_labels)
        result["vars"]["hostAggregates"] = \
            await self._collect_nova_hostaggregate_config(ctx, node_labels)
        result["vars"]["ceph_backend"] = \
            self._collect_ceph_config(ctx, node_labels)
        return result


class NovaComputeNodes(resource.L2AwareStatefulAgentResource):
    def get_listener(self) -> typing.List[sm.Listener]:
        return [
            sm.KubernetesListener[typing.Mapping](
                "compute.yaook.cloud", "v1", "novacomputenodes",
                self._handle_event,
                broadcast=True,
            ),
        ]
