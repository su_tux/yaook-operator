##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
{% if target_release in ["queens", "rocky", "stein"] %}
{%   set policy = "policy.json" %}
{% else %}
{%   set policy = "policy.yaml" %}
{% endif %}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "neutron-api"
  labels:
    app: "neutron-api"
spec:
  replicas:  {{ crd_spec.api.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
        config-timestamp: {{ dependencies['config'].last_update_timestamp() }}
    spec:
      automountServiceAccountToken: false
      shareProcessNamespace: true
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "neutron-api"
          image:  {{ versioned_dependencies['neutron_docker_image'] }}
          imagePullPolicy: Always
          command: ["neutron-server", "--config-file=/etc/neutron/neutron.conf", "--config-file=/etc/neutron/plugins/ml2/ml2_conf.ini"]
          volumeMounts:
            - name: neutron-config-vol
              mountPath: /etc/neutron
            - name: neutron-ml2-config-volume
              mountPath: /etc/neutron/plugins/ml2
            - name: ca-certs
              mountPath: /etc/ssl/certs
{% if 'ovn' in crd_spec.setup | default(False) %}
            - name: ml2-plugin-certs
              mountPath: /etc/ssl/ml2-plugin
{% endif %}
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-bundle.crt
          livenessProbe:
            exec:
              command:
                - curl
                - localhost:8080
            failureThreshold: 6
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          readinessProbe:
            exec:
              command:
                - curl
                - localhost:8080
            failureThreshold: 3
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          startupProbe:
            exec:
              command:
                - curl
                - localhost:8080
            failureThreshold: 30
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          resources: {{ crd_spec | resources('api.neutron-api') }}
        - name: "ssl-terminator"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "9696"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9090"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /
              port: 9696
              scheme: HTTPS
            failureThreshold: 6
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          readinessProbe:
            httpGet:
              path: /
              port: 9696
              scheme: HTTPS
            failureThreshold: 3
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          startupProbe:
            httpGet:
              path: /
              port: 9696
              scheme: HTTPS
            failureThreshold: 30
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          resources: {{ crd_spec | resources('api.ssl-terminator') }}
        - name: "ssl-terminator-external"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "9697"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9091"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /
              port: 9697
              scheme: HTTPS
            failureThreshold: 6
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          readinessProbe:
            httpGet:
              path: /
              port: 9697
              scheme: HTTPS
            failureThreshold: 3
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          startupProbe:
            httpGet:
              path: /
              port: 9697
              scheme: HTTPS
            failureThreshold: 30
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          resources: {{ crd_spec | resources('api.ssl-terminator-external') }}
{% if crd_spec.api.internal | default(False) %}
        - name: "ssl-terminator-internal"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "9698"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9092"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-internal-config
              mountPath: /config
            - name: tls-secret-internal
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /
              port: 9698
              scheme: HTTPS
            failureThreshold: 6
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          readinessProbe:
            httpGet:
              path: /
              port: 9698
              scheme: HTTPS
            failureThreshold: 3
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          startupProbe:
            httpGet:
              path: /
              port: 9698
              scheme: HTTPS
            failureThreshold: 30
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          resources: {{ crd_spec | resources('api.ssl-terminator-internal') }}
{% endif %}
        - name: "service-reload"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('api.service-reload') }}
        - name: "service-reload-external"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('api.service-reload-external') }}
{% if crd_spec.api.internal | default(False) %}
        - name: "service-reload-internal"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-internal-config
              mountPath: /config
            - name: tls-secret-internal
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('api.service-reload-internal') }}
{% endif %}
      volumes:
        - name: neutron-config-vol
          projected:
            sources:
            - secret:
                name: {{ dependencies['config'].resource_name() }}
                items:
                  - key: neutron.conf
                    path: neutron.conf
            - configMap:
                name: {{ dependencies['ready_neutron_policy'].resource_name() }}
                items:
                  - key: {{ policy }}
                    path: {{ policy }}
        - name: neutron-ml2-config-volume
          secret:
            secretName: {{ dependencies['config'].resource_name() }}
            items:
              - key: neutron_ml2.conf
                path: ml2_conf.ini
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_certificate_secret'].resource_name() }}
        - name: tls-secret-external
          secret:
            secretName: {{ dependencies['external_certificate_secret'].resource_name() }}
        - name: ssl-terminator-config
          emptyDir: {}
        - name: ssl-terminator-external-config
          emptyDir: {}
{% if crd_spec.api.internal | default(False) %}
        - name: tls-secret-internal
          secret:
            secretName: {{ dependencies['internal_certificate_secret'].resource_name() }}
        - name: ssl-terminator-internal-config
          emptyDir: {}
{% endif %}
{% if 'ovn' in crd_spec.setup | default(False) %}
        - name: ml2-plugin-certs
          secret:
            secretName: {{ dependencies['ready_ml2_plugin_certificate_secret'].resource_name() }}
{% endif %}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
