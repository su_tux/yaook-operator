#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import logging
import typing
import abc

import kubernetes_asyncio.client as kclient

import yaook.common.config
import yaook.op.common

import yaook.statemachine as sm
from yaook.statemachine import api_utils, context
from yaook.statemachine.resources.instancing import (
    InstanceState, InstanceGroup
)
from yaook.statemachine.exceptions import ConfigurationInvalid


def _get_per_node_special_config(
        config_templates: typing.List[typing.Mapping],
        node_labels: typing.Mapping[str, str],
        section: str,
        logger: logging.Logger,
        ) -> typing.List[typing.Mapping]:
    extracted_configs = yaook.op.common.extract_labeled_configs(
        config_templates,
        node_labels,
    )
    logger.debug(
        "extracted configs %r from %r for labels %r",
        extracted_configs,
        config_templates,
        node_labels,
    )
    configs = yaook.op.common.transpose_config(
        extracted_configs,
        [section],
    )
    logger.debug(
        "transposed configs %r from %r",
        configs,
        extracted_configs,
    )
    return configs.pop(section)


async def _bgp_spec_configkeys(
        ctx: sm.Context,
        setupkey: str
) -> typing.Set[str]:
    return ctx.parent_spec["setup"][setupkey].get("bgp", {}).keys()


class NeutronDHCPAgent(sm.SingleObject[typing.Mapping]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> sm.ResourceInterfaceWithStatus[typing.Mapping]:
        return sm.neutron_dhcp_agent_interface(api_client)

    def _needs_update(self, old: typing.Mapping, new: typing.Mapping) -> bool:
        relevant_keys = [
            "keystoneRef",
            "novaRef",
            "neutronConfig",
            "neutronDHCPAgentConfig",
            "caConfigMapName",
            "messageQueue",
            "neutronMetadataAgentConfig",
            "imagePullSecrets",
            "region",
            "targetRelease",
        ]
        old_spec = old["spec"]
        old_spec_pruned = {
            k: v for k, v in old_spec.items()
            if k in relevant_keys
        }
        new_spec = new["spec"]
        new_spec_pruned = {
            k: v for k, v in new_spec.items()
            if k in relevant_keys
        }
        return (super()._needs_update(old, new) or
                old_spec_pruned != new_spec_pruned)

    def _collect_neutron_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        return _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovs"]["dhcp"]["configTemplates"],
            node_labels,
            'neutronConfig',
            ctx.logger,
        )

    def _collect_neutron_dhcp_agent_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        return _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovs"]["dhcp"]["configTemplates"],
            node_labels,
            'neutronDHCPAgentConfig',
            ctx.logger,
        )

    def _collect_neutron_metadata_agent_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        return _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovs"]["dhcp"]["configTemplates"],
            node_labels,
            'neutronMetadataAgentConfig',
            ctx.logger,
        )

    async def get_used_resources(
            self,
            ctx: sm.Context,
            ) -> typing.Iterable[sm.ResourceReference]:
        result = list(await super().get_used_resources(ctx))
        try:
            instance = await self._get_current(ctx, True)
        except sm.ResourceNotPresent:
            return result

        pods = sm.pod_interface(ctx.api_client)
        label_selector = sm.LabelSelector(
            match_labels={
                sm.context.LABEL_PARENT_PLURAL: "neutrondhcpagents",
                sm.context.LABEL_PARENT_GROUP: "network.yaook.cloud",
                sm.context.LABEL_PARENT_NAME: instance["metadata"]["name"],
            }
        )
        for pod in await pods.list_(
                ctx.namespace,
                label_selector=label_selector.as_api_selector()):
            result.extend(api_utils.extract_pod_references(
                pod.spec, ctx.namespace,
            ))
        return result


class TemplatedNeutronDHCPAgent(
        yaook.statemachine.resources.BodyTemplateMixin,
        NeutronDHCPAgent):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> yaook.statemachine.resources.TemplateParameters:
        result = await super()._get_template_parameters(ctx, dependencies)
        node_labels = await yaook.op.common.get_node_labels_from_instance(ctx)
        result["vars"]["neutron_config"] = \
            self._collect_neutron_config(ctx, node_labels)
        result["vars"]["neutron_dhcp_agent_config"] = \
            self._collect_neutron_dhcp_agent_config(ctx, node_labels)
        result["vars"]["neutron_metadata_agent_config"] = \
            self._collect_neutron_metadata_agent_config(ctx, node_labels)
        return result


class NeutronDHCPAgents(sm.L2AwareStatefulAgentResource):
    def get_listener(self) -> typing.List[sm.Listener]:
        return [
            sm.KubernetesListener[typing.Mapping](
                "network.yaook.cloud", "v1", "neutrondhcpagents",
                self._handle_event,
                broadcast=True,
            ),
        ]


class NeutronL2Agent(sm.SingleObject[typing.Mapping]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> sm.ResourceInterfaceWithStatus[typing.Mapping]:
        return sm.neutron_l2_agent_interface(api_client)

    def _needs_update(self, old: typing.Mapping, new: typing.Mapping) -> bool:
        relevant_keys = [
            "keystoneRef",
            "neutronConfig",
            "bridgeConfig",
            "overlayNetworkConfig",
            "neutronOpenvSwitchAgentConfig",
            "caConfigMapName",
            "messageQueue",
            "imagePullSecrets",
            "region",
            "targetRelease",
        ]
        old_spec = old["spec"]
        old_spec_pruned = {
            k: v for k, v in old_spec.items()
            if k in relevant_keys
        }
        new_spec = new["spec"]
        new_spec_pruned = {
            k: v for k, v in new_spec.items()
            if k in relevant_keys
        }
        return (super()._needs_update(old, new) or
                old_spec_pruned != new_spec_pruned)

    def _collect_neutron_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        return _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovs"]["l2"]["configTemplates"],
            node_labels,
            'neutronConfig',
            ctx.logger,
        )

    def _collect_neutron_ovs_agent_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        return _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovs"]["l2"]["configTemplates"],
            node_labels,
            'neutronOpenvSwitchAgentConfig',
            ctx.logger,
        )

    def _collect_bridge_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        configs = _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovs"]["l2"]["configTemplates"],
            node_labels,
            'bridgeConfig',
            ctx.logger,
        )
        if not configs:
            return []

        # TODO: do we really want to support only one bridge mapping section?
        # many mappings are ok, but they must be configured at same label...
        primary = configs[0]
        for cfg in configs[1:]:
            if cfg != primary:
                raise sm.ConfigurationInvalid(
                    "found multiple conflicting bridge configurations for "
                    f"{ctx.instance}: {primary} != {cfg}\n%s" % repr(configs)
                )

        return configs[0]

    def _collect_overlay_network_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:

        overlay_network_configs = _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovs"]["l2"]["configTemplates"],
            node_labels,
            'overlayNetworkConfig',
            ctx.logger,
        )

        if len(overlay_network_configs) == 0:
            return {}

        unique_overlay_network_configs = []

        for onc in overlay_network_configs:
            if onc not in unique_overlay_network_configs:
                unique_overlay_network_configs.append(onc)

        if len(unique_overlay_network_configs) > 1:
            raise sm.ConfigurationInvalid(
                "Multiple configurations for overlay network match Neutron L2 "
                f"agent by node label for node {ctx.instance}. Overlay network"
                " configuration (overlayNetworkConfig) is: "
                f"{overlay_network_configs}"
            )

        return unique_overlay_network_configs[0]

    async def get_used_resources(
            self,
            ctx: sm.Context,
            ) -> typing.Iterable[sm.ResourceReference]:
        result = list(await super().get_used_resources(ctx))
        try:
            instance = await self._get_current(ctx, True)
        except sm.ResourceNotPresent:
            return result

        pods = sm.pod_interface(ctx.api_client)
        label_selector = sm.LabelSelector(
            match_labels={
                sm.context.LABEL_PARENT_PLURAL: "neutronl2agents",
                sm.context.LABEL_PARENT_GROUP: "network.yaook.cloud",
                sm.context.LABEL_PARENT_NAME: instance["metadata"]["name"],
            }
        )
        for pod in await pods.list_(
                ctx.namespace,
                label_selector=label_selector.as_api_selector()):
            result.extend(api_utils.extract_pod_references(
                pod.spec, ctx.namespace,
            ))
        return result


class TemplatedNeutronL2Agent(
        yaook.statemachine.resources.BodyTemplateMixin,
        NeutronL2Agent):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> yaook.statemachine.resources.TemplateParameters:
        result = await super()._get_template_parameters(ctx, dependencies)
        node_labels = await yaook.op.common.get_node_labels_from_instance(ctx)
        result["vars"]["neutron_config"] = \
            self._collect_neutron_config(ctx, node_labels)
        result["vars"]["neutron_openvswitch_agent_config"] = \
            self._collect_neutron_ovs_agent_config(ctx, node_labels)
        result["vars"]["bridge_config"] = \
            self._collect_bridge_config(ctx, node_labels)
        result["vars"]["overlay_network_config"] = \
            self._collect_overlay_network_config(ctx, node_labels)
        return result


class NeutronL2Agents(sm.StatefulAgentResource):
    def get_listener(self) -> typing.List[sm.Listener]:
        return [
            sm.KubernetesListener[typing.Mapping](
                "network.yaook.cloud", "v1", "neutronl2agents",
                self._handle_event,
                broadcast=True,
            ),
        ]


class NeutronOVNAgent(sm.SingleObject[typing.Mapping]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> sm.ResourceInterfaceWithStatus[typing.Mapping]:
        return sm.neutron_ovn_agent_interface(api_client)

    def _needs_update(self, old: typing.Mapping, new: typing.Mapping) -> bool:
        relevant_keys = [
            "bridgeConfig",
            "caConfigMapName",
            "keystoneRef",
            "ovnMonitorAll",
            "southboundInactivityProbe",
            "southboundServers",
            "imagePullSecrets",
            "region",
            "resources",
            "issuerRef",
        ]
        old_spec = old["spec"]
        old_spec_pruned = {
            k: v for k, v in old_spec.items()
            if k in relevant_keys
        }
        new_spec = new["spec"]
        new_spec_pruned = {
            k: v for k, v in new_spec.items()
            if k in relevant_keys
        }
        return (super()._needs_update(old, new) or
                old_spec_pruned != new_spec_pruned)

    def _collect_bridge_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        configs = _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovn"]["controller"]["configTemplates"],
            node_labels,
            'bridgeConfig',
            ctx.logger,
        )
        if not configs:
            return []

        # TODO: do we really want to support only one bridge mapping section?
        # many mappings are ok, but they must be configured at same label...
        primary = configs[0]
        for cfg in configs[1:]:
            if cfg != primary:
                raise sm.ConfigurationInvalid(
                    "found multiple conflicting bridge configurations for "
                    f"{ctx.instance}: {primary} != {cfg}\n%s" % repr(configs)
                )

        return configs[0]

    def _collect_neutron_metadata_agent_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        return _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovn"]["controller"]["configTemplates"],
            node_labels,
            'neutronMetadataAgentConfig',
            ctx.logger,
        )

    async def _collect_southbound_services(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> typing.List:
        southbound_servers = []
        sb = typing.cast(
            sm.OptionalKubernetesReference,
            dependencies.get("southbound_ovsdb_access_service")
        )
        service_ref = await sb.get_all(ctx)
        southbound_servers = await yaook.op.common.get_ovn_db_servers(
            ctx, service_ref, yaook.op.common.OVSDB_SOUTHBOUND_PORT)
        return southbound_servers

    async def _deployed_on_compute_node(
            self,
            node_labels: typing.Mapping[str, str],
            ) -> bool:
        param = yaook.op.common.SchedulingKey.COMPUTE_HYPERVISOR.value \
            in node_labels
        return param

    async def get_used_resources(
            self,
            ctx: sm.Context,
            ) -> typing.Iterable[sm.ResourceReference]:
        result = list(await super().get_used_resources(ctx))
        try:
            instance = await self._get_current(ctx, True)
        except sm.ResourceNotPresent:
            return result

        pods = sm.pod_interface(ctx.api_client)
        label_selector = sm.LabelSelector(
            match_labels={
                sm.context.LABEL_PARENT_PLURAL: "neutronovnagents",
                sm.context.LABEL_PARENT_GROUP: "network.yaook.cloud",
                sm.context.LABEL_PARENT_NAME: instance["metadata"]["name"],
            }
        )
        for pod in await pods.list_(
                ctx.namespace,
                label_selector=label_selector.as_api_selector()):
            result.extend(api_utils.extract_pod_references(
                pod.spec, ctx.namespace,
            ))
        result.append(sm.ResourceReference.config_map(
            ctx.namespace, instance["spec"]["caConfigMapName"]))
        return result


class TemplatedNeutronOVNAgent(
        yaook.statemachine.resources.BodyTemplateMixin,
        NeutronOVNAgent):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> yaook.statemachine.resources.TemplateParameters:
        result = await super()._get_template_parameters(ctx, dependencies)
        node_labels = await yaook.op.common.get_node_labels_from_instance(ctx)
        result["vars"]["bridge_config"] = \
            self._collect_bridge_config(ctx, node_labels)
        result["vars"]["southbound_servers"] = \
            self._collect_southbound_services(ctx, dependencies)
        result["vars"]["deployed_on_compute_node"] = \
            self._deployed_on_compute_node(node_labels)
        result["vars"]["neutron_metadata_agent_config"] = \
            self._collect_neutron_metadata_agent_config(ctx, node_labels)
        return result


class NeutronOVNAgents(sm.StatefulAgentResource):
    def get_listener(self) -> typing.List[sm.Listener]:
        return [
            sm.KubernetesListener[typing.Mapping](
                "network.yaook.cloud", "v1", "neutronovnagents",
                self._handle_event,
                broadcast=True,
            ),
        ]


class NeutronL3Agent(sm.SingleObject[typing.Mapping]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> sm.ResourceInterfaceWithStatus[typing.Mapping]:
        return sm.neutron_l3_agent_interface(api_client)

    def _needs_update(self, old: typing.Mapping, new: typing.Mapping) -> bool:
        relevant_keys = [
            "keystoneRef",
            "novaRef",
            "neutronConfig",
            "neutronL3AgentConfig",
            "neutronMetadataAgentConfig",
            "caConfigMapName",
            "messageQueue",
            "imagePullSecrets",
            "targetRelease",
            "startupLimitMinutes",
            "evictor",
            "region",
        ]
        old_spec = old["spec"]
        old_spec_pruned = {
            k: v for k, v in old_spec.items()
            if k in relevant_keys
        }
        new_spec = new["spec"]
        new_spec_pruned = {
            k: v for k, v in new_spec.items()
            if k in relevant_keys
        }
        return (super()._needs_update(old, new) or
                old_spec_pruned != new_spec_pruned)

    def _collect_neutron_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        return _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovs"]["l3"]["configTemplates"],
            node_labels,
            'neutronConfig',
            ctx.logger,
        )

    def _collect_neutron_l3_agent_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        return _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovs"]["l3"]["configTemplates"],
            node_labels,
            'neutronL3AgentConfig',
            ctx.logger,
        )

    def _collect_neutron_metadata_agent_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        return _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovs"]["l3"]["configTemplates"],
            node_labels,
            'neutronMetadataAgentConfig',
            ctx.logger,
        )

    async def get_used_resources(
            self,
            ctx: sm.Context,
            ) -> typing.Iterable[sm.ResourceReference]:
        result = list(await super().get_used_resources(ctx))
        try:
            instance = await self._get_current(ctx, True)
        except sm.ResourceNotPresent:
            return result

        pods = sm.pod_interface(ctx.api_client)
        label_selector = sm.LabelSelector(
            match_labels={
                sm.context.LABEL_PARENT_PLURAL: "neutronl3agents",
                sm.context.LABEL_PARENT_GROUP: "network.yaook.cloud",
                sm.context.LABEL_PARENT_NAME: instance["metadata"]["name"],
            }
        )
        for pod in await pods.list_(
                ctx.namespace,
                label_selector=label_selector.as_api_selector()):
            result.extend(api_utils.extract_pod_references(
                pod.spec, ctx.namespace,
            ))
        return result


class TemplatedNeutronL3Agent(
        yaook.statemachine.resources.BodyTemplateMixin,
        NeutronL3Agent):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> yaook.statemachine.resources.TemplateParameters:
        result = await super()._get_template_parameters(ctx, dependencies)
        node_labels = await yaook.op.common.get_node_labels_from_instance(ctx)
        result["vars"]["neutron_config"] = \
            self._collect_neutron_config(ctx, node_labels)
        result["vars"]["neutron_l3_agent_config"] = \
            self._collect_neutron_l3_agent_config(ctx, node_labels)
        result["vars"]["neutron_metadata_agent_config"] = \
            self._collect_neutron_metadata_agent_config(ctx, node_labels)
        return result


class NeutronL3Agents(sm.L2AwareStatefulAgentResource):
    def get_listener(self) -> typing.List[sm.Listener]:
        return [
            sm.KubernetesListener[typing.Mapping](
                "network.yaook.cloud", "v1", "neutronl3agents",
                self._handle_event,
                broadcast=True,
            ),
        ]


class NeutronBGPAgent(sm.SingleObject[typing.Mapping]):
    RELEVANT_KEYS: typing.List[str]
    SETUP_KEY: str
    PARENT_PLURAL: str

    @abc.abstractmethod
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> sm.ResourceInterfaceWithStatus[typing.Mapping]:
        raise NotImplementedError

    def _needs_update(self, old: typing.Mapping, new: typing.Mapping) -> bool:
        old_spec = old["spec"]
        old_spec_pruned = {
            k: v for k, v in old_spec.items()
            if k in self.RELEVANT_KEYS
        }
        new_spec = new["spec"]
        new_spec_pruned = {
            k: v for k, v in new_spec.items()
            if k in self.RELEVANT_KEYS
        }
        return (super()._needs_update(old, new) or
                old_spec_pruned != new_spec_pruned)

    async def get_used_resources(
            self,
            ctx: sm.Context,
            ) -> typing.Iterable[sm.ResourceReference]:
        result = list(await super().get_used_resources(ctx))
        try:
            instance = await self._get_current(ctx, True)
        except sm.ResourceNotPresent:
            return result

        pods = sm.pod_interface(ctx.api_client)
        label_selector = sm.LabelSelector(
            match_labels={
                sm.context.LABEL_PARENT_PLURAL: self.PARENT_PLURAL,
                sm.context.LABEL_PARENT_GROUP: "network.yaook.cloud",
                sm.context.LABEL_PARENT_NAME: instance["metadata"]["name"],
            }
        )
        for pod in await pods.list_(
                ctx.namespace,
                label_selector=label_selector.as_api_selector()):
            result.extend(api_utils.extract_pod_references(
                pod.spec, ctx.namespace,
            ))
        return result

    async def get_node_labels_from_instance(
            self,
            ctx: sm.Context) -> typing.Mapping[str, str]:
        core_client = kclient.CoreV1Api(ctx.api_client)
        data = typing.cast(typing.Mapping, ctx.instance_data)
        return (await core_client.read_node(data["node"])).metadata.labels


class NeutronBGPDRAgent(NeutronBGPAgent):
    RELEVANT_KEYS: typing.List[str] = [
        "hostname",
        "keystoneRef",
        "novaRef",
        "neutronConfig",
        "neutronBGPDRAgentConfig",
        "bgpInterfaceMapping",
        "caConfigMapName",
        "messageQueue",
        "imagePullSecrets",
        "targetRelease",
    ]

    SETUP_KEY: str = "ovs"
    PARENT_PLURAL: str = "neutronbgpdragents"

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> sm.ResourceInterfaceWithStatus[typing.Mapping]:
        return sm.neutron_bgp_dragent_interface(api_client)

    def _collect_interface_mapping(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            config_key: str,
            ) -> typing.Mapping:
        interface_mappings = _get_per_node_special_config(
            ctx.parent_spec["setup"][self.SETUP_KEY].get("bgp", {})
            .get(config_key).get("configTemplates", []),
            node_labels,
            'bgpInterfaceMapping',
            ctx.logger,
        )
        # We only support one interface config per bgp agent
        try:
            primary = interface_mappings[0]
        except IndexError:
            raise sm.ConfigurationInvalid(
                f"bgpInterfaceMapping for bgp agent '{str(config_key)}' is "
                "missing."
            )
        for cfg in interface_mappings[1:]:
            if cfg != primary:
                raise sm.ConfigurationInvalid(
                    "found multiple conflicting bgp interface configurations "
                    f"for {ctx.instance}: {primary} != {cfg}\n%s" % (
                        repr(interface_mappings))
                )
        return interface_mappings[0]

    def _collect_neutron_bgpdr_agent_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            config_key: str,
            ) -> typing.Tuple[
                typing.Collection[typing.Mapping],
                typing.Collection[typing.Mapping]]:
        neutron_configs = _get_per_node_special_config(
            ctx.parent_spec["setup"][self.SETUP_KEY].get("bgp", {})
            .get(config_key).get("configTemplates", []),
            node_labels,
            'neutronConfig',
            ctx.logger,
        )

        bgp_configs = _get_per_node_special_config(
            ctx.parent_spec["setup"][self.SETUP_KEY].get("bgp", {})
            .get(config_key).get("configTemplates", []),
            node_labels,
            'neutronBGPDRAgentConfig',
            ctx.logger,
        )
        return (neutron_configs, bgp_configs)


class NeutronOVNBGPAgent(NeutronBGPAgent):
    RELEVANT_KEYS: typing.List[str] = [
        "addressScopes",
        "bgpNodeAnnotationSuffix",
        "bridgeName",
        "driver",
        "hostname",
        "issuerRef",
        "localAS",
        "lockName",
        "peers",
        "syncInterval",
    ]

    SETUP_KEY: str = "ovn"
    PARENT_PLURAL: str = "neutronovnbgpagents"

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> sm.ResourceInterfaceWithStatus[typing.Mapping]:
        return sm.neutron_ovn_bgp_agent_interface(api_client)

    def _collect_interface_mapping(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            config_key: str,
            ) -> typing.Mapping:
        interface_mappings = _get_per_node_special_config(
            ctx.parent_spec["setup"][self.SETUP_KEY].get("bgp", {})
            .get(config_key).get("configTemplates", []),
            node_labels,
            'bgpInterfaceMapping',
            ctx.logger,
        )
        # We only support one interface config per bgp agent
        try:
            primary = interface_mappings[0]
        except IndexError:
            raise sm.ConfigurationInvalid(
                f"bgpInterfaceMapping for bgp agent '{str(config_key)}' is "
                "missing."
            )
        for cfg in interface_mappings[1:]:
            if cfg != primary:
                raise sm.ConfigurationInvalid(
                    "found multiple conflicting bgp interface configurations "
                    f"for {ctx.instance}: {primary} != {cfg}\n%s" % (
                        repr(interface_mappings))
                )
        return interface_mappings[0]

    def _collect_ovn_bgp_agent_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            config_key: str,
            ) -> typing.List[typing.Mapping]:
        bgp_configs = _get_per_node_special_config(
            ctx.parent_spec["setup"][self.SETUP_KEY].get("bgp", {})
            .get(config_key).get("configTemplates", []),
            node_labels,
            'config',
            ctx.logger,
        )
        return bgp_configs


class TemplatedNeutronBGPDRAgent(
        yaook.statemachine.resources.BodyTemplateMixin,
        NeutronBGPDRAgent):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> yaook.statemachine.resources.TemplateParameters:
        result = await super()._get_template_parameters(ctx, dependencies)
        node_labels = await self.get_node_labels_from_instance(ctx)
        config_key = typing.cast(
            typing.Mapping,
            ctx.instance_data,
        )["configkey"]
        neutron_config, bgp_config = self._collect_neutron_bgpdr_agent_config(
            ctx,
            node_labels,
            config_key,
        )

        result["vars"]["config_key"] = config_key
        result["vars"]["neutron_config"] = neutron_config
        result["vars"]["neutron_bgp_dragent_config"] = bgp_config
        result["vars"]["bgp_interface_mapping"] = \
            self._collect_interface_mapping(ctx, node_labels, config_key)
        return result


class TemplatedNeutronOVNBGPAgent(
        yaook.statemachine.resources.BodyTemplateMixin,
        NeutronOVNBGPAgent):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> yaook.statemachine.resources.TemplateParameters:
        result = await super()._get_template_parameters(ctx, dependencies)
        node_labels = await self.get_node_labels_from_instance(ctx)
        config_key = typing.cast(
            typing.Mapping,
            ctx.instance_data,
        )["configkey"]

        config = self._collect_ovn_bgp_agent_config(
            ctx,
            node_labels,
            config_key,
        ).pop()

        result["vars"]["address_scopes"] = config.get("addressScopes", [])
        result["vars"]["peers"] = config["peers"]
        result["vars"]["config_key"] = config_key
        result["vars"]["driver"] = config["driver"]
        result["vars"]["bgp_local_as"] = config["localAS"]
        result["vars"]["bridge_name"] = config["bridgeName"]
        result["vars"]["sync_interval"] = config["syncInterval"]
        return result


class NeutronBGPAgents(sm.L2AwareStatefulAgentResource):
    def __init__(
        self,
        *,
        scheduling_keys: typing.Collection[str],
        setup_key: str,
        **kwargs: typing.Any
    ):
        super().__init__(
            scheduling_keys=scheduling_keys,
            **kwargs)
        self.setup_key = setup_key
        self.listener_key = (
            "neutronovnbgpagents"
            if setup_key == "ovn"
            else "neutronbgpdragents"
        )

    def get_listener(self) -> typing.List[sm.Listener]:
        return [
            sm.KubernetesListener[typing.Mapping](
                "network.yaook.cloud", "v1", self.listener_key,
                self._handle_event,
                broadcast=True,
            ),
        ]

    async def get_target_instances(
            self,
            ctx: sm.Context,
            ) -> typing.Mapping[str, typing.Any]:
        nodes = await self._get_target_nodes(ctx)
        configkeys = await _bgp_spec_configkeys(ctx, self.setup_key)
        lockNames_check = set()
        target_instances = {}
        for configkey in configkeys:
            # Kubernetes annotations are split into (optional) prefix/name.
            # The bgp annotation name used by the bgp-operator for the l2-lock
            # is "bgp-<configkey>". The maximum annotation name length is 63
            # characters (https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/#syntax-and-character-set).   # noqa E501
            # That means that the <configkey>-part of the name must have a
            # maximum length of 59 characters
            strippedConfigkey = configkey[:59].rstrip("-_.")
            lockName = "bgp-" + strippedConfigkey
            # here we check if we cut at an unfortunate position and a
            # non-alphanumeric character is at the last position (this is
            # invalid for kubernetes)
            if not api_utils.is_k8s_regex_valid(lockName):
                raise ConfigurationInvalid(
                    f"The lockName {lockName} does not match the k8s regex "
                    "validation"
                )
            # We add the lockName to a set and double check at the end
            # if the length of the configkeys and the check-set are equal.
            lockNames_check.add(lockName)
            for node in nodes:
                # We need to cut the target instances to 63 characters or the
                # generating of the bgpdr agent cr will fail at the lookup of
                # the ca certs with an instance because the instance is too
                # long.

                # The names for the target instances have the configkey and the
                # node name seperated by a '.' in it (for useability /
                # readability).
                # The maximum length for cr-names is 63 characters (by
                # kubernetes) that's why we also need to check if our
                # combinations of configkey + '.' + hostname are unique when
                # cut at the 63th character
                bgp_agent_name = (
                    strippedConfigkey + '.' + node.metadata.name
                )[:63].rstrip("-_.")
                # here we check if we cut at an unfortunate position and a
                # non-alphanumeric character is at the last position (this is
                # invalid for kubernetes)
                if not api_utils.is_k8s_regex_valid(bgp_agent_name):
                    raise ConfigurationInvalid(
                        "The bgp agent name shortened to 63 characters "
                        "does not match the k8s regex. Wrong name: "
                        f"{bgp_agent_name[:63]}"
                    )
                # If the bgp_agent_name is valid we add it to the
                # target_instances.
                target_instances[bgp_agent_name] = {
                    "node": node.metadata.name,
                    "configkey": configkey,
                    "lockName": lockName,
                }
        # Check if the number of configkeys is equal to the number of
        # lockNames
        if len(configkeys) != len(lockNames_check):
            raise ConfigurationInvalid(
                "The configkeys for the bgp agents are equal till the 59th "
                "character and cannot be created. Please use names that "
                "differ in the first 59 characters."
                # good config keys : firstbgp, secondbgp, thirdbgp
                # bad config keys : mybgpagent1, mybgpagent2, mybgpagent3
            )
        # We should get count_of_specified_bgp_agents (configkeys) multiplied
        # by count_of_nodes (nodes) target_instances. If not then something
        # went wrong and we should not procceed.
        if (len(nodes)*len(configkeys)) != len(target_instances):
            raise ConfigurationInvalid(
                "The configkeys and the node names do not differ when put "
                "together with a point in between. Use shorter keys so "
                "when combining the config keys with your node names they "
                "differ in the first 64 characters."
            )
        return target_instances

    def _does_node_require_maintenance(
            self,
            ctx: sm.Context,
            ) -> bool:
        if ctx.instance_data is None:
            raise AssertionError()
        nodes = sm.resources.openstack.nodes_in_maintenance.get()
        for node in nodes:
            if node == ctx.instance_data["node"]:
                return True

        return False

    async def _group_instances(
            self,
            ctx: context.Context,
            instances: typing.Mapping[str, InstanceState]
            ) -> typing.List[typing.Tuple[
                InstanceGroup, typing.Mapping[str, InstanceState]]]:
        group_instances: typing.List[typing.Tuple[
            InstanceGroup, typing.Mapping[str, InstanceState]]] = []
        config_keys = await _bgp_spec_configkeys(ctx, self.setup_key)
        for config_key in config_keys:
            configkey_instances = {}
            for instance, instance_state in instances.items():
                if instance.startswith(config_key + "."):
                    configkey_instances[instance] = instance_state

            # Note: this will create the same disruption budgets for each
            # config key
            group_instances_per_config = await super()._group_instances(
                ctx, configkey_instances)
            for group_instance in group_instances_per_config:
                group_instance[0].field_manager = "%s-%s-%s" % (
                    config_key, ctx.parent_kind, self.component)
            group_instances.extend(group_instances_per_config)

        return group_instances


class OVSDBAwareDeployment(sm.TemplatedDeployment):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.resources.DependencyMap
            ) -> sm.resources.TemplateParameters:
        param = await super()._get_template_parameters(ctx, dependencies)
        cluster_domain = api_utils.get_cluster_domain()
        for db_schema in ['nb', 'sb']:
            service_depencency = typing.cast(
                sm.Service,
                dependencies[f"ovsdb_{db_schema}_service"],
            )
            service_ref = \
                await service_depencency.get_all(ctx)
            ovsdb_service_list = []
            if db_schema == 'nb':
                port = yaook.op.common.OVSDB_NORTHBOUND_PORT
            elif db_schema == 'sb':
                port = yaook.op.common.OVSDB_SOUTHBOUND_PORT
            for ovsdb_pod_service in service_ref.values():
                ovsdb_service_list.append(
                    # FIXME: Make protocol user configurable
                    f"ssl:{ovsdb_pod_service.name}."
                    f"{ovsdb_pod_service.namespace}.svc."
                    f"{cluster_domain}:{port}"
                )
            param[f"ovsdb_{db_schema}"] = \
                ",".join(ovsdb_service_list)
        return param
