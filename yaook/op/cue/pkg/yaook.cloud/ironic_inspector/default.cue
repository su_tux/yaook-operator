// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ironic_inspector

import (
	"yaook.cloud/ironic_inspector_template"
)

#standalone_ironic_conf_template: ironic_inspector_template.ironic_inspector_template_conf_spec
#standalone_ironic_conf_template: {
	// NOTE: The sections in this template must be included manually in the
	// yaook/op/infra_ironic/cr.py due to restrictions in our cue handling.
	// (normally, you'd want to include #standalone_ironic_conf_template as
	// another definition to form the config, but we don't have any means to
	// do that)
	pxe_filter: {
		driver: "noop"
	}
	processing: {
		add_ports:                 "pxe"
		keep_ports:                "present"
		always_store_ramdisk_logs: true
		store_data:                "database"
		node_not_found_hook:       "enroll"
		power_off:                 false
	}
	discovery: {
		enroll_node_driver: "ipmi"
	}
}

ironic_inspector_conf_spec: ironic_inspector_template.ironic_inspector_template_conf_spec
ironic_inspector_conf_spec: {
	DEFAULT: {
		listen_address: "127.0.0.1"
		listen_port:    8080
		transport_url:  "fake://"
	}
	database: {
		#connection_host:        *"mysql-keystone-mariadb" | string
		#connection_port:        *3306 | int
		#connection_username:    *"keystone" | string
		#connection_password:    string
		#connection_database:    *"keystone" | string
		#connection_ssl_ca_path: *"/etc/pki/tls/certs/ca-bundle.crt" | string
		connection:              *"mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )" | string

		// This needs to be changed in lockstep with the timeoutClient in the
		// database.yaml in the keystone operator. connection_recycle_time
		// should always be ~10% smaller than the haproxy timeoutClient.
		connection_recycle_time: 280
	}
	ironic: {
		auth_url:   string
		"auth-url": auth_url
	}
}
