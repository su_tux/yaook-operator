// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package nova

import (
	"strings"
	"yaook.cloud/nova_template"
)

nova_conf_spec: nova_template.nova_template_conf_spec
nova_conf_spec: {
	api_database: {
		#connection_host:        *"mysql-novaapi-mariadb" | string
		#connection_port:        *3306 | int
		#connection_username:    *"novaapi" | string
		#connection_password:    string
		#connection_database:    *"novaapi" | string
		#connection_ssl_ca_path: *"/etc/ssl/certs/ca-bundle.crt" | string
		connection:              *"mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )" | string

		// This needs to be changed in lockstep with the timeoutClient in the
		// database.yaml in the nova operator. connection_recycle_time
		// should always be ~10% smaller than the haproxy timeoutClient.
		connection_recycle_time: 280
	}
	cinder: {
		auth_url:       string
		"auth-url":     auth_url
		cafile:         keystone_authtoken.cafile
		catalog_info:   "volumev3:cinderv3:internalURL"
		region_name:    keystone_authtoken.region_name
		os_region_name: region_name
	}
	database: {
		#connection_host:        *"mysql-nova-mariadb" | string
		#connection_port:        *3306 | int
		#connection_username:    *"nova" | string
		#connection_password:    string
		#connection_database:    *"nova" | string
		#connection_ssl_ca_path: *"/etc/ssl/certs/ca-bundle.crt" | string
		connection:              *"mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )" | string

		// This needs to be changed in lockstep with the timeoutClient in the
		// database.yaml in the nova operator. connection_recycle_time
		// should always be ~10% smaller than the haproxy timeoutClient.
		connection_recycle_time: 280
	}
	DEFAULT: {
		osapi_compute_listen:      "127.0.0.1"
		osapi_compute_listen_port: 8080
		metadata_listen:           "127.0.0.1"
		metadata_listen_port:      8080
		#transport_url_hosts:      *["rabbitmq-nova"] | [...string]
		#transport_url_port:       *5671 | int
		#transport_url_username:   *"admin" | string
		#transport_url_password:   string
		#transport_url_vhost:      *"" | string
		#transport_url_parts: [ for Host in #transport_url_hosts {"\( #transport_url_username ):\( #transport_url_password )@\( Host ):\( #transport_url_port )"}]
		transport_url: "rabbit://\( strings.Join(#transport_url_parts, ",") )/\( #transport_url_vhost )"
		use_stderr:    *true | bool
		use_json:      *true | bool
		policy_file:   *"/etc/nova/policy.yaml" | "/etc/nova/policy.json"
	}
	keystone_authtoken: {
		auth_url:            string
		cafile:              "/etc/ssl/certs/ca-bundle.crt"
		"auth-url":          auth_url
		auth_type:           "password"
		project_domain_name: string
		user_domain_name:    string
		project_name:        string
		username:            string
		password:            string
		service_token_roles: *["admin"] | [string, ...]
	}
	keystone: {
		keystone_authtoken
	}
	neutron: {
		auth_url:                     string
		"auth-url":                   auth_url
		cafile:                       keystone_authtoken.cafile
		service_metadata_proxy:       true
		metadata_proxy_shared_secret: string
		region_name:                  keystone_authtoken.region_name
		os_region_name:               region_name
	}
	placement: {
		os_region_name:      *"RegionOne" | string
		auth_url:            string
		cafile:              keystone_authtoken.cafile
		"auth-url":          auth_url
		auth_type:           "password"
		project_domain_name: string
		user_domain_name:    string
		project_name:        string
		username:            string
		password:            string
	}
	scheduler: {
		discover_hosts_in_cells_interval: 60
	}
	service_user: {
		send_service_user_token: *true | bool
		keystone_authtoken
	}
	upgrade_levels: {
		compute: "auto"
	}
	vnc: {
		enabled: true
		auth_schemes: ["vencrypt"]
		vncserver_listen:     "0.0.0.0"
		novncproxy_port:      8080
		vencrypt_client_key:  "/etc/pki/nova-novncproxy/client-key.pem"
		vencrypt_client_cert: "/etc/pki/nova-novncproxy/client-cert.pem"
		vencrypt_ca_certs:    "/etc/pki/nova-novncproxy/ca-cert.pem"
	}
	wsgi: {
		api_paste_config: "/usr/local/etc/nova/api-paste.ini"
	}
	oslo_messaging_rabbit: {
		ssl:                 true
		ssl_ca_file:         "/etc/ssl/certs/ca-bundle.crt"
		amqp_durable_queues: true
		rabbit_quorum_queue: true
	}
	oslo_policy: {
		policy_file:          *"/etc/nova/policy.yaml" | "/etc/nova/policy.json"
		enforce_new_defaults: false
		enforce_scope:        false
	}
	oslo_middleware: {
		enable_proxy_headers_parsing: *true | bool
	}
	barbican: {
		barbican_endpoint_type: *"internal" | string
	}
}
