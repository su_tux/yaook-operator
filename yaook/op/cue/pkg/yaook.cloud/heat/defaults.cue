// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package heat

import (
	"strings"
	"yaook.cloud/heat_template"
)

heat_conf_spec: heat_template.heat_template_conf_spec
heat_conf_spec: {
	DEFAULT: {
		#transport_url_hosts:    *["rabbitmq-heat"] | [...string]
		#transport_url_port:     *5671 | int
		#transport_url_username: *"admin" | string
		#transport_url_password: string
		#transport_url_vhost:    *"" | string
		#transport_url_parts: [ for Host in #transport_url_hosts {"\( #transport_url_username ):\( #transport_url_password )@\( Host ):\( #transport_url_port )"}]
		transport_url:           "rabbit://\( strings.Join(#transport_url_parts, ",") )/\( #transport_url_vhost )"
		#connection_host:        *"mysql-heat-mariadb" | string
		#connection_port:        *3306 | int
		#connection_username:    *"heat" | string
		#connection_password:    string
		#connection_database:    *"heat" | string
		#connection_ssl_ca_path: *"/etc/pki/tls/certs/ca-bundle.crt" | string
		sql_connection:          "mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )"
		use_stderr:              *true | bool
		use_json:                *true | bool
		api_paste_config:        "/etc/heat/api-paste.ini"
		policy_file:             "/etc/heat/policy.json"
		db_auto_create:          false

		// This needs to be changed in lockstep with the timeoutClient in the
		// database.yaml in the heat operator. sql_idle_timeout
		// should always be ~10% smaller than the haproxy timeoutClient.
		sql_idle_timeout: 280
	}
	heat_api: {
		bind_host: "127.0.0.1"
		bind_port: 8080
	}
	heat_api_cfn: {
		bind_host: "127.0.0.1"
		bind_port: 8080
	}
	keystone_authtoken: {
		www_authenticate_uri: *"https://keystone:5000/v3" | string
		auth_url:             *"https://keystone:5000/v3" | string
		auth_uri:             *"https://keystone:5000/v3" | string
		cafile:               "/etc/pki/tls/certs/ca-bundle.crt"
		auth_type:            "password"
		project_domain_name:  string
		user_domain_name:     string
		project_name:         string
		username:             string
		password:             string
		service_token_roles:  *["admin"] | [string, ...]
	}
	clients: {
		endpoint_type: keystone_authtoken.valid_interfaces[0]
	}
	trustee: {
		auth_type: "password"
		auth_url:  *"https://keystone:5000/v3" | string
		auth_uri:  *"https://keystone:5000/v3" | string
		username:  string
		password:  string
	}
	oslo_middleware: {
		enable_proxy_headers_parsing: *true | bool
	}
	oslo_messaging_rabbit: {
		ssl:                 true
		ssl_ca_file:         "/etc/pki/tls/certs/ca-bundle.crt"
		amqp_durable_queues: true
		rabbit_quorum_queue: true
	}
	oslo_policy: {
		policy_file: "/etc/heat/policy.json"
	}
}
