#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import environ
import typing

import kubernetes_asyncio.client as kclient

import yaook.op.common
import yaook.statemachine as sm
import yaook.statemachine.resources.orchestration as orchestration
# TODO(resource-refactor): clean up imports
import yaook.statemachine.resources.openstack as resource

from . import resources as nc_resources


def _uses_ceph(ctx: sm.Context) -> bool:
    return ctx.parent_spec.get("cephBackend", {}).get("enabled", False)


# This can be removed after 2023-03-31 as we can then assume that
# `memcachedRef` has been rolled out everywhere
def _test_has_memcached_ref(ctx: sm.Context) -> bool:
    return "memcachedRef" in ctx.parent_spec


class MemcachedReference(sm.ForeignReference):
    def __init__(self, **kwargs):
        super().__init__(
            resource_interface_factory=sm.memcachedservice_interface,
            **kwargs,
        )

    def get_resource_references(
            self,
            ctx: sm.Context,
            ) -> typing.Collection[
                kclient.V1ObjectReference
            ]:
        return [kclient.V1ObjectReference(
            name=ctx.parent_spec["memcachedRef"]["name"],
            namespace=ctx.namespace,
        )]


class EmptySSHIdentity(sm.SSHIdentity):
    def __init__(
            self,
            *,
            metadata: sm.MetadataProvider,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata = metadata

    async def _make_body(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> sm.ResourceBody:
        return {
            "apiVersion": "compute.yaook.cloud/v1",
            "kind": "SSHIdentity",
            "metadata": sm.evaluate_metadata(ctx, self._metadata),
            "status": {"keys": {
                "host": [],
                "user": [],
            }},
        }

    def _needs_update(
            self,
            current: typing.Mapping,
            new: typing.Mapping) -> bool:
        return False


@environ.config(prefix="YAOOK_NOVA_COMPUTE_OP")
class OpNovaComputeConfig:
    interface = environ.var("internal")
    job_image = environ.var()


AMQP_USERNAME = "compute-{name}"


class NovaComputeNode(sm.CustomResource):
    API_GROUP = "compute.yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "novacomputenodes"
    KIND = "NovaComputeNode"
    ADDITIONAL_PERMISSIONS = (
        # The jobs management is hidden behind the ComputeStateResource and
        # thus cannot be auto-discovered by the CLI tool.
        (False, "batch", "jobs", {"create", "list", "watch", "get", "delete",
                                  "patch", "update", "deletecollection"}),
        # We need to pass a few permissions to the job itself
        (False, "compute.yaook.cloud", "novacomputenodes/status",
         {"get", "patch", "replace", "update"}),
        # We need to pass on permissions to patch the status of sshidentities
        # to the role of the compute node pod itself.
        (False, "compute.yaook.cloud", "sshidentities/status", {"patch"}),
        # Required to read the node state for judging the next steps in
        # ComputeStateResource
        (True, "", "nodes", {"get", "patch", "list"}),
        (True, "", "pods/exec", {"create", "get"}),
        (True, "rbac.authorization.k8s.io", "clusterrolebindings",
         {"create", "list", "get", "patch", "delete", "deletecollection"}),  # noqa: E501
        (True, "rbac.authorization.k8s.io", "clusterroles",
         {"create", "list", "get", "patch", "delete", "deletecollection"})  # noqa: E501
    )
    RELEASES = [
        "queens",
        "train",
        "yoga"
    ]
    VALID_UPGRADE_TARGETS: typing.List[str] = []

    neutron_l2_lock = orchestration.L2Lock()

    nova_compute_docker_image = yaook.op.common.image_dependencies(
        "nova-compute/nova-compute-{release}-ubuntu",
        RELEASES,
    )

    keystone = sm.KeystoneReference()
    keystone_internal_api = yaook.op.common.keystone_api_config_reference(
        keystone,
    )
    keystone_operator_api = yaook.op.common.keystone_api_config_reference(
        keystone,
    )

    mq = sm.AMQPServerRef()
    mq_service = sm.ForeignResourceDependency(
        foreign_resource=mq,
        foreign_component=yaook.op.common.AMQP_SERVER_SERVICE_COMPONENT,
        resource_interface_factory=sm.service_interface,
    )
    mq_user_password = sm.AutoGeneratedPassword(
        metadata=("nova-cell1-mq-compute-user-", True),
    )
    mq_user = sm.SimpleAMQPUser(
        metadata=("nova-cell1-compute-", True),
        server=mq,
        username_format=AMQP_USERNAME,
        password_secret=mq_user_password,
    )

    memcached = MemcachedReference()
    memcached_statefulset = sm.OptionalKubernetesReference(
        condition=_test_has_memcached_ref,
        wrapped_state=sm.ForeignResourceDependency(
            resource_interface_factory=sm.stateful_set_interface,
            foreign_resource=memcached,
            foreign_component=yaook.op.common.MEMCACHED_STATEFUL_COMPONENT,
        )
    )

    user = sm.StaticKeystoneUserWithParent(
        username="nova-compute",
        keystone=keystone
    )
    user_password = yaook.op.common.keystone_user_credentials_reference(
        user
    )

    ceph_config = sm.Optional(
        condition=_uses_ceph,
        wrapped_state=nc_resources.CephConfigResource(
            metadata=("nova-compute-ceph-", True),
            copy_on_write=True,
            add_cue_layers=[
                nc_resources.CephConfigLayer(),
            ],
        ),
    )

    job_role = sm.TemplatedRole(
        component="job_role",
        template="job-role.yaml",
        params={
            "password_secret_name":
            yaook.op.common.NOVA_COMPUTE_OPERATOR_USER_SECRET,
        },
    )

    job_clusterrole = sm.TemplatedClusterRole(
        component="job_clusterrole",
        template="job-clusterrole.yaml",
    )

    # The k8s RBAC layer does not allow restricting creation of objects by
    # name. We want to restrict the compute service account to only writing to
    # a specific secret though. Hence, we have to pre-create it and then
    # only grant `patch` permissions on this object to the service account.
    public_keys = EmptySSHIdentity(
        component=yaook.op.common.NOVA_COMPUTE_PUBLIC_KEYS_COMPONENT,
        metadata=lambda ctx: (f"{ctx.parent_name}-", True),
    )

    compute_role = sm.TemplatedRole(
        component="compute_role",
        template="compute-role.yaml",
        params={
            "password_secret_name":
            yaook.op.common.NOVA_COMPUTE_OPERATOR_USER_SECRET,
        },
        add_dependencies=[public_keys],
    )

    compute_config = sm.CueSecret(
        metadata=("nova-compute-", True),
        copy_on_write=True,
        add_cue_layers=[
            nc_resources.SpecSequenceLayer(),
            nc_resources.VNCLayer(),
            sm.KeystoneAuthLayer(
                target="novacompute",
                endpoint_config=keystone_internal_api,
                credentials_secret=user_password,
            ),
            sm.KeystoneAuthLayer(
                target="novacompute",
                config_section="cinder",
                endpoint_config=keystone_internal_api,
                credentials_secret=user_password,
            ),
            sm.KeystoneAuthLayer(
                target="novacompute",
                config_section="neutron",
                endpoint_config=keystone_internal_api,
                credentials_secret=user_password,
            ),
            sm.KeystoneAuthLayer(
                target="novacompute",
                config_section="placement",
                endpoint_config=keystone_internal_api,
                credentials_secret=user_password,
            ),
            sm.OptionalLayer(
                condition=_test_has_memcached_ref,
                wrapped_layer=sm.MemcachedConnectionLayer(
                    target="novacompute",
                    memcached_sfs=memcached_statefulset,
                ),
            ),
            sm.AMQPTransportLayer(
                target="novacompute",
                service=mq_service,
                username=lambda ctx: AMQP_USERNAME.format(
                    name=ctx.parent_name),
                password_secret=mq_user_password,
            ),
            sm.RegionNameConfigLayer(
                target="novacompute",
                allow_not_defined=True
            ),
            sm.RegionNameConfigLayer(
                target="novacompute",
                config_section="glance",
                allow_not_defined=True
            ),
            sm.RegionNameConfigLayer(
                target="novacompute",
                config_section="cinder",
                allow_not_defined=True
            ),
            sm.RegionNameConfigLayer(
                target="novacompute",
                config_section="neutron",
                allow_not_defined=True
            ),
            sm.RegionNameConfigLayer(
                target="novacompute",
                config_section="placement",
                allow_not_defined=True
            ),
        ],
        add_dependencies=[
            mq_user,
        ],
    )

    compute_service = sm.TemplatedService(
        template="compute-service.yaml",
    )

    job_service_account = sm.TemplatedServiceAccount(
        component="job_service_account",
        template="serviceaccount.yaml",
        params={
            "suffix": "job",
        },
        add_dependencies=[compute_service],
    )

    job_role_binding = sm.TemplatedRoleBinding(
        template="rolebinding.yaml",
        copy_on_write=True,
        params={
            "role": job_role.component,
            "service_account": job_service_account.component,
            "name_prefix": "compute-job-",
        },
        add_dependencies=[job_role, job_service_account],
    )

    job_clusterrole_binding = sm.TemplatedClusterRoleBinding(
        template="clusterrolebinding.yaml",
        copy_on_write=True,
        params={
            "cluster_role": job_clusterrole.component,
            "service_account": job_service_account.component,
            "name_prefix": "compute-job-",
        },
        add_dependencies=[job_clusterrole, job_service_account],
    )

    compute_service_account = sm.TemplatedServiceAccount(
        component="compute_service_account",
        template="serviceaccount.yaml",
        params={
            "suffix": "compute",
        },
        add_dependencies=[compute_service],
    )

    compute_role_binding = sm.TemplatedRoleBinding(
        template="rolebinding.yaml",
        copy_on_write=True,
        params={
            "role": compute_role.component,
            "service_account": compute_service_account.component,
            "name_prefix": "compute-node-",
        },
        add_dependencies=[compute_role, compute_service_account],
    )

    vnc_backend_certificate_secret = sm.EmptyTlsSecret(
        metadata=lambda ctx: (f"{ctx.parent_name}-vnc-", True),
    )

    vnc_backend_certificate = sm.TemplatedCertificate(
        template="vnc-backend-certificate.yaml",
        add_dependencies=[vnc_backend_certificate_secret, compute_service],
    )

    ready_vnc_backend_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=vnc_backend_certificate,
    )

    compute = resource.TemplatedRecreatingStatefulSet(
        template="compute-statefulset.yaml",
        add_dependencies=[
            compute_service,
            compute_config,
            ceph_config,
            compute_service_account,
            public_keys,
            ready_vnc_backend_certificate_secret,
            neutron_l2_lock,
        ],
        versioned_dependencies=[
            nova_compute_docker_image
        ],
        params={
            "tolerations": list(map(
                sm.api_utils.make_toleration,
                [
                    yaook.op.common.SchedulingKey.COMPUTE_HYPERVISOR.value,
                ],
            )),
        },
    )
    compute_pdb = sm.DisallowedPodDisruptionBudget(
         metadata=lambda ctx: (f"nova-{ctx.parent_name}-pdb-", True),
         replicated=compute,
    )

    api_state = nc_resources.ComputeStateResource(
        finalizer="compute.yaook.cloud/nova-compute-node",
        endpoint_config=keystone_operator_api,
        job_endpoint_config=keystone_internal_api,
        credentials_secret=user_password,
        scheduling_keys=[
            yaook.op.common.SchedulingKey.OPERATOR_ANY.value,
            yaook.op.common.SchedulingKey.OPERATOR_NOVA.value,
        ],
        eviction_job_template="eviction-job.yaml",
        add_dependencies=[job_service_account],
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)
        config = environ.to_config(OpNovaComputeConfig)
        self.keystone_operator_api._foreign_component = {
            "internal": yaook.op.common.KEYSTONE_INTERNAL_API_COMPONENT,
            "public": yaook.op.common.KEYSTONE_PUBLIC_API_COMPONENT,
        }[config.interface]
        self.api_state._params["job_image"] = config.job_image


sm.register(NovaComputeNode)
