#!/usr/bin/env python3
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import typing
import yaml

import yaook
import yaook.op.common
import yaook.common.config
import yaook.op.tasks
import yaook.statemachine as sm


JOB_SCHEDULING_KEYS = [
    yaook.op.common.SchedulingKey.OPERATOR_CEILOMETER.value,
    yaook.op.common.SchedulingKey.OPERATOR_ANY.value,
]

COMPUTE_SCHEDULING_KEYS = [
    yaook.op.common.SchedulingKey.COMPUTE_HYPERVISOR.value
]

MQ_NAME = "ceilometer"

API_SVC_USERNAME = "api"
CEILOMETER_COMPUTE_SVC_USERNAME_FORMAT = "ceilometer-compute-{instance}"


class EventDefinitionConfig(sm.ConfigMap):
    def __init__(self,
                 *,
                 metadata: sm.MetadataProvider,
                 file_name: str,
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata = metadata
        self._file_name = file_name

    async def _make_body(self,
                         ctx: sm.Context,
                         dependecies: sm.DependencyMap
                         ) -> sm.ResourceBody:
        data = {
            self._file_name:
            str(yaml.dump(ctx.parent_spec.get('ceilometerEventDefinitions',
                [])))
        }

        return {
            "apiVersion": "v1",
            "kind": "ConfigMap",
            "metadata": sm.evaluate_metadata(ctx, self._metadata),
            "data": data,
        }


class Ceilometer(sm.ReleaseAwareCustomResource):
    API_GROUP = "yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "ceilometerdeployments"
    KIND = "CeilometerDeployment"
    RELEASES = [
        "queens",
        "train",
        "yoga",
    ]
    VALID_UPGRADE_TARGETS: typing.List[str] = []

    ADDITIONAL_PERMISSIONS = (
        # To get the secret of the issuerRef
        (False, "cert-manager.io", "issuers", {"get"}),
    )

    ceilometer_docker_image = yaook.op.common.image_dependencies(
        "ceilometer/ceilometer-{release}",
        RELEASES,
    )
    ceilometer_notification_docker_image = yaook.op.common.image_dependencies(
        "ceilometer-agent-notification/"
        "ceilometer-agent-notification-{release}",
        RELEASES,
    )
    ssl_terminator_image = sm.ConfigurableVersionedDockerImage(
        'ssl-terminator/ssl-terminator',
        sm.YaookSemVerSelector(),
    )
    rabbitmq_image = sm.VersionedDockerImage(
        "library/rabbitmq",
        sm.SemVerSelector([">=3.8.0", "<4.0.0"], suffix="-management"),
    )
    memcached_image = sm.VersionedDockerImage(
        "bitnami/memcached",
        sm.BitnamiVersionSelector([
            ([">=1.6.10", "<2.0.0"], []),
        ]),
    )

    keystone = sm.KeystoneReference()
    keystone_internal_api = yaook.op.common.keystone_api_config_reference(
        keystone,
    )

    keystone_user = sm.StaticKeystoneUser(
        keystone=keystone,
        username="ceilometer",
    )
    keystone_user_credentials = \
        yaook.op.common.keystone_user_credentials_reference(
            keystone_user
        )

    mq = sm.TemplatedAMQPServer(
        template="message-queue.yaml",
        params={
            "mq_name": MQ_NAME,
        },
        versioned_dependencies=[rabbitmq_image],
    )
    mq_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=mq,
        foreign_component=yaook.op.common.AMQP_SERVER_SERVICE_COMPONENT,
    )

    mq_api_user_password = sm.AutoGeneratedPassword(
        metadata=("ceilometer-mq-user-", True),
        copy_on_write=True,
    )
    mq_api_user = sm.SimpleAMQPUser(
        metadata=("ceilometer-api-", True),
        server=mq,
        username_format=API_SVC_USERNAME,
        password_secret=mq_api_user_password,
    )

    memcached = sm.TemplatedMemcachedService(
        template="memcached.yaml",
        versioned_dependencies=[memcached_image],
    )
    memcached_statefulset = sm.ForeignResourceDependency(
        resource_interface_factory=sm.stateful_set_interface,
        foreign_resource=memcached,
        foreign_component=yaook.op.common.MEMCACHED_STATEFUL_COMPONENT,
    )

    ca_certs = sm.CAConfigMap(
        metadata=("ceilometer-certificates-", True),
        usercerts_spec_key="caCertificates",
        issuer_ref="issuerRef",
    )

    config = sm.CueSecret(
        metadata=("ceilometer-config-", True),
        copy_on_write=True,
        add_cue_layers=[
            sm.SpecLayer(
                target="ceilometer",
                accessor="ceilometerConfig",
            ),
            sm.KeystoneAuthLayer(
                target="ceilometer",
                credentials_secret=keystone_user_credentials,
                endpoint_config=keystone_internal_api,
                config_section="service_credentials",
            ),
            sm.MemcachedConnectionLayer(
                target="ceilometer",
                memcached_sfs=memcached_statefulset,
            ),
            sm.AMQPTransportLayer(
                target="ceilometer",
                service=mq_service,
                username=API_SVC_USERNAME,
                password_secret=mq_api_user_password,
            ),
            sm.MultivaluedSecretInjectionLayer(
                target="ceilometer",
                accessor=lambda ctx: ctx.parent_spec.get(
                    "ceilometerSecrets", []),
            ),
            sm.SpecLayer(
                target="ceilometer_event_pipeline",
                accessor="ceilometerEventPipeline",
                flavor=yaook.common.config.YAML_CONFIG,
            ),
            sm.SpecLayer(
                target="ceilometer_pipeline",
                accessor="ceilometerPipeline",
                flavor=yaook.common.config.YAML_CONFIG,
            ),
            sm.SecretInjectionLayer(
                target="panko",
                accessor=lambda ctx: ctx.parent_spec.get(
                    "pankoConfig", []),
            ),
        ]
    )

    event_definition_config = EventDefinitionConfig(
        metadata=lambda ctx: (f"{ctx.parent_name}-event-definitions-", True),
        copy_on_write=True,
        file_name="event_definitions.yaml"
    )

    upgrade = sm.TemplatedJob(
        template="ceilometer-job-upgrade.yaml",
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[config, ca_certs],
        versioned_dependencies=[ceilometer_docker_image],
    )
    notification_deployment = sm.TemplatedDeployment(
        template="ceilometer-deployment-notification.yaml",
        scheduling_keys=[
            yaook.op.common.SchedulingKey.CEILOMETER_ANY_SERVICE.value,
            yaook.op.common.SchedulingKey.CEILOMETER_NOTIFICATION.value,
        ],
        add_dependencies=[config, ca_certs, event_definition_config, upgrade],
        versioned_dependencies=[ceilometer_notification_docker_image],
    )
    notification_deployment_pdb = sm.QuorumPodDisruptionBudget(
         metadata=("ceilometer-agent-notification-pdb-", True),
         replicated=notification_deployment,
    )

    central_deployment = sm.TemplatedDeployment(
        template="ceilometer-deployment-central.yaml",
        scheduling_keys=[
            yaook.op.common.SchedulingKey.CEILOMETER_ANY_SERVICE.value,
            yaook.op.common.SchedulingKey.CEILOMETER_CENTRAL.value,
        ],
        add_dependencies=[config, ca_certs, upgrade],
        versioned_dependencies=[ceilometer_docker_image],
    )
    central_deployment_pdb = sm.QuorumPodDisruptionBudget(
         metadata=("ceilometer-agent-central-pdb-", True),
         replicated=central_deployment,
    )

    mq_ceilometer_compute_user_passwords = sm.PerNode(
        scheduling_keys=COMPUTE_SCHEDULING_KEYS,
        wrapped_state=sm.AutoGeneratedPassword(
            metadata=("ceilometer-compute-mq-user-", True),
        ),
    )
    mq_ceilometer_compute_user = sm.PerNode(
        scheduling_keys=COMPUTE_SCHEDULING_KEYS,
        wrapped_state=sm.SimpleAMQPUser(
            metadata=("ceilometer-compute-", True),
            server=mq,
            username_format=CEILOMETER_COMPUTE_SVC_USERNAME_FORMAT,
            password_secret=mq_ceilometer_compute_user_passwords,
        ),
    )
    ceilometer_compute_keystone_users = sm.PerNode(
        scheduling_keys=COMPUTE_SCHEDULING_KEYS,
        wrapped_state=sm.InstancedKeystoneUser(
            prefix="ceilometer-compute",
            keystone=keystone,
        )
    )
    ceilometer_compute_keystone_credentials = \
        yaook.op.common.keystone_user_credentials_reference(
            ceilometer_compute_keystone_users,
        )
    compute_configs = sm.PerNode(
        scheduling_keys=COMPUTE_SCHEDULING_KEYS,
        wrapped_state=sm.CueSecret(
            metadata=("ceilometer-compute-config-", True),
            copy_on_write=True,
            add_cue_layers=[
                sm.KeystoneAuthLayer(
                    target="ceilometer",
                    credentials_secret=ceilometer_compute_keystone_credentials,
                    endpoint_config=keystone_internal_api,
                    config_section="service_credentials",
                ),
                sm.AMQPTransportLayer(
                    target="ceilometer",
                    service=mq_service,
                    username=lambda ctx: f"ceilometer-compute-{ctx.instance}",
                    password_secret=mq_ceilometer_compute_user_passwords,
                ),
                sm.NodeLabelSelectedLayer(
                    target_map={
                        "ceilometerComputeConfig": "ceilometer",
                    },
                    accessor=(
                        lambda ctx: ctx.parent_spec["ceilometerCompute"]
                                                   ["configTemplates"]
                    ),
                )
            ],
        ),
    )
    compute_agent_cds = sm.TemplatedConfiguredDaemonSet(
        template="ceilometer-compute-cds-agent.yaml",
        scheduling_keys=COMPUTE_SCHEDULING_KEYS,
        add_dependencies=[compute_configs, ca_certs],
        versioned_dependencies=[ceilometer_docker_image],
    )
    compute_agent_pdb = sm.DisallowedPodDisruptionBudget(
         metadata=("ceilometer-compute-pdb-", True),
         replicated=compute_agent_cds,
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)


sm.register(Ceilometer)
