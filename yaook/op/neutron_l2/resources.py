#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import typing

import openstack

from yaook.statemachine import (
    context,
    customresource,
    watcher,
)
import yaook.statemachine.resources.openstack as resource
from yaook.statemachine.resources.base import DependencyMap


class L2StateResource(resource.L2ProvidingAgentStateResource):
    # Due to l2 eviction works in a different way, we need a own class here
    def __init__(
            self,
            *,
            scheduling_keys: typing.Collection[str],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self.neutronAgentWatch: watcher.ExternalWatcher[
                openstack.network.v2.agent.Agent] = resource.\
            NeutronAgentWatcher("NeutronL2Agent", "neutron-opensvswitch-agent")

    def get_listeners(self) -> typing.List[context.Listener]:
        return super().get_listeners() + [
            context.KubernetesListener[typing.Mapping](
                "", "v1", "nodes",
                self._handle_event,
                broadcast=True,
            ),
        ] + [
            context.ExternalListener(
                watcher=self.neutronAgentWatch,
                listener=self._handle_agent_event,
            ),
        ]

    def _get_agent(
            self,
            network_client: openstack.network.v2._proxy.Proxy,
            host: str,
            ) -> typing.Optional[openstack.network.v2.agent.Agent]:
        try:
            agent = resource.get_network_agent(network_client, host,
                                               "neutron-openvswitch-agent")
        except LookupError:
            return None
        return agent

    def _update_status(
            self,
            ctx: context.Context,
            connection_info: typing.Mapping[str, typing.Any],
            state_up: bool,
            ) -> typing.Optional[resource.ResourceStatus]:
        client = openstack.connect(**connection_info)
        # Call _check_agent() here to verify, last_heartbeat_at is after we
        # created the agent.
        agent = self._check_agent(ctx, client)
        if agent is None:
            return None
        is_state_up = agent.is_admin_state_up
        ctx.logger.debug("is_state_up = %r, want state_up = %r",
                         is_state_up, state_up)

        # As we did not raise by now the connection parameters seem to be valid
        self.neutronAgentWatch.connection_parameters = connection_info
        self.neutronAgentWatch.namespace = ctx.namespace

        if is_state_up != state_up:
            if state_up:
                ctx.logger.debug("enabling l2 agent %s %s",
                                 agent.binary, agent.host)
                client.network.update_agent(
                    agent=agent.id,
                    admin_state_up=True
                )

        agent = self._get_agent(client.network, ctx.parent_name)
        if agent is None:
            return None
        return resource.ResourceStatus(
            up=agent.is_alive,
            enabled=agent.is_admin_state_up,
            disable_reason=None,
        )

    async def update(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> None:
        spec_state = resource.ResourceSpecState(
            ctx.parent["spec"]["state"]
        )
        if spec_state != resource.ResourceSpecState.ENABLED:
            ctx.logger.warning(
                "Should set agent to state %s, but we only enable l2 agents! "
                "For disabling a l2 agend, delete it.",
                spec_state
                )
        conn_info = await self.get_openstack_connection_info(ctx)
        loop = asyncio.get_event_loop()

        status = await loop.run_in_executor(
            None,
            self._update_status,
            ctx,
            conn_info,
            True,
        )
        ctx.logger.debug("updated status in OpenStack: %s",
                         status)

        current_state = resource.ResourceStatusState(
            ctx.parent.get("status", {}).get("state", "Creating"),
        )
        next_state = resource.ResourceStatusState.ENABLED
        if current_state == resource.ResourceStatusState.CREATING and \
                (status is None or not status.up):
            next_state = resource.ResourceStatusState.CREATING
        else:
            await self._maintenance_required_label(
                ctx,
                "add",
                False,
                )
        additional_patch = {
            "state": next_state.value,
        }

        await customresource.update_status(
            ctx.parent_intf,
            ctx.namespace,
            ctx.parent_name,
            conditions=[
                customresource.ConditionUpdate(
                    type_=resource.ResourceCondition.ENABLED.value,
                    reason="Unknown",
                    message="",
                    status=str(
                        status.enabled
                        if status is not None
                        else "Unknown"
                    ),
                ),
            ],
            additional_patch=additional_patch,
        )
