##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "glance-api"
  labels:
    app: "glance-api"
spec:
  replicas:  {{ crd_spec.api.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
        config-timestamp: {{ dependencies['config'].last_update_timestamp() }}
    spec:
      automountServiceAccountToken: false
      shareProcessNamespace: true
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "glance-api"
          image:  {{ versioned_dependencies['glance_docker_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: glance-config-volumev2
              mountPath: /etc/glance
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
            {% if use_ceph %}
            - name: glance-ceph-conf-volume
              mountPath: /etc/ceph/ceph.conf
              subPath: ceph.conf
            - name: glance-ceph-keyfile-volume
              mountPath: /etc/ceph/keyfile
              subPath: keyfile
            {% endif %}
            {% if use_file %}
            - name: glance-image-volume
              mountPath: /var/lib/glance/images/
            {% endif %}
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
          livenessProbe:
            exec:
              command:
                - curl
                - localhost:8080
          readinessProbe:
            exec:
              command:
                - curl
                - localhost:8080
          resources: {{ crd_spec | resources('api.glance-api') }}
        - name: "ssl-terminator"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "9292"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9090"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
            - name: MAX_BODY_SIZE_MB
              value: "0"
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 9292
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 9292
              scheme: HTTPS
          resources: {{ crd_spec | resources('api.ssl-terminator') }}
        - name: "ssl-terminator-external"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "9293"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9091"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
            - name: MAX_BODY_SIZE_MB
              value: "0"
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 9293
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 9293
              scheme: HTTPS
          resources: {{ crd_spec | resources('api.ssl-terminator-external') }}
{% if crd_spec.api.internal | default(False) %}
        - name: "ssl-terminator-internal"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "9294"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9092"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
            - name: MAX_BODY_SIZE_MB
              value: "0"
          volumeMounts:
            - name: ssl-terminator-internal-config
              mountPath: /config
            - name: tls-secret-internal
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 9294
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 9294
              scheme: HTTPS
          resources: {{ crd_spec | resources('api.ssl-terminator-internal') }}
{% endif %}
        - name: "service-reload"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('api.service-reload') }}
        - name: "service-reload-external"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('api.service-reload-external') }}
{% if crd_spec.api.internal | default(False) %}
        - name: "service-reload-internal"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-internal-config
              mountPath: /config
            - name: tls-secret-internal
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('api.service-reload') }}
{% endif %}
      volumes:
        - name: glance-config-volumev2
          projected:
            sources:
            - secret:
                name: {{ dependencies['config'].resource_name() }}
                items:
                  - key: glance.conf
                    path: glance.conf
            - configMap:
                name: {{ dependencies['glance_policy'].resource_name() }}
                items:
                  - key: policy.json
                    path: policy.json
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_certificate_secret'].resource_name() }}
        - name: tls-secret-external
          secret:
            secretName: {{ dependencies['external_certificate_secret'].resource_name() }}
{% if crd_spec.api.internal | default(False) %}
        - name: tls-secret-internal
          secret:
            secretName: {{ dependencies['internal_certificate_secret'].resource_name() }}
        - name: ssl-terminator-internal-config
          emptyDir: {}
{% endif %}
        {% if use_ceph %}
        - name: glance-ceph-conf-volume
          secret:
            secretName: {{ dependencies['config'].resource_name() }}
            items:
              - key: ceph.conf
                path: ceph.conf
        - name: glance-ceph-keyfile-volume
          secret:
            secretName: {{ crd_spec.backends.ceph.keyringReference }}
            items:
              - key: {{ crd_spec.backends.ceph.keyringUsername }}
                path: keyfile
        {% endif %}
        {% if use_file %}
        - name: glance-image-volume
          persistentVolumeClaim:
            claimName: {{ dependencies['image_volume'].resource_name() }}
        {% endif %}
        - name: ssl-terminator-config
          emptyDir: {}
        - name: ssl-terminator-external-config
          emptyDir: {}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
