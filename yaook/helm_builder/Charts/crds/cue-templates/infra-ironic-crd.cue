// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#memcached
crd.#region
crd.#issuer
{
	#kind:     "InfrastructureIronicDeployment"
	#plural:   "infrastructureironicdeployments"
	#singular: "infrastructureironicdeployment"
	#shortnames: ["iironicd", "iironicds"]
	#releases: ["train", "ussuri", "victoria", "wallaby", "xena", "yoga", "zed", "2023.1"]
	#schema: properties: {
		spec: {
			required: [
				"api",
				"imageServer",
				"database",
				"memcached",
				"targetRelease",
				"dnsmasq",
				"issuerRef",
				"ingressAddress",
				"pxe",
				"ironicConfig",
				"inspectorConfig",
			]
			properties: {
				keystoneRef:    crd.#keystoneref
				api:            crd.apiendpoint
				serviceMonitor: crd.#servicemonitor
				inspectorApi:   crd.apiendpoint
				imageServer:    crd.storageconfig
				ingressAddress: {type: "string"}
				imageServer: {
					required: ["ingress"]
					properties: {
						ingress:       crd.#ingress
						pvcAccessMode: crd.#pvcaccessmode
					}
				}
				database: {
					type: "object"
					required: ["ironic", "inspector"]
					properties: {
						ironic:    crd.#databasesnip
						inspector: crd.#databasesnip
					}
				}
				pxe: {
					type: "object"
					required: ["listenNetwork", "dhcp"]
					properties: {
						listenNetwork: {
							type: "string"
						}
						dhcp: {
							type: "array"
							items: {
								type: "object"
								required: ["dhcpRange"]
								properties: {
									dhcpRange: type:      "string"
									defaultGateway: type: "string"
								}
							}
						}
					}
				}
				ipa: {
					type: "object"
					properties: {
						pxeAppendKernelParams: {
							type: "string"
						}
					}
				}

				dnsmasq: crd.storageconfig
				// waaaay more than enough for dnsmasq state
				dnsmasq: properties: storageSize: default: "100Mi"
				ironicConfig:    crd.#anyconfig
				inspectorConfig: crd.#anyconfig

				api: properties: resources: {
					type: "object"
					properties: {
						"ironic-api":              crd.#containerresources
						"ssl-terminator":          crd.#containerresources
						"ssl-terminator-external": crd.#containerresources
						"service-reload":          crd.#containerresources
						"service-reload-external": crd.#containerresources
					}
				}
				inspectorApi: properties: resources: {
					type: "object"
					properties: {
						"ironic-inspector":        crd.#containerresources
						"ssl-terminator":          crd.#containerresources
						"ssl-terminator-external": crd.#containerresources
						"service-reload":          crd.#containerresources
						"service-reload-external": crd.#containerresources
					}
				}
				imageServer: properties: resources: {
					type: "object"
					properties: {
						"bootstrap-cfg": crd.#containerresources
						"httpd":         crd.#containerresources
					}
				}
				dnsmasq: properties: resources: {
					type: "object"
					properties: "dnsmasq": crd.#containerresources
				}
				conductor: {
					type: "object"
					properties: {
						resources: {
							type: "object"
							properties: "conductor": crd.#containerresources
						}
					}
					crd.replicated
				}
				jobResources: {
					type: "object"
					properties: {
						"infra-ironic-db-sync-job":           crd.#containerresources
						"infra-ironic-inspector-db-sync-job": crd.#containerresources
					}
				}
			}
		}
	}
}
