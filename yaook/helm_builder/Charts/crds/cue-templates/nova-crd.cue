// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#memcached
crd.#region
crd.#issuer
{
	#kind:     "NovaDeployment"
	#plural:   "novadeployments"
	#singular: "novadeployment"
	#shortnames: ["novad", "novads"]
	#releases: ["queens", "train", "yoga"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"api",
			"conductor",
			"metadata",
			"scheduler",
			"vnc",
			"placement",
			"targetRelease",
			"novaConfig",
			"database",
			"messageQueue",
			"memcached",
			"region",
			"issuerRef",
			"databaseCleanup",
			"eviction",
		]
		properties: {
			keystoneRef:    crd.#keystoneref
			serviceMonitor: crd.#servicemonitor

			database: {
				type: "object"
				required: ["api", "cell0", "cell1"]
				properties: {
					api:       crd.#databasesnip
					placement: crd.#databasesnip
					cell0:     crd.#databasesnip
					cell1:     crd.#databasesnip
				}
			}

			databaseCleanup: {
				type: "object"
				required: ["schedule"]
				properties: {
					schedule: crd.#cronschedulesnip
					deletionTimeRange: {
						type:    "integer"
						default: 60
					}
				}
			}

			messageQueue: {
				type: "object"
				required: ["cell1"]
				properties: cell1: crd.#messagequeuesnip
			}

			api:         crd.apiendpoint
			conductor:   crd.replicated
			placement:   crd.apiendpoint
			scheduler:   crd.replicated
			consoleauth: crd.replicated
			vnc:         crd.apiendpoint
			metadata:    crd.replicated

			novaConfig:  crd.#anyconfig
			novaSecrets: crd.#configsecret
			policy: {
				type: "object"
				additionalProperties: type: "string"
			}

			placementConfig:  crd.#anyconfig
			placementSecrets: crd.#configsecret
			placementPolicy: {
				type: "object"
				additionalProperties: type: "string"
			}

			compute: {
				crd.pernodeconfig
				properties: configTemplates: items: properties: {
					novaComputeConfig: crd.#anyconfig
					hostAggregates: {
						type: "array"
						items: {
							type: "string"
						}
					}
					volumeBackends: {
						type: "object"
						properties: ceph: {
							type: "object"
							required: ["enabled", "keyringSecretName", "user", "uuid"]
							properties: {
								enabled: type:           "boolean"
								keyringSecretName: type: "string"
								user: type:              "string"
								uuid: {
									type:    "string"
									pattern: "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}"
								}
								cephConfig: crd.#anyconfig
							}
						}
					}
				}
			}

			eviction: crd.eviction

			ids: {
				type: "object"
				properties: {
					cinderGid: type: "integer"
				}
			}

			api: properties: resources: {
				type: "object"
				properties: {
					"nova-api":                crd.#containerresources
					"ssl-terminator":          crd.#containerresources
					"ssl-terminator-external": crd.#containerresources
					"ssl-terminator-internal": crd.#containerresources
					"service-reload":          crd.#containerresources
					"service-reload-external": crd.#containerresources
					"service-reload-internal": crd.#containerresources
				}
			}
			metadata: properties: resources: {
				type: "object"
				properties: {
					"nova-metadata":  crd.#containerresources
					"ssl-terminator": crd.#containerresources
					"service-reload": crd.#containerresources
				}
			}
			placement: properties: resources: {
				type: "object"
				properties: {
					"placement":               crd.#containerresources
					"ssl-terminator":          crd.#containerresources
					"ssl-terminator-external": crd.#containerresources
					"ssl-terminator-internal": crd.#containerresources
					"service-reload":          crd.#containerresources
					"service-reload-external": crd.#containerresources
					"service-reload-internal": crd.#containerresources
				}
			}
			vnc: properties: resources: {
				type: "object"
				properties: {
					"nova-novncproxy":         crd.#containerresources
					"ssl-terminator-external": crd.#containerresources
					"service-reload-external": crd.#containerresources
				}
			}
			conductor: properties: resources: {
				type: "object"
				properties: "nova-conductor": crd.#containerresources
			}
			consoleauth: properties: resources: {
				type: "object"
				properties: "nova-consoleauth": crd.#containerresources
			}
			scheduler: properties: resources: {
				type: "object"
				properties: "nova-scheduler": crd.#containerresources
			}
			compute: properties: resources: {
				type: "object"
				properties: {
					"keygen":            crd.#containerresources
					"chown-nova":        crd.#containerresources
					"nova-compute":      crd.#containerresources
					"nova-compute-ssh":  crd.#containerresources
					"libvirtd":          crd.#containerresources
					"compute-evict-job": crd.#containerresources
				}
			}
			evictPollMigrationSpeedLocalDisk: {
				type:        "integer"
				default:     30
				description: "The speed in MiB/s to what the migration of vms will be temporarily limited, when the root disk is local and using poll migration during the eviction process. Used 30 MiB/s as default, based on experience."
			}
			jobResources: {
				type: "object"
				properties: {
					"nova-db-cleanup-cronjob": crd.#containerresources
					"nova-api-db-sync-job":    crd.#containerresources
					"nova-create-cell1-job":   crd.#containerresources
					"nova-db-sync-job":        crd.#containerresources
					"nova-map-cell0-job":      crd.#containerresources
					"placement-db-sync-job":   crd.#containerresources
				}
			}
		}
	}
}
