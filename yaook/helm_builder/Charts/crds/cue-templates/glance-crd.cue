// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#database
crd.#memcached
crd.#policy
crd.#region
crd.#issuer
{
	#kind:     "GlanceDeployment"
	#plural:   "glancedeployments"
	#singular: "glancedeployment"
	#shortnames: ["glanced", "glanceds"]
	#releases: ["train", "ussuri", "victoria", "wallaby", "xena", "yoga"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"api",
			"database",
			"memcached",
			"targetRelease",
			"glanceConfig",
			"backends",
			"region",
			"issuerRef",
		]
		properties: {
			keystoneRef: crd.#keystoneref

			api:            crd.apiendpoint
			serviceMonitor: crd.#servicemonitor
			glanceConfig:   crd.#anyconfig
			glanceSecrets:  crd.#configsecret

			backends: {
				type: "object"
				oneOf: [
					{required: ["ceph"]},
					{required: ["file"]},
					{required: ["s3"]},
				]
				properties: {
					ceph: {
						type: "object"
						required: ["keyringReference", "keyringUsername", "keyringPoolname"]
						properties: {
							keyringReference: type: "string"
							keyringUsername: type:  "string"
							keyringPoolname: type:  "string"
							cephConfig: crd.#anyconfig
						}
					}
					file: {
						crd.storageconfig
						required: ["storageSize", "storageClassName"]
					}
					s3: {
						type: "object"
						required: [
							"endpoint",
							"bucket",
							"credentialRef",
						]
						properties: {
							endpoint: type: "string"
							bucket: type:   "string"
							credentialRef: crd.#ref
							addressingStyle: {
								type:    "string"
								default: "virtual"
								enum: [
									"virtual",
									"path",
									"auto",
								]
							}
						}
					}
				}
			}

			api: properties: resources: {
				type: "object"
				properties: {
					"glance-api":              crd.#containerresources
					"ssl-terminator":          crd.#containerresources
					"ssl-terminator-external": crd.#containerresources
					"ssl-terminator-internal": crd.#containerresources
					"service-reload":          crd.#containerresources
					"service-reload-external": crd.#containerresources
					"service-reload-internal": crd.#containerresources
				}
			}
			jobResources: {
				type: "object"
				properties: {
					"glance-db-load-metadefs-job": crd.#containerresources
					"glance-db-sync-job":          crd.#containerresources
					"glance-db-upgrade-pre-job":   crd.#containerresources
					"glance-db-upgrade-post-job":  crd.#containerresources
				}
			}
		}
	}
}
