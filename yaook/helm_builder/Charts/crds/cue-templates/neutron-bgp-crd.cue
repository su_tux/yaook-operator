// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#region

crd.#openstackcrd
{
	#group:    "network.yaook.cloud"
	#kind:     "NeutronBGPDRAgent"
	#plural:   "neutronbgpdragents"
	#singular: "neutronbgpdragent"
	#shortnames: ["bgpdragent", "bgpdragents"]
	#releases: ["queens", "train"]
	#schema: properties: spec: {
		required: [
			"bgpInterfaceMapping",
			"caConfigMapName",
			"configKey",
			"keystoneRef",
			"messageQueue",
			"neutronBGPDRAgentConfig",
			"novaRef",
			"targetRelease",
		]
		properties: {
			hostname: type: "string"
			lockName: type: "string"
			keystoneRef: crd.#keystoneref
			novaRef:     crd.#ref
			caConfigMapName: type: "string"
			configKey: type:       "string"
			neutronConfig: {
				type:  "array"
				items: crd.#anyconfig
			}
			neutronBGPDRAgentConfig: {
				type:  "array"
				items: crd.#anyconfig
			}
			bgpInterfaceMapping: {
				type: "object"
				required: ["bridgeName"]
				properties: {
					bridgeName: type: "string"
				}
			}
			messageQueue: {
				type: "object"
				required: ["amqpServerRef"]
				properties: {
					amqpServerRef: crd.#ref
				}
			}
			state: {
				type:    "string"
				default: "Enabled"
				enum: [
					"Enabled",
					"Disabled",
					"DisabledAndCleared",
				]
			}
			resources: {
				type: "object"
				properties: {
					"neutron-bgp-interface-setup": crd.#containerresources
					"neutron-bgp-dragent":         crd.#containerresources
					"bgp-evict-job":               crd.#containerresources
				}
			}
		}
	}
	#schema: properties: status: properties: {
		conditions: items: properties: type: enum: [
			"Converged",
			"GarbageCollected",
			"Evicted",
			"Enabled",
			"BoundToNode",
			"RequiresRecreation",
		]
		eviction: {
			type:     "object"
			nullable: true
			required: ["reason"]
			properties: reason: type: "string"
		}
		state: {
			type:    "string"
			default: "Creating"
			enum: [
				"Creating",
				"Enabled",
				"Disabled",
				"Evicting",
				"DisabledAndCleared",
			]
		}
	}
	#additionalprintercolumns: [
		{
			name:        "Phase"
			type:        "string"
			description: "Current status of the Resource"
			jsonPath:    ".status.phase"
		},
		{
			name:        "Reason"
			type:        "string"
			description: "The reason for the current status"
			jsonPath:    ".status.conditions[?(@.type==\"Converged\")].reason"
		},
		{
			name:        "State"
			type:        "string"
			description: "The state of the service"
			jsonPath:    ".status.state"
		},
		{
			name:        "Evicting"
			type:        "string"
			description: "Eviction status"
			jsonPath:    ".status.eviction.mode"
		},
		{
			name:        "Enabled"
			type:        "string"
			description: "Enabled status"
			jsonPath:    ".status.conditions[?(@.type==\"Enabled\")].status"
		},
		{
			name:        "Requires Recreation"
			type:        "string"
			description: "Requires Recreation status"
			jsonPath:    ".status.conditions[?(@.type==\"RequiresRecreation\")].status"
		},
	]
}
