// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#operatorcrd
crd.#imagepullsecretcrd
{
	#group:    "infra.yaook.cloud"
	#kind:     "AMQPServer"
	#plural:   "amqpservers"
	#singular: "amqpserver"
	#shortnames: ["amqps", "amqp"]
	#schema: properties: spec: {
		crd.replicated
		crd.storageconfig
		required: [
			"imageRef",
			"storageSize",
			"frontendIssuerRef",
			"backendCAIssuerRef",
		]
		properties: {
			imageRef: type: "string"
			serviceMonitor: crd.#servicemonitor
			implementation: {
				type:    "string"
				default: "RabbitMQ"
				enum: ["RabbitMQ"]
			}
			rabbitmqConfig: {
				type: "object"
				additionalProperties: "x-kubernetes-int-or-string": true
			}
			frontendIssuerRef:  crd.#ref
			backendCAIssuerRef: crd.#ref
			policies: {
				type: "object" // virtual host
				additionalProperties: {
					type: "object" // policy name
					additionalProperties: {
						type: "object"
						required: [
							"pattern",
							"definition",
							"priority",
							"applyto",
						]
						properties: {
							pattern: type: "string"
							definition: {
								type:                                   "object"
								"x-kubernetes-preserve-unknown-fields": true
							}
							priority: type: "integer"
							applyto: type:  "string"
						}
					}
				}
			}
			resources: {
				type: "object"
				properties: rabbitmq: crd.#containerresources
			}
		}
	}
	#schema: properties: status: properties: replicas: type: "integer"
}
