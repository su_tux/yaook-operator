// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package crd

#database: #crd
#database: {
	#schema: properties: spec: {
		properties: {
			database: #databasesnip
		}
	}
}

#memcached: #crd
#memcached: {
	#schema: properties: spec: {
		properties: {
			memcached: #memcachedsnip
		}
	}
}

#messagequeue: #crd
#messagequeue: {
	#schema: properties: spec: {
		properties: {
			messageQueue: #messagequeuesnip
		}
	}
}

#policy: #crd
#policy: {
	#schema: properties: spec: {
		properties: {
			policy: {
				type:                                   "object"
				"x-kubernetes-preserve-unknown-fields": true
			}
		}
	}
}

#region: #crd
#region: {
	#schema: properties: spec: properties: region: #regionspec
}

#issuer: #crd
#issuer: {
	#schema: properties: spec: properties: issuerRef: #issuerspec
}

#openstackcrd: {
	#releaseawarecrd
	#cacertificatecrd
	#imagepullsecretcrd
	#releases: *["queens"] | [...string]
}

#cacertificatecrd: {
	#operatorcrd
	#schema: properties: {
		spec: {
			properties: {
				caCertificates: {
					description:              "CA Certificates that should be added to all services can be placed here. CAs that issue certificates to the service are automatically added and do not need to be specified."
					type:                     "array"
					"x-kubernetes-list-type": "set"
					items: {
						type: "string"
					}
				}
			}
		}
	}
}

#releaseawarecrd: {
	#operatorcrd
	#releases: [...string]
	#schema: properties: {
		spec: {
			properties: {
				targetRelease: {
					type: "string"
					enum: #releases
				}
			}
		}
		status: properties: {
			installedRelease: {
				type: "string"
			}
			nextRelease: {
				type: "string"
			}
		}
	}
}

#tempestJob: {
	#operatorcrd
	#schema: properties: {
		spec: {
			required: [
				"keystoneRef",
				"target",
			]
			properties: {
				keystoneRef: #keystoneref
				target: {
					type: "object"
					oneOf: [
						{required: ["service"]},
						{required: ["regex"]},
					]
					properties: {
						service: {
							type: "string"
							enum: [
								"cinder",
								"glance",
								"keystone",
								"nova",
								"neutron",
								"all",
							]
						}
						regex: {
							type: "string"
						}
					}
				}
				pushgateway: {
					type: "string"
				}
				serial: {
					type:    "boolean"
					default: false
				}
				exclude: {
					type: "array"
					items: {
						type: "string"
					}
				}
				tempestConfig: #anyconfig
				account_cleanup: {
					type:    "boolean"
					default: false
				}
				preprovisioned_users: {
					type: "object"
					required: [
						"secret",
						"key",
					]
					properties: {
						secret: #ref
						key: type: "string"
					}
				}
				region: {
					type:    "string"
					default: "RegionOne"
				}
				resources: {
					type: "object"
					properties: {
						"tempest-job": #containerresources
					}
				}
			}
		}
	}
}

#imagepullsecretcrd: {
	#schema: properties: spec: properties: {
		imagePullSecrets: #imagepullsecretsnip
	}
}

#oneshotoperatorcrd: {
	#operatorcrd
	#schema: properties: status: properties: phase: enum: [
		"Created",
		"Updating",
		"WaitingForDependency",
		"BackingOff",
		"Completed",
		"Failed",
	]
}

#operatorcrd: {
	#crd
	#schema: {
		required: ["spec"]
		properties: spec: type: "object"
		properties: {
			status: {
				type: "object"
				required: ["conditions", "observedGeneration", "phase"]
				properties: {
					conditions: {
						type:                     "array"
						"x-kubernetes-list-type": "map"
						"x-kubernetes-list-map-keys": ["type"]
						items: {
							type: "object"
							required: [
								"lastTransitionTime",
								"lastUpdateTime",
								"message",
								"reason",
								"status",
								"type",
							]
							properties: {
								lastTransitionTime: {
									type:    "string"
									pattern: "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z"
								}
								lastUpdateTime: {
									type:    "string"
									pattern: "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z"
								}
								message: type: "string"
								reason: type:  "string"
								status: type:  "string"
								type: {
									type: "string"
									enum: ["Converged", "GarbageCollected", ...string]
								}
							}
						}
					}
					observedGeneration: type: "integer"
					updatedGeneration: type:  "integer"
					phase: {
						type: "string"
						enum: *[
							"Created",
							"Updating",
							"WaitingForDependency",
							"Updated",
							"BackingOff",
							"InvalidConfiguration",
						] | [...string]
					}
				}
			}
		}
	}
	#additionalprintercolumns: *[
					{
			name:        "Phase"
			type:        "string"
			description: "Current status of the Resource"
			jsonPath:    ".status.phase"
		},
		{
			name:        "Reason"
			type:        "string"
			description: "The reason for the current status"
			jsonPath:    ".status.conditions[?(@.type==\"Converged\")].reason"
		},
		{
			name:        "Message"
			type:        "string"
			description: "Informative messages"
			jsonPath:    ".status.conditions[?(@.type==\"Converged\")].message"
		},
	] | [...]
}

#crd: {
	#group:    *"yaook.cloud" | string
	#kind:     string
	#plural:   string
	#singular: string
	#shortnames: [...string]
	#schema: _
	#additionalprintercolumns: [...]

	apiVersion: "apiextensions.k8s.io/v1"
	kind:       "CustomResourceDefinition"
	metadata: {
		name: "\(#plural).\(#group)"
		annotations: {
			"helm.sh/resource-policy": "keep"
		}
	}
	spec: {
		scope: "Namespaced"
		group: #group
		names: {
			kind:       #kind
			plural:     #plural
			singular:   #singular
			shortNames: #shortnames
		}
		versions: [
			{
				name:    "v1"
				served:  true
				storage: true
				subresources: {
					status: {}
				}
				schema: {
					openAPIV3Schema: {
						type: "object"
						#schema
					}
				}
				additionalPrinterColumns: #additionalprintercolumns
			},
		]
	}
}
