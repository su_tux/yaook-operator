// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package crd

apiendpoint: {
	replicated
	publishEndpoint
	type: "object"
	required: ["ingress", ...]
	properties: {
		ingress: #ingress
		internal: {
			type: "object"
			required: ["ingress"]
			properties: {
				ingress: #ingress
			}
		}
	}
}

#ingress: {
	type: "object"
	required: ["fqdn", "port"]
	properties: {
		fqdn: type: "string"
		port: type: "integer"
		ingressClassName: {
			type:    "string"
			default: "nginx"
		}
		externalCertificateSecretRef: #ref
	}
}

replicated: {
	type: "object"
	properties: {
		replicas: {
			type:    "integer"
			default: *3 | int
		}
	}
}

publishEndpoint: {
	type: "object"
	properties: {
		publishEndpoint: {
			type:        "boolean"
			default:     *true | bool
			description: "Publish it to Keystone endpoints as well"
		}
	}
}

pernodeconfig: {
	type: "object"
	required: ["configTemplates"]
	properties: configTemplates: {
		type: "array"
		items: {
			type: "object"
			required: ["nodeSelectors"]
			properties: {
				nodeSelectors: {
					type: "array"
					items: {
						type: "object"
						required: ["matchLabels"]
						properties: matchLabels: {
							type: "object"
							additionalProperties: type: "string"
						}
					}
				}
			}
		}
	}
}

#anyconfig: {
	type: "object"
	additionalProperties: {
		type:                                   "object"
		"x-kubernetes-preserve-unknown-fields": true
	}
}

#anyobj: {
	type:                                   "object"
	"x-kubernetes-preserve-unknown-fields": true
}

#configsecret: {
	type: "array"
	items: {
		type: "object"
		required: ["secretName", "items"]
		properties: {
			secretName: type: "string"
			items: {
				type: "array"
				items: {
					type: "object"
					required: ["key", "path"]
					properties: {
						key: type: "string"
						path: {
							type:    "string"
							pattern: "/.*"
						}
					}
				}
			}
		}
	}
}

quantilepattern: "^([+-]?[0-9.]+)([eEinumkKMGTP]*[-+]?[0-9]*)$"
storageconfig: {
	type: "object"
	properties: {
		storageSize: {
			type:    "string"
			pattern: quantilepattern
			default: *"8Gi" | string
		}
		storageClassName: type: "string"
	}
}

#pvcaccessmode: {
	type:    "string"
	default: "ReadWriteOnce"
	enum: ["ReadWriteOnce", "ReadWriteMany"]
}

#mysqlconfig: {
	type: "object"
	properties: {
		mysqld: {
			type: "object"
			properties: {
				optimizer_switch: {
					type: "object"
					additionalProperties: {
						type: "boolean"
					}
				}
			}

			"x-kubernetes-preserve-unknown-fields": true
		}
		galera: {
			type:                                   "object"
			"x-kubernetes-preserve-unknown-fields": true
		}
		"client-server": {
			type:                                   "object"
			"x-kubernetes-preserve-unknown-fields": true
		}
		sst: {
			type:                                   "object"
			"x-kubernetes-preserve-unknown-fields": true
		}
	}
}

#servicemonitor: {
	type: "object"
	properties: {
		additionalLabels: {
			type: "object"
			additionalProperties: type: "string"
		}
	}
}

#databasesnip: {
	storageconfig
	replicated
	type: "object"
	required: [
		"proxy",
		"backup",
	]
	properties: {
		proxy: replicated
		proxy: properties: {
			replicas: default: 2
			resources: {
				type: "object"
				properties: {
					"create-ca-bundle": #containerresources
					"haproxy":          #containerresources
					"service-reload":   #containerresources
				}
			}
		}
		backup: backupspec
		backup: properties: {
			mysqldump: {
				type:    "boolean"
				default: false
			}
		}
		mysqlConfig: #mysqlconfig
		resources: {
			type: "object"
			properties: {
				"mariadb-galera":  #containerresources
				"backup-creator":  #containerresources
				"backup-shifter":  #containerresources
				"mysqld-exporter": #containerresources
			}
		}
	}
}

#memcachedsnip: {
	replicated
	type: "object"
	properties: {
		memory: {
			type:    "integer"
			default: 512
		}
		connections: {
			type:    "integer"
			default: 1024
		}
		resources: {
			type: "object"
			properties: memcached: #containerresources
		}
	}
}

#messagequeuesnip: {
	storageconfig
	replicated
	type: "object"
	properties: {
		resources: {
			type: "object"
			properties: rabbitmq: #containerresources
		}
	}
}

#ref: {
	type: "object"
	required: ["name"]
	properties: {
		name: {
			type: "string"
		}
	}
}

#keystoneref: {
	#ref
	properties: kind: {
		type: "string"
		enum: ["KeystoneDeployment", "ExternalKeystoneDeployment"]
		default: "KeystoneDeployment"
	}
}

#secretkeyref: {
	#ref
	properties: key: {
		type:    "string"
		default: "password"
	}
}

#regionspec: {
	type: "object"
	properties: {
		name: {
			type:    "string"
			default: "RegionOne"
		}
		parent: {
			type: "string"
		}
	}
}

#issuerspec: {
	type: "object"
	properties: {
		name: {
			type:    "string"
			default: "ca-issuer"
		}
	}
}

eviction: {
	type: "object"
	properties: {
		ironicNodeShutdown: {
			description: "Secret reference to Credentials for Ironic, containing: OS_AUTH_URL, OS_USERNAME, OS_PASSWORD, OS_PROJECT_NAME, OS_REGION_NAME, OS_INTERFACE"
			type:        "object"
			required: ["credentialsSecretRef"]
			properties: {
				credentialsSecretRef: #ref
			}
		}
		volumeLockDurationSeconds: {
			description: "wait for releasing the volume lock during the eviction"
			type:        "integer"
			default:     0
		}
	}
}

#cronschedulesnip: {
	type:    "string"
	pattern: "((\\d+|([\\d*]+(\\/|-)\\d+)|\\*) ?){5}"
	default: "0 0 * * *"
}

backupspec: {
	type: "object"
	required: [
		"schedule",
	]
	properties: {
		schedule: #cronschedulesnip
		targets: {
			type: "object"
			anyOf: [
				{required: ["s3"]},
			]
			properties: {
				s3: {
					type: "object"
					required: [
						"endpoint",
						"bucket",
						"credentialRef",
					]
					properties: {
						endpoint: type:   "string"
						bucket: type:     "string"
						filePrefix: type: "string"
						credentialRef: #ref
						addressingStyle: {
							type:    "string"
							default: "virtual"
						}
					}
				}
			}
		}
	}
}

ovnBgpConfig: {
	type: "object"
	properties: {
		addressScopes: {
			type: "array"
			items: {
				type: "string"
			}
			description: "List of address scope IDs for the subnet, in case you are using `ovn_stretched_l2_bgp_driver`"
		}
		bridgeName: {
			type:        "string"
			description: "Name of the provider bridge to which the BGP interface should be added."
		}
		driver: {
			type: "string"
			enum: [
				"ovn_bgp_driver",
				"ovn_stretched_l2_bgp_driver",
				"ovn_evpn_driver",
			]
			default:     "ovn_stretched_l2_bgp_driver"
			description: "Name of the `ovn-bgp-agent` driver that can be used. Please check `ovn-bgp-agent` docs for further details."
		}
		localAS: {
			type:        "integer"
			minimum:     1
			maximum:     4294967295
			description: "The AS number to be used on the BGP agent side."
		}
		peers: {
			type: "object"
			additionalProperties: {
				type: "object"
				required: ["AS", "IP"]
				properties: {
					AS: {
						type:        "integer"
						minimum:     1
						maximum:     4294967295
						description: "The AS number to be peered with."
					}
					IP: {
						type:        "string"
						regex:       "^((^\\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\\s*$)|(^\\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:)))(%.+)?\\s*$))$" // Shamelessly stolen from https://www.regextester.com/104038
						description: "List of peer IPs that have the same AS number and can be added to the same peer group."
					}
				}
			}
		}
		resources: {
			type: "object"
			properties: {
				"ovn-bgp-agent": #containerresources
				"frr-bgpd":      #containerresources
				"frr-zebra":     #containerresources
			}
		}
		syncInterval: {
			type:        "integer"
			default:     *120 | int
			description: "The interval time(seconds) when it should resync with southbound database."
		}

	}
}

#imagepullsecretsnip: {
	type:  "array"
	items: #ref
}

// To make normalization easier, only integer values are allowed.
#containerreqlimits: {
	type: "object"
	properties: {
		cpu: {
			type:    "string"
			pattern: "^[1-9][0-9]*m?$"
		}
		memory: {
			type:    "string"
			pattern: "^[1-9][0-9]*(E|P|T|G|M|k|Ei|Pi|Ti|Gi|Mi|Ki)?$"
		}
	}
}

#containerresources: {
	type: "object"
	properties: {
		limits:   #containerreqlimits
		requests: #containerreqlimits
	}
}
