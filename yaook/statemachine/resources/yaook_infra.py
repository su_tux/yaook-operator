#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`~yaook.statemachine.infra` – Objects representing infra.yaook.cloud resources
###################################################################################

.. autosummary::

    MySQLService
    MySQLUser
    AMQPServer
    AMQPUser
    OVSDBService
    TemplatedMySQLService
    TemplatedMySQLUser
    TemplatedAMQPServer
    TemplatedAMQPUser
    TemplatedOVSDBService

.. autoclass:: MySQLService

.. autoclass:: MySQLUser

.. autoclass:: AMQPServer

.. autoclass:: AMQPUser

.. autoclass:: OVSDBService

.. autoclass:: MemcachedService

.. autoclass:: TemplatedMySQLService

.. autoclass:: TemplatedMySQLUser

.. autoclass:: TemplatedAMQPServer

.. autoclass:: TemplatedAMQPUser

.. autoclass:: TemplatedOVSDBService

.. autoclass:: TemplatedMemcachedService

"""  # noqa
import copy
import typing

import kubernetes_asyncio.client as kclient

from .. import api_utils, context, exceptions, interfaces
from .base import (
    DependencyMap,
    MetadataProvider,
    ResourceBody,
    evaluate_metadata,
)
from .k8s import BodyTemplateMixin, KubernetesReference
from .yaook import YaookReadyResource


class MySQLService(YaookReadyResource):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[typing.Mapping]:
        return interfaces.mysqlservice_interface(api_client)

    def _needs_update(self,
                      current: typing.Mapping,
                      new: typing.Mapping) -> bool:
        current_metadata = copy.deepcopy(current["metadata"])
        new_metadata = copy.deepcopy(new["metadata"])
        current_metadata.setdefault("annotations", {}).pop(
            context.ANNOTATION_LAST_UPDATE, None,
        )
        new_metadata.setdefault("annotations", {}).pop(
            context.ANNOTATION_LAST_UPDATE, None,
        )

        return (
            api_utils.deep_has_changes(current_metadata, new_metadata) or
            (current["spec"].get("targetRelease", {}) !=
             new["spec"].get("targetRelease", {})) or
            current["spec"]["proxy"] != new["spec"]["proxy"] or
            current["spec"]["replicas"] != new["spec"]["replicas"] or
            (current["spec"].get("backup", {}) !=
             new["spec"].get("backup", {})) or
            (set(current["spec"].get("caCertificates", [])) !=
             set(new["spec"].get("caCertificates", []))) or
            (current["spec"].get("mysqlConfig", {}) !=
             new["spec"].get("mysqlConfig", {})) or
            current["spec"]["storageSize"] != new["spec"]["storageSize"] or
            (current["spec"].get("storageClassName") !=
             new["spec"].get("storageClassName")) or
            (current["spec"].get("serviceMonitor") !=
             new["spec"].get("serviceMonitor"))
        )


class MySQLUser(YaookReadyResource):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[typing.Mapping]:
        return interfaces.mysqluser_interface(api_client)

    def _needs_update(self,
                      current: typing.Mapping,
                      new: typing.Mapping) -> bool:
        current_metadata = copy.deepcopy(current["metadata"])
        new_metadata = copy.deepcopy(new["metadata"])
        current_metadata.setdefault("annotations", {}).pop(
            context.ANNOTATION_LAST_UPDATE, None,
        )
        new_metadata.setdefault("annotations", {}).pop(
            context.ANNOTATION_LAST_UPDATE, None,
        )
        current_spec = copy.deepcopy(current["spec"])
        new_spec = copy.deepcopy(new["spec"])
        current_spec.pop("user", None)
        new_spec.pop("user", None)
        return (
            api_utils.deep_has_changes(current_metadata, new_metadata) or
            api_utils.deep_has_changes(current_spec, new_spec)
        )

    async def get_used_resources(
            self,
            ctx: context.Context,
            ) -> typing.Collection[api_utils.ResourceReference]:
        try:
            current = await self._get_current(ctx, True)
        except exceptions.ResourceNotPresent:
            return []

        return [
            api_utils.ResourceReference(
                api_version="v1",
                plural="secrets",
                namespace=ctx.namespace,
                name=current["spec"]["passwordSecretKeyRef"]["name"],
            )
        ]


class AMQPServer(YaookReadyResource):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[typing.Mapping]:
        return interfaces.amqpserver_interface(api_client)

    def _needs_update(self,
                      current: typing.Mapping,
                      new: typing.Mapping) -> bool:
        current_metadata = copy.deepcopy(current["metadata"])
        new_metadata = copy.deepcopy(new["metadata"])
        current_metadata.setdefault("annotations", {}).pop(
            context.ANNOTATION_LAST_UPDATE, None,
        )
        new_metadata.setdefault("annotations", {}).pop(
            context.ANNOTATION_LAST_UPDATE, None,
        )

        return (
            api_utils.deep_has_changes(current_metadata, new_metadata) or
            current["spec"]["replicas"] != new["spec"]["replicas"] or
            current["spec"]["imageRef"] != new["spec"]["imageRef"] or
            (current["spec"].get("storageClassName") !=
             new["spec"].get("storageClassName")) or
            (current["spec"].get("serviceMonitor") !=
             new["spec"].get("serviceMonitor"))
        )


class AMQPUser(YaookReadyResource):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterface[typing.Mapping]:
        return interfaces.amqpuser_interface(api_client)

    def _needs_update(self,
                      current: typing.Mapping,
                      new: typing.Mapping) -> bool:
        current_metadata = copy.deepcopy(current["metadata"])
        new_metadata = copy.deepcopy(new["metadata"])
        current_metadata.setdefault("annotations", {}).pop(
            context.ANNOTATION_LAST_UPDATE, None,
        )
        new_metadata.setdefault("annotations", {}).pop(
            context.ANNOTATION_LAST_UPDATE, None,
        )
        return (
            api_utils.deep_has_changes(current_metadata, new_metadata) or
            (current["spec"]["passwordSecretKeyRef"] !=
             new["spec"]["passwordSecretKeyRef"])
        )

    async def get_used_resources(
            self,
            ctx: context.Context,
            ) -> typing.Collection[api_utils.ResourceReference]:
        try:
            current = await self._get_current(ctx, True)
        except exceptions.ResourceNotPresent:
            return []
        return [
            api_utils.ResourceReference(
                api_version="v1",
                plural="secrets",
                namespace=ctx.namespace,
                name=current["spec"]["passwordSecretKeyRef"]["name"],
            ),
        ]


class MemcachedService(YaookReadyResource):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[typing.Mapping]:
        return interfaces.memcachedservice_interface(api_client)

    def _needs_update(self,
                      current: typing.Mapping,
                      new: typing.Mapping) -> bool:
        current_metadata = copy.deepcopy(current["metadata"])
        new_metadata = copy.deepcopy(new["metadata"])
        current_metadata.setdefault("annotations", {}).pop(
            context.ANNOTATION_LAST_UPDATE, None,
        )
        new_metadata.setdefault("annotations", {}).pop(
            context.ANNOTATION_LAST_UPDATE, None,
        )

        return (
            api_utils.deep_has_changes(current_metadata, new_metadata) or
            current["spec"]["imageRef"] != new["spec"]["imageRef"] or
            current["spec"]["memory"] != new["spec"]["memory"] or
            current["spec"]["connections"] != new["spec"]["connections"] or
            current["spec"]["replicas"] != new["spec"]["replicas"]
        )


class OVSDBService(YaookReadyResource):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[typing.Mapping]:
        return interfaces.ovsdbservice_interface(api_client)

    def _needs_update(self,
                      current: typing.Mapping,
                      new: typing.Mapping) -> bool:
        current_metadata = copy.deepcopy(current["metadata"])
        new_metadata = copy.deepcopy(new["metadata"])
        current_metadata.setdefault("annotations", {}).pop(
            context.ANNOTATION_LAST_UPDATE, None,
        )
        new_metadata.setdefault("annotations", {}).pop(
            context.ANNOTATION_LAST_UPDATE, None,
        )

        return (
            api_utils.deep_has_changes(current_metadata, new_metadata) or
            current["spec"]["backup"] != new["spec"]["backup"] or
            current["spec"]["imageRef"] != new["spec"]["imageRef"] or
            current["spec"].get("inactivityProbeMs") !=
            new["spec"].get("inactivityProbeMs") or
            current["spec"]["issuerRef"] != new["spec"]["issuerRef"] or
            current["spec"].get("ovnRelay") != new["spec"].get("ovnRelay") or
            current["spec"]["replicas"] != new["spec"]["replicas"] or
            current["spec"].get("resources") != new["spec"].get("resources") or
            current["spec"].get("serviceMonitor") !=
            new["spec"].get("serviceMonitor") or
            current["spec"].get("storageClassName") !=
            new["spec"].get("storageClassName") or
            current["spec"]["storageSize"] !=
            new["spec"]["storageSize"]
        )


class TemplatedMySQLService(BodyTemplateMixin,
                            MySQLService):
    """
    Manage a jinja2-templated MySQLService.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.

        :class:`~.MySQLServiceState`:
            for arguments specific to MySQLService.
    """


class TemplatedMySQLUser(BodyTemplateMixin,
                         MySQLUser):
    """
    Manage a jinja2-templated MySQLUser.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.

        :class:`~.MySQLUserState`:
            for arguments specific to MySQLUser.
    """


class SimpleMySQLUser(MySQLUser):
    def __init__(
            self,
            *,
            metadata: MetadataProvider,
            database: KubernetesReference[typing.Mapping],
            username: str,
            password_secret: KubernetesReference[kclient.V1Secret],
            password_key: str = "password",
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self._metadata = metadata
        self._database = database
        self._password_secret = password_secret
        self._password_key = password_key
        self._username = username
        self._declare_dependencies(database, password_secret)

    async def _make_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> ResourceBody:
        metadata = evaluate_metadata(ctx, self._metadata)
        database_ref = await self._database.get(ctx)
        secret_ref = await self._password_secret.get(ctx)
        return {
            "apiVersion": "infra.yaook.cloud/v1",
            "kind": "MySQLUser",
            "metadata": metadata,
            "spec": {
                "user": self._username.format(instance=ctx.instance),
                "passwordSecretKeyRef": {
                    "name": secret_ref.name,
                    "key": self._password_key,
                },
                "serviceRef": {
                    "name": database_ref.name,
                },
            },
        }


class TemplatedAMQPServer(BodyTemplateMixin,
                          AMQPServer):
    """
    Manage a jinja2-templated AMQPServer.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.

        :class:`~.AMQPServerState`:
            for arguments specific to AMQPServers.
    """


class TemplatedAMQPUser(BodyTemplateMixin,
                        AMQPUser):
    """
    Manage a jinja2-templated AMQPUser.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.

        :class:`~.AMQPUser`:
            for arguments specific to AMQPUsers.
    """


class SimpleAMQPUser(AMQPUser):
    def __init__(
            self,
            *,
            metadata: MetadataProvider,
            server: KubernetesReference[typing.Mapping],
            username_format: str,
            password_secret: KubernetesReference[kclient.V1Secret],
            password_key: str = "password",
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata = metadata
        self._server = server
        self._username_format = username_format
        self._password_secret = password_secret
        self._password_key = password_key
        self._declare_dependencies(server, password_secret)

    async def _make_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> ResourceBody:
        metadata = evaluate_metadata(ctx, self._metadata)
        server_ref = await self._server.get(ctx)
        secret_ref = await self._password_secret.get(ctx)
        return {
            "apiVersion": "infra.yaook.cloud/v1",
            "kind": "AMQPUser",
            "metadata": metadata,
            "spec": {
                "user": self._username_format.format(
                    instance=ctx.instance, name=ctx.parent_name),
                "passwordSecretKeyRef": {
                    "name": secret_ref.name,
                    "key": self._password_key,
                },
                "serverRef": {
                    "name": server_ref.name,
                },
            },
        }


class TemplatedMemcachedService(BodyTemplateMixin,
                                MemcachedService):
    """
    Manage a jinja2-templated MemcachedService.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.

        :class:`~.MemcachedServiceState`:
            for arguments specific to MemcachedService.
    """


class TemplatedOVSDBService(BodyTemplateMixin,
                            OVSDBService):
    """
    Manage a jinja2-templated MySQLService.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.

        :class:`~.MySQLServiceState`:
            for arguments specific to MySQLService.
    """
