import copy
import typing

import kubernetes_asyncio.client as kclient

from .. import api_utils, context, exceptions, interfaces, watcher
from .k8s import KubernetesReference, SingleObject

T = typing.TypeVar("T")

# This is a fun hack in order to allow type checking of methods such as
# `_set_owner`, which reference both methods from the fictitious base class
# as well as from the mixin.
if typing.TYPE_CHECKING:
    _K8SReferenceBase = KubernetesReference[T]
else:
    _K8SReferenceBase = object


class YaookResourceReadinessMixin(typing.Generic[T], _K8SReferenceBase):
    """
    Mixin to handle the readyness of yaook resources.
    Will only return `True` for `is_ready` if the resource is
    fully reconciled.

    Fully Reconciled means that the respective operator has reconciled
    the latest spec of the resource at least once and has finished the
    reconciliation or is currently doing a reconcile process.
    """
    def _is_updated(self, resource: typing.Mapping) -> bool:
        generation = resource.get("metadata", {}).get("generation")
        status = resource.get("status", {})
        updated_generation = status.get("updatedGeneration")
        phase = status.get("phase")
        return (
                    (phase == context.Phase.UPDATED.value or
                     phase == context.Phase.UPDATING.value) and
                    generation == updated_generation
               )

    def get_listeners(self) -> typing.List[context.Listener]:
        # the api client isn’t needed until you actually do something
        # with the interface. we need to convert the factory into
        # a class to access the resource name and group.
        # See also
        # yaook.statemachine.dependencies.\
        # ForeignResourceDependency.get_listeners
        iface = self._create_resource_interface(typing.cast(
            kclient.ApiClient,
            None,
        ))
        return super().get_listeners() + [
            context.KubernetesListener[T](
                iface.group, iface.version, iface.plural,
                self._handle_dependency_event,
            ),
        ]

    def _handle_dependency_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[T],
            ) -> bool:
        if event.type_ == watcher.EventType.MODIFIED and \
                self.get_resource_interface(ctx).group.endswith("yaook.cloud"):
            # We can add a few conditions if we are handling our own
            # custom resources. These can prevent spuirous reconciles.
            old_raw_object = typing.cast(typing.Dict[str, typing.Any],
                                         event.old_raw_object)
            raw_object = typing.cast(typing.Dict[str, typing.Any],
                                     event.raw_object)
            old = copy.deepcopy(old_raw_object)
            new = copy.deepcopy(raw_object)
            # Condition 1: Our resources always have a generation, we do not
            # need to rely on the resourceVersion to find changes
            old.setdefault("metadata", {}).pop("resourceVersion", None)
            new.setdefault("metadata", {}).pop("resourceVersion", None)
            # Condition 2: Our resources always have a phase, we do not need
            # to care about conditions here
            old.setdefault("status", {}).pop("conditions", None)
            new.setdefault("status", {}).pop("conditions", None)
            # Condition 3: If we set a last update time, we do not need it
            old.setdefault("metadata", {}).setdefault("annotations", {}).pop(
                context.ANNOTATION_LAST_UPDATE, None)
            new.setdefault("metadata", {}).setdefault("annotations", {}).pop(
                context.ANNOTATION_LAST_UPDATE, None)
            # Condition 4: Generally managed fields are uninteresting
            old.setdefault("metadata", {}).pop("managedFields", None)
            new.setdefault("metadata", {}).pop("managedFields", None)

            if not api_utils.deep_has_changes(old, new):
                return False

        return self._is_updated(event.raw_object)

    async def is_ready(self, ctx: context.Context) -> bool:
        try:
            reference = await self.get(ctx)
            interface_ = self.get_resource_interface(ctx)
            interface = typing.cast(interfaces.ResourceInterfaceWithStatus[T],
                                    interface_)
            resource_ = await interface.read_status(
                reference.namespace, reference.name)
            resource = typing.cast(typing.Mapping, resource_)
            return self._is_updated(resource)
        except (exceptions.AmbiguousRequest, exceptions.ResourceNotPresent):
            return False
        except kclient.ApiException as exc:
            if exc.status == 404:
                return False
            raise


class YaookReadyResource(YaookResourceReadinessMixin,
                         SingleObject[typing.Mapping]):
    """
    A base class for resources handled by yaook operators where we should
    wait for their readyness
    """
