#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
Kubernetes Authentication resources
###################################

.. currentmodule:: yaook.statemachine.resources

.. autoclass:: ServiceAccount

.. autoclass:: TemplatedServiceAccount

"""
import typing

import kubernetes_asyncio.client as kclient

from .. import interfaces
from .k8s import BodyTemplateMixin, SingleObject


class ServiceAccount(SingleObject[kclient.V1ServiceAccount]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[
                kclient.V1ServiceAccount]:
        return interfaces.service_account_interface(api_client)

    def _needs_update(self,
                      current: kclient.V1ServiceAccount,
                      new: typing.Mapping) -> bool:
        return (super()._needs_update(current, new) or
                current.metadata.name != new["metadata"]["name"])


class TemplatedServiceAccount(BodyTemplateMixin, ServiceAccount):
    """
    Manage a jinja2-templated ServiceAccount.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.
    """
