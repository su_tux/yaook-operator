#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
Prometheus/Monitoring resources
###############################

.. currentmodule:: yaook.statemachine.resources

.. autoclass:: ServiceMonitor

.. autoclass:: GeneratedServiceMonitor

.. autoclass:: GeneratedStatefulsetServiceMonitor
"""
import copy
import typing

import kubernetes_asyncio.client as kclient

from .. import context, interfaces
from .base import (
    DependencyMap,
    MetadataProvider,
    ResourceBody,
    evaluate_metadata,
)
from .k8s import KubernetesReference, SingleObject
from .orchestration import (
    OptionalKubernetesReference,
)


class ServiceMonitor(SingleObject[typing.Mapping]):
    """
    Manage a ServiceMonitor resource.
    """

    def _needs_update(self,
                      current: typing.Mapping,
                      new: typing.Mapping) -> bool:
        return (current["spec"] != new["spec"] or
                super()._needs_update(current, new))


class GeneratedServiceMonitor(ServiceMonitor):
    """
    Generate a ServiceMonitor from an existing Service Resource.
    The ServiceMonitor will check all pods behind specified service
    using a specified named port.
    The Certificate which is used by the monitored service needs to be
    passed as its CA is used to validate the TLS connection.
    """

    def __init__(
            self,
            metadata: MetadataProvider,
            service: typing.Union[
                SingleObject[kclient.V1Service],
                OptionalKubernetesReference[kclient.V1Service],
                ],
            certificate: KubernetesReference[kclient.V1Secret],
            endpoints: typing.List[str],
            server_name_provider: typing.Optional[
                typing.Callable[[context.Context], str]
                ] = None,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata = metadata
        self.service = service
        self.certificate = certificate
        self.endpoints = endpoints
        self.server_name_provider = server_name_provider
        self._declare_dependencies(self.service)
        self._declare_dependencies(self.certificate)

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterface[typing.Mapping]:
        return interfaces.servicemonitor_interface(api_client)

    async def _make_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> ResourceBody:
        if self.server_name_provider:
            service_name = self.server_name_provider(ctx)
        else:
            service_name = (await self.service.get(ctx)).name
        ca_name = (await self.certificate.get(ctx)).name

        # We need to copy here as we otherwise potentially pass a reference to
        # a dict in `ctx.parent_spec`. This would cause `adopt_object` to later
        # update this dict and thereby modify `ctx.parent_spec`
        metadata_labels = copy.copy(
            ctx.parent_spec.get('serviceMonitor', {})
            .get('additionalLabels', {}))

        config = {
            "apiVersion": "monitoring.coreos.com/v1",
            "kind": "ServiceMonitor",
            "metadata": {
                **evaluate_metadata(ctx, self._metadata),
                "labels": metadata_labels,
            },
            "spec": {
                "selector": {
                    "matchLabels": self.service.labels(ctx)
                },
                "endpoints": [
                    {
                        "port": e,
                        "scheme": "https",
                        "tlsConfig": {
                            "ca": {
                                "secret": {
                                    "name": ca_name,
                                    "key": "tls.crt",
                                },
                            },
                            "serverName": service_name,
                        },
                    } for e in self.endpoints
                ],
            },
        }
        return config


class GeneratedStatefulsetServiceMonitor(GeneratedServiceMonitor):
    """
    Subclass of GeneratedServiceMonitor that ensure to relabel "instance" label
    (build from pod-ip:scrape-port) for the scrape target to the more useful
    pair of values (pod-name:scrape-port) for statefulset in servicemonitor
    """
    def _add_relabel_rule(
            self,
            monitorspec: ResourceBody,
            relabelings: typing.Dict) -> None:
        """
        Add relabel rule defined in `relabelings` into relabelings
        list in `monitorspec`.


        :param monitorspec: ServiceMonitor onto which the relabel rule is added
        :param relabelings: relabel rule merged into monitorspec
        :return: None
        """
        for i in range(len(self.endpoints)):
            if "relabelings" in monitorspec["spec"]["endpoints"][i]:
                for j in relabelings["relabelings"]:
                    monitorspec["spec"]["endpoints"][i]
                    ["relabelings"].append(j)
            else:
                monitorspec["spec"]["endpoints"][i].update(relabelings)

    async def _make_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> ResourceBody:

        config = {
            "relabelings": [
                {
                    "action": "replace",
                    "separator": ':',
                    "sourceLabels": [
                        "__meta_kubernetes_pod_name",
                        "__meta_kubernetes_pod_container"
                        "_port_number",
                    ],
                    "targetLabel": "instance",
                },
            ],
        }
        origin_config = await super()._make_body(
            ctx=ctx,
            dependencies=dependencies
            )

        self._add_relabel_rule(origin_config, config)

        return origin_config
