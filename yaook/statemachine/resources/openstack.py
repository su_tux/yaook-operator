#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limit
from abc import ABCMeta, abstractmethod
import asyncio
import enum
import json
import jsonpatch
import openstack
import requests
import typing
import dataclasses

from contextvars import ContextVar
from datetime import datetime

from yaook.statemachine.resources import dependencies

from .. import (
    api_utils,
    context,
    customresource,
    exceptions,
    interfaces,
    watcher,
    resources,
)
from . import base, instancing, k8s
from .base import (
    DependencyMap,
    ResourceBody,
    TemplateMixin,
)
from .external import ExternalResource
from .k8s import KubernetesReference
from .k8s_workload import Job, TemplatedJob, TemplatedStatefulSet

import kubernetes_asyncio.client as kclient

LOCK_NAME_FORMAT = context.ANNOTATION_L2_MIGRATION_LOCK + "{kind}"


def raise_response_error_if_any(
        resp: requests.models.Response
        ) -> None:
    if resp.status_code < 400:
        return

    try:
        json_data = resp.json()
    except Exception:
        msg = None
    else:
        if isinstance(json_data, dict):
            msg = json_data.get("message")
            if msg is None:
                if len(json_data) == 1:
                    nested = json_data[list(json_data.keys())[0]]
                    if isinstance(nested, dict):
                        msg = nested.get("message")
        else:
            msg = None

    raise openstack.exceptions.HttpException(
        msg or "Error",
        response=resp,
    )


def get_network_agent(
        network_client: openstack.network.v2._proxy.Proxy,
        host: str,
        binary: str,
        ) -> openstack.network.v2.agent.Agent:
    agents = list(network_client.agents(
        binary=binary,
        host=host)
    )
    if not agents:
        raise LookupError(f"{binary} on {host} not found")
    return agents[0]


def disable_network_agent(
        network_client: openstack.network.v2._proxy.Proxy,
        agent: openstack.network.v2.agent.Agent,) -> None:
    raise_response_error_if_any(
        network_client.put(
            f"/agents/{agent.id}",
            json={
                "agent": {
                    "admin_state_up": False,
                },
            },
        ),
    )


class ResourceCondition(enum.Enum):
    EVICTED = "Evicted"
    ENABLED = "Enabled"
    BOUND_TO_NODE = "BoundToNode"
    REQUIRES_RECREATION = "RequiresRecreation"


class ResourceEvictingStatus(enum.Enum):
    NOT_EVICTING = "NotEvicting"
    EVICTING = "Evicting"
    FORCE_EVICTING = "ForceEvicting"


class ResourceEvictingReason(enum.Enum):
    SUCCESS = "Success"
    DELETING = "Deleting"
    DESIRED_STATE = "DesiredState"
    NODE_DOWN = "NodeDown"
    UNKNOWN = "Unknown"


class ResourceSpecState(enum.Enum):
    ENABLED = "Enabled"
    DISABLED = "Disabled"
    DISABLED_AND_CLEARED = "DisabledAndCleared"

    def to_status_state(self):
        return ResourceStatusState(self.value)


class ResourceStatusState(enum.Enum):
    CREATING = "Creating"
    ENABLED = "Enabled"
    DISABLED = "Disabled"
    EVICTING = "Evicting"
    DISABLED_AND_CLEARED = "DisabledAndCleared"


class EvictionIntent(typing.NamedTuple):
    reason: ResourceEvictingReason


if typing.TYPE_CHECKING:
    _MixinBase = base.Resource
else:
    _MixinBase = object


class OpenStackMixin(_MixinBase):
    auth_url: str

    def __init__(
            self,
            *,
            endpoint_config: KubernetesReference[kclient.V1ConfigMap],
            credentials_secret: KubernetesReference[kclient.V1Secret],
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self._declare_dependencies(endpoint_config, credentials_secret)
        self._endpoint_config = endpoint_config
        self._credentials_secret = credentials_secret

    async def get_openstack_connection_info(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[str, typing.Any]:
        secrets = interfaces.secret_interface(ctx.api_client)
        configmaps = interfaces.config_map_interface(ctx.api_client)
        secret_ref = await self._credentials_secret.get(ctx)
        config_ref = await self._endpoint_config.get(ctx)
        credentials = api_utils.decode_secret_data(
            (await secrets.read(secret_ref.namespace, secret_ref.name)).data
        )
        endpoint_config = (await configmaps.read(
            config_ref.namespace, config_ref.name,
        )).data

        ca_ref = kclient.V1ObjectReference(
            name=ctx.parent_spec["caConfigMapName"],
            namespace=ctx.namespace)
        ca_path = await base.write_ca_certificates(ca_ref, ctx)

        conn_info = {
            "auth": {
                "auth_url": endpoint_config["OS_AUTH_URL"],
                "username": credentials["OS_USERNAME"],
                "project_name": credentials["OS_PROJECT_NAME"],
                "user_domain_name": credentials["OS_USER_DOMAIN_NAME"],
                "project_domain_name": credentials["OS_PROJECT_DOMAIN_NAME"],
                "password": credentials["OS_PASSWORD"],
            },
            "auth_plugin": credentials["OS_AUTH_TYPE"],
            "interface": endpoint_config["OS_INTERFACE"],
            "identity_api_version": endpoint_config["OS_IDENTITY_API_VERSION"],
            "cacert": ca_path,
        }
        if "region" in ctx.parent_spec:
            conn_info["region_name"] = \
                ctx.parent_spec["region"]["name"]
        return conn_info


# TODO(resource-refactor): should this be typing.Protocol instead of
# asyncio.Protocol?
class BackgroundJobController(asyncio.Protocol):
    async def should_run_background_job(
            self,
            ctx: context.Context,
            ) -> bool:
        pass

    async def make_background_job_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> ResourceBody:
        pass


class BackgroundJob(Job):
    def __init__(
            self,
            *,
            controller: BackgroundJobController,
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self._controller = controller

    async def _make_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> ResourceBody:
        return await self._controller.make_background_job_body(
            ctx,
            dependencies,
        )

    async def reconcile(
            self,
            ctx: context.Context,
            **kwargs: typing.Any,
            ) -> None:
        should_run = await self._controller.should_run_background_job(
            ctx,
        )
        if should_run:
            return await super().reconcile(
                ctx,
                **kwargs,
            )

        try:
            existing = await self.get(ctx)
        except exceptions.ResourceNotPresent:
            return
        else:
            intf = self.get_resource_interface(ctx)
            await intf.delete(existing.namespace, existing.name)

    async def is_ready(self, ctx: context.Context) -> bool:
        should_run = await self._controller.should_run_background_job(ctx)
        if should_run:
            return await super().is_ready(ctx)
        return True


class ResourceStatus(typing.NamedTuple):
    # up / alive
    up: bool
    # enabled / admin_state_up
    enabled: bool
    # disable_reason (just for nova compute)
    disable_reason: typing.Optional[str]


class APIStateResource(OpenStackMixin,
                       TemplateMixin,
                       ExternalResource,
                       metaclass=ABCMeta):
    """
    Manage the state of a <something> resource.

    :param scheduling_keys: Initialises :class:`_background_job`

    :param eviction_job_template: Initialises :attr:`_eviction_job_template`

    :param job_endpoint_config: Initialises :attr:`_job_endpoint_config`

    TODO:
    .. seealso::

        :class:`~.KubernetesResource`, :class:`~.Resource`
            for more supported arguments.

    **Public interface:**

    .. attribute:: copy_on_write

        TODO:

    .. autoattribute::

    .. automethod::

    **Methods to be implemented by subclasses:**

    .. automethod:: _get_status

    .. automethod:: _update_status

    .. automethod:: _next_state

    **Interface for subclasses:**

        TODO

    **Internal API:**

    .. automethod:: _orphan
    """
    def __init__(
            self,
            *,
            scheduling_keys: typing.Collection[str],
            eviction_job_template: str,
            job_endpoint_config: KubernetesReference[
                kclient.V1ConfigMap,
            ],
            **kwargs: typing.Any):
        self._background_job = BackgroundJob(
            controller=typing.cast(BackgroundJobController, self),
            scheduling_keys=scheduling_keys,
        )
        super().__init__(**kwargs)
        self._eviction_job_template = eviction_job_template
        self._job_endpoint_config = job_endpoint_config
        self._declare_dependencies(job_endpoint_config)

    def _set_component(self, component: str) -> None:
        super()._set_component(component)
        self._background_job.__set_name__(type(self), component)

    def get_listeners(self) -> typing.List[context.Listener]:
        return super().get_listeners() + self._background_job.get_listeners()

    async def should_run_background_job(
            self,
            ctx: context.Context,
            ) -> bool:
        # NOTE: the ctx may have been modified by the reconcile method at this
        # point to influence the judgement of this method accordingly.
        return ctx.parent["status"]["eviction"] is not None

    async def make_background_job_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> ResourceBody:
        credentials_secret_name = \
            (await self._credentials_secret.get(ctx)).name
        endpoint_config_name = \
            (await self._job_endpoint_config.get(ctx)).name

        params = dict(self._params)
        params.setdefault("credentials_secret", credentials_secret_name)
        params.setdefault("endpoint_config", endpoint_config_name)

        wrapped_dependencies = {
            k: k8s.DependencyTemplateWrapper(ctx, dep)
            for k, dep in dependencies.items()
            if isinstance(dep, k8s.KubernetesResource)
        }

        return await self._render_template(
            self._eviction_job_template,
            {
                "params": params,
                "vars": {
                    "reason": ctx.parent["status"]["eviction"]["reason"],
                    "node_name": ctx.parent["metadata"]["name"],
                },
                "labels": self._background_job.labels(ctx),
                "dependencies": wrapped_dependencies,
                "crd_spec": ctx.parent_spec,
            },
        )

    def _get_status(
            self,
            ctx: context.Context,
            connection_info: typing.Mapping[str, typing.Any],
            ) -> typing.Optional[ResourceStatus]:
        client = openstack.connect(**connection_info)
        resource = self._get_resource(client, ctx)
        return resource

    @abstractmethod
    def _get_resource(
            self,
            client: openstack.connection.Connection,
            ctx: context.Context,
            ) -> typing.Optional[ResourceStatus]:
        """
        Get the status of an OpenStack object (e.g: compute service).

        :return: NamedTuple with the status' of the object.
        """
        raise NotImplementedError

    @abstractmethod
    def _update_status(
            self,
            ctx: context.Context,
            connection_info: typing.Mapping[str, typing.Any],
            enabled: bool,
            ) -> typing.Optional[ResourceStatus]:
        """
        Update the status of an OpenStack object (e.g: compute service).

        :return: NamedTuple with the updated status' of the object.
        """
        raise NotImplementedError

    def _get_node_name(
            self,
            ctx: context.Context,
            ) -> str:
        """
        Get the name of the node which is hosting the OpenStack agent.
        """
        return ctx.parent_name

    async def _next_state(
            self,
            ctx: context.Context,
            status: typing.Optional[ResourceStatus],
            ) -> typing.Tuple[
                ResourceStatusState,
                typing.Optional[EvictionIntent],
            ]:
        # NOTE: automatic eviction because of service/agent status not being up
        # should be handled by the operator itself since it has the global
        # view; it can force evictions using the state attribute.
        #
        # This state transition function should only cover cases where it is
        # completely unambiguous that *not* evicting would be a terrible idea.
        # Those are cases where data loss is imminent or where the intent has
        # been made clear by the owner of the resource:
        #
        # - Node/Agent unreachable: That is very likely an effective data plane
        #   disruption and the eviction should start ASAP.
        # - Network unavailable: Also very likely a data plane disruption which
        #   should be taken care of immediately.
        # - Node/Agent deleted: AHAHahahahaah omg
        # - Node/Agent deleting: We should get our ass out of there ASAP.
        # - Resource deleting: Intent from the owner that we should completely
        #   shut down.
        # - spec.state set to DisabledAndCleared: Intent from the owner that
        #   we should clear out the node/agent.
        spec_state = ResourceSpecState(
            ctx.parent["spec"]["state"]
        )
        current_state = ResourceStatusState(
            ctx.parent.get("status", {}).get("state", "Creating"),
        )
        is_deleting = \
            ctx.parent["metadata"].get("deletionTimestamp") is not None
        node_name = self._get_node_name(ctx)
        reason = ResourceEvictingReason.DESIRED_STATE

        if current_state == ResourceStatusState.EVICTING:
            eviction_done = await self._background_job.is_ready(ctx)
            # NOTE: If we ever want to allow aborting an eviction by changing
            # the spec.state, we have to be careful to test this code path
            # well; we must not abort an eviction just because of an Enabled
            # spec.state if there are other indications to continue to evict
            # (e.g. deletion, node down etc.)
            if not eviction_done:
                reason = ResourceEvictingReason(
                    ctx.parent["status"]["eviction"]["reason"]
                )
                return (
                    ResourceStatusState.EVICTING,
                    EvictionIntent(
                        reason=reason,
                    )
                )
            current_state = ResourceStatusState.DISABLED_AND_CLEARED

        v1 = kclient.CoreV1Api(ctx.api_client)
        try:
            node = await v1.read_node(node_name)
        except kclient.ApiException as exc:
            if exc.status == 404:
                # oh my god we are super screwed!
                is_node_down = True
                is_node_deleting = True
            else:
                raise
        else:
            is_node_deleting = node.metadata.deletion_timestamp is not None
            taint_keys = set(taint.key for taint in node.spec.taints or [])
            is_node_down = (
                # Node is not reachable by k8s, this means that it cannot be
                # managed properly anymore. The network may still be intact
                # one way or another, so trying to migrate stuff away while
                # the human is still getting out of bed is nice.
                "node.kubernetes.io/unreachable" in taint_keys or

                # I’m not sure we can do anything here without human
                # intervention (set forced-down to allow evacuation), but
                # getting the job warmed up is probably a good thing anyway.
                "node.kubernetes.io/network-unavailable" in taint_keys
            )

        is_deleting = is_deleting or is_node_deleting

        if is_deleting:
            # deletion is stronger than the spec state, so we override it here
            spec_state = ResourceSpecState.DISABLED_AND_CLEARED
            reason = ResourceEvictingReason.DELETING

        if is_node_down:
            # we are so screwed
            spec_state = ResourceSpecState.DISABLED_AND_CLEARED
            reason = ResourceEvictingReason.NODE_DOWN

        if current_state == ResourceStatusState.CREATING:
            # if the node is deleting, we have no guarantee whatsoever that
            # the service will ever come up.
            # we do the full eviction dance (which works because it is
            # resilient against a non-existent service and will simply complete
            # as it will not find any matching instances)
            if not is_node_down and not is_node_deleting and (
                    status is None or not status.up):
                return ResourceStatusState.CREATING, None

        # Note that we override spec_state in some cases above.
        if spec_state == ResourceSpecState.DISABLED_AND_CLEARED:
            if (current_state !=
                    ResourceStatusState.DISABLED_AND_CLEARED):
                return (
                    ResourceStatusState.EVICTING,
                    EvictionIntent(
                        reason=reason,
                    ),
                )

        return spec_state.to_status_state(), None

    async def update(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> None:
        conn_info = await self.get_openstack_connection_info(ctx)
        loop = asyncio.get_event_loop()
        status = await loop.run_in_executor(
            None,
            self._get_status,
            ctx,
            conn_info,
        )
        ctx.logger.debug("obtained status: %s", status)

        next_state, eviction_intent = await self._next_state(
            ctx,
            status,
        )
        ctx.logger.debug("determined next state: %s (eviction_intent: %s)",
                         next_state, eviction_intent)

        status = await loop.run_in_executor(
            None,
            self._update_status,
            ctx,
            conn_info,
            next_state == ResourceStatusState.ENABLED,
        )
        ctx.logger.debug("updated status in OpenStack: %s",
                         status)

        additional_patch = {
            "eviction": None if eviction_intent is None else {
                "reason": eviction_intent.reason.value,
            },
            "state": next_state.value,
        }

        await customresource.update_status(
            ctx.parent_intf,
            ctx.namespace,
            ctx.parent_name,
            conditions=[
                customresource.ConditionUpdate(
                    type_=ResourceCondition.ENABLED.value,
                    reason="Unknown",
                    message="",
                    status=str(
                        status.enabled
                        if status is not None
                        else "Unknown"
                    ),
                ),
                # TODO: bootstrap eviction status if it does not exist
            ],
            additional_patch=additional_patch,
        )

        # This allows the should_run_background_job and
        # make_background_job_body methods when called from the background job
        # to return the correct information without having to re-do the logic
        # here with potentially diverging input.
        ctx.parent["status"].update(additional_patch)

        await self._background_job.reconcile(
            ctx,
            dependencies=dependencies,
        )

    # override where l2 agent is needed, so delete() will remove the
    # migration lock annotation from node
    def _needs_l2_agent(self) -> bool:
        return False

    async def delete(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> None:
        if (await self.update(ctx, dependencies) or
                not await self._background_job.is_ready(ctx)):
            raise exceptions.ContinueableError(
                "Eviction still in progress",
                ready=False,
            )
        if self._needs_l2_agent():
            v1 = kclient.CoreV1Api(ctx.api_client)
            await v1.patch_node(
                ctx.parent_name,
                [{
                    "op": "remove",
                    "path": jsonpatch.JsonPointer.from_parts(
                        [
                            "metadata", "annotations",
                            LOCK_NAME_FORMAT.format(kind=ctx.parent_kind)
                        ]).path,
                    "value": "",
                }]
            )

    def _handle_neutron_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[
                openstack.network.v2.agent.Agent],
            ) -> bool:
        if (event.type_ == watcher.EventType.ADDED or
                event.type_ == watcher.EventType.DELETED or
                event.old_object is None):
            return True
        if (datetime.strptime(
                    event.old_object.started_at, "%Y-%m-%d %H:%M:%S") <=
                ctx.creation_timestamp and
                datetime.strptime(
                    event.object_.started_at, "%Y-%m-%d %H:%M:%S") >
                ctx.creation_timestamp):
            return True
        return (
            event.old_object.is_admin_state_up !=
            event.object_.is_admin_state_up or
            event.old_object.is_alive !=
            event.object_.is_alive
        )


# Cached list of nodes for which the L2 maintenance is required. Making use
# of ContextVar to ensure same cache between different async threads
nodes_in_maintenance: ContextVar = ContextVar(
    "nodes_in_maintenance", default=set())


class StatefulAgentResource(instancing.StatefulPerNode[typing.Mapping]):
    @abstractmethod
    def get_listener(self) -> typing.List[context.Listener]:
        raise NotImplementedError

    def get_listeners(self) -> typing.List[context.Listener]:
        return super().get_listeners() + self.get_listener()

    async def reconcile(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> None:
        nodes_in_maintenance.set(await self._get_nodes_in_maintenance(ctx))
        await super().reconcile(ctx, dependencies)

    async def _get_nodes_in_maintenance(
        self,
        ctx: context.Context,
    ) -> typing.Collection[str]:
        return []

    def _handle_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[typing.Mapping]) -> bool:
        if event.type_ in [watcher.EventType.ADDED, watcher.EventType.DELETED]:
            return True
        if event.old_object is None:
            old_status = {}
        else:
            old_status = event.old_object.get("status", {})
        new_status = event.object_.get("status", {})
        old_phase = old_status.get("phase")
        old_state = old_status.get("state")
        new_phase = new_status.get("phase")
        new_state = new_status.get("state")

        generation = event.object_.get("metadata", {}).get("generation")
        observed_generation = new_status.get("observedGeneration")
        if (new_phase != context.Phase.UPDATED.value or
                generation != observed_generation):
            return False

        if (old_phase, old_state) != (new_phase, new_state):
            return True

        if (self._get_recreation_condition(event.object_) and
           not self._get_recreation_condition(event.old_object or {})):
            return True

        return False

    def _get_run_state(
            self,
            ctx: context.Context,
            instance: typing.Mapping,
            ) -> instancing.ResourceRunState:
        # Running unless deleting
        if instance["metadata"].get("deletionTimestamp") is not None:
            return instancing.ResourceRunState.SHUTTING_DOWN
        status = instance.get("status", {})
        phase = status.get("phase")
        state = status.get("state")
        generation = instance.get("metadata", {}).get("generation")
        updated_generation = status.get("updatedGeneration")
        if ((phase == context.Phase.UPDATED.value or
             phase == context.Phase.UPDATING.value) and
                state != ResourceStatusState.CREATING.value and
                generation == updated_generation):
            return instancing.ResourceRunState.READY
        elif (phase == context.Phase.UPDATED.value or
                phase == context.Phase.UPDATING.value or
                phase == context.Phase.WAITING_FOR_DEPENDENCY.value or
                phase == context.Phase.CREATED.value or
                phase is None or
                state == ResourceStatusState.CREATING.value):
            return instancing.ResourceRunState.STARTING
        return instancing.ResourceRunState.UNKNOWN

    def _get_spec_state(
            self,
            ctx: context.Context,
            intent: typing.Mapping,
            instance: typing.Mapping,
            ) -> instancing.ResourceSpecState:
        if self._wrapped_state._needs_update(instance, intent):
            return instancing.ResourceSpecState.STALE
        if self._get_recreation_condition(instance):
            return instancing.ResourceSpecState.STALE
        if self._does_node_require_maintenance(ctx):
            return instancing.ResourceSpecState.STALE

        return instancing.ResourceSpecState.UP_TO_DATE

    def _get_recreation_condition(self,
                                  instance: typing.Mapping
                                  ) -> typing.Optional[bool]:
        for condition in instance.get("status", {}).get("conditions", []):
            if condition.get("type") == \
                    ResourceCondition.REQUIRES_RECREATION.value:
                return condition.get("status") == "True"
        return None

    def _does_node_require_maintenance(
            self,
            ctx: context.Context,
            ) -> bool:
        if ctx.instance is None:
            raise AssertionError()
        nodes = nodes_in_maintenance.get()
        for node in nodes:
            if node == ctx.instance:
                return True

        return False

    async def _compile_full_state(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            target_instances: typing.Mapping[str, typing.Any],
            assignment: typing.Mapping[
                str, typing.Collection[instancing.ResourceInfo]],
            ) -> typing.Tuple[
                typing.Mapping[str, instancing.InstanceState],
                typing.Collection[kclient.V1ObjectReference],
            ]:
        full_state, new_to_delete = await super()._compile_full_state(
            ctx, dependencies, target_instances, assignment)

        for instance, instance_data in target_instances.items():
            instance_ctx = ctx.with_instance(
                instance,
                instance_data
            )
            if instance in full_state and self._does_node_require_maintenance(
                    instance_ctx):
                full_state[instance].may_create_resources = False

        return full_state, new_to_delete


class TemplatedRecreatingStatefulSet(TemplatedStatefulSet):
    async def _update_resource(self,
                               ctx: context.Context,
                               _cur: kclient.V1StatefulSet,
                               _new: ResourceBody) -> bool:
        await customresource.update_status(
            ctx.parent_intf,
            ctx.namespace,
            ctx.parent_name,
            conditions=[
                customresource.ConditionUpdate(
                    type_=ResourceCondition.REQUIRES_RECREATION.value,
                    status="True",
                    reason="",
                    message=f"Resource {self.component} may "  # nosemgrep
                            "not be updated in place and requires recreation",
                ),
            ]
        )
        return True


class L2AwareStatefulAgentResource(StatefulAgentResource):
    """
    Each Resource depending on l2 agent must inherit from this class, so
    we check the node for l2 related label and evict and remove the
    resource if l2 agent needs to be updated or removed.

    .. seealso::

        :class:`~.StatefulAgentResource`, :class:`~.StatefulAgentResource`
            for supported arguments.
    """
    async def _get_nodes_in_maintenance(
        self,
        ctx: context.Context,
    ) -> typing.Collection[str]:
        selectors = typing.cast(typing.List,
                                instancing._scheduling_keys_to_selectors(
                                    self._scheduling_keys)
                                )
        label_selectors = []
        for selector in selectors:
            label_selectors.append(
                api_utils.LabelSelector(
                    match_expressions=[
                        *selector.match_expressions,
                        api_utils.LabelExpression(
                            key=context.LABEL_L2_REQUIRE_MIGRATION,
                            operator=api_utils.SelectorOperator.NOT_IN,
                            values=["False"],
                        )
                    ]
                )
            )
            label_selectors.append(
                api_utils.LabelSelector(
                    match_expressions=[
                        *selector.match_expressions,
                        api_utils.LabelExpression(
                            key=context.LABEL_L2_REQUIRE_MIGRATION,
                            operator=api_utils.SelectorOperator.NOT_EXISTS,
                            values=None
                        )
                    ]
                )
            )

        nodes = list(await interfaces.get_selected_nodes_union(
                    ctx.api_client,
                    label_selectors,)
                    )
        return [node.metadata.name for node in nodes]

    def _handle_node_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[
                kclient.V1Node,
            ],
            ) -> bool:
        if (event.type_ == watcher.EventType.ADDED or
                event.type_ == watcher.EventType.DELETED or
                event.old_object is None):
            return True
        # has the node stopped matching the scheduling key?
        for key in self._scheduling_keys:
            if key in event.old_object.metadata.labels.keys() and \
                    key not in event.object_.metadata.labels.keys():
                return True
        # has the node gained its maintenance required label and is matching
        # the scheduling key?
        # has the node switched maintenance required from true to false or
        # vice versa and is matching the scheduling key?
        for key in self._scheduling_keys:
            if key in event.object_.metadata.labels.keys() and \
                    {k: v for k, v in event.old_object.metadata.labels.items()
                        if context.LABEL_L2_REQUIRE_MIGRATION in k} != \
                    {k: v for k, v in event.object_.metadata.labels.items()
                        if context.LABEL_L2_REQUIRE_MIGRATION in k}:
                return True
        # has the node started matching the scheduling key and has the
        # maintenance required label set to false?
        for key in self._scheduling_keys:
            if key not in event.old_object.metadata.labels.keys() and \
                    key in event.object_.metadata.labels.keys() and \
                    event.object_.metadata.labels.get(
                    context.LABEL_L2_REQUIRE_MIGRATION, True) == 'False':
                return True
        return False


class NeutronAgentWatcher(
        watcher.ExternalWatcher[openstack.network.v2.agent.Agent]):

    def __init__(self,
                 kind: str,
                 binary: str,
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self.kind = kind
        self.binary = binary

    def to_kubernetes_reference(self, name: str) -> kclient.V1ObjectReference:
        if self.namespace:
            return kclient.V1ObjectReference(
                api_version="network.yaook.cloud/v1",
                kind=self.kind,
                name=name,
                namespace=self.namespace,
            )
        raise ValueError("Trying to generate a object reference while we "
                         "do not know the namespace yet.")

    def _get_state(self) -> typing.Mapping[
            str, openstack.network.v2.agent.Agent]:
        if self.connection_parameters:
            client = openstack.connect(**self.connection_parameters)
            agents = client.network.agents(binary=self.binary)
            return {a.host: a for a in agents}
        return {}

    async def get_state(self) -> typing.Mapping[
            str, openstack.network.v2.agent.Agent]:
        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(
            None,
            self._get_state,
        )

    def to_dict(self, obj: openstack.network.v2.agent.Agent
                ) -> typing.Mapping[str, typing.Any]:
        return obj.to_dict()


@dataclasses.dataclass(repr=False)
class _OpaqueParameters:
    """
    This is an opaque wrapper to prevent the :attr:`params` from being logged
    in clear text accidentally.
    """
    params: typing.Mapping[str, typing.Any]


class KeystoneResource(ExternalResource):
    def __init__(
            self,
            *,
            admin_credentials: KubernetesReference[kclient.V1Secret],
            endpoint_config: KubernetesReference[kclient.V1ConfigMap],
            ca_config: KubernetesReference[kclient.V1ConfigMap],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._admin_credentials = admin_credentials
        self._endpoint_config = endpoint_config
        self._ca_config = ca_config
        self._declare_dependencies(
            admin_credentials, endpoint_config, ca_config,
        )

    async def _get_connection_params(
            self,
            ctx: context.Context,
            region: typing.Optional[str] = None
            ) -> _OpaqueParameters:
        credentials_ref = await self._admin_credentials.get(ctx)
        endpoint_cfg_ref = await self._endpoint_config.get(ctx)

        configmaps = interfaces.config_map_interface(ctx.api_client)
        secrets = interfaces.secret_interface(ctx.api_client)

        credentials = interfaces.api_utils.decode_secret_data(
            (await secrets.read(credentials_ref.namespace,
                                credentials_ref.name)).data
        )
        endpoint_cfg = (await configmaps.read(endpoint_cfg_ref.namespace,
                                              endpoint_cfg_ref.name)).data

        ca_config_ref = await self._ca_config.get(ctx)
        ca_path = await base.write_ca_certificates(ca_config_ref, ctx)

        conn_info = {
            "auth": {
                "auth_url": endpoint_cfg["OS_AUTH_URL"],
                "password": credentials["OS_PASSWORD"],
                "username": credentials["OS_USERNAME"],
                "user_domain_name": credentials["OS_USER_DOMAIN_NAME"],
                "project_name": credentials["OS_PROJECT_NAME"],
                "project_domain_name": credentials["OS_PROJECT_DOMAIN_NAME"],
            },
            "interface": endpoint_cfg["OS_INTERFACE"],
            "identity_api_version": endpoint_cfg["OS_IDENTITY_API_VERSION"],
            "cacert": ca_path,
        }
        if "region" in ctx.parent_spec:
            conn_info["region_name"] = \
                ctx.parent_spec["region"]["name"]
        if region:
            conn_info["region_name"] = region
        return _OpaqueParameters(params=conn_info)


class L2ProvidingAgentStateResource(OpenStackMixin,
                                    TemplateMixin,
                                    ExternalResource):
    def _handle_agent_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[
                openstack.network.v2.agent.Agent],
            ) -> bool:
        if event.object_.host != ctx.parent_name:
            return False
        if (event.type_ == watcher.EventType.ADDED or
                event.type_ == watcher.EventType.DELETED or
                event.old_object is None):
            return True
        # ovn agents have started_at not set, so filter it out here
        if (event.old_object.started_at is not None and
                datetime.strptime(
                    event.old_object.started_at, "%Y-%m-%d %H:%M:%S") <=
                ctx.creation_timestamp and
                datetime.strptime(
                    event.object_.started_at, "%Y-%m-%d %H:%M:%S") >
                ctx.creation_timestamp):
            return True
        return (
            event.old_object.is_admin_state_up !=
            event.object_.is_admin_state_up or
            event.old_object.is_alive !=
            event.object_.is_alive
        )

    def _handle_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[typing.Mapping]
            ) -> bool:
        node = typing.cast(kclient.V1Node, event.object_)
        if node.metadata is None or node.metadata.name != ctx.parent_name:
            return False
        if event.type_ in [
                watcher.EventType.ADDED,
                watcher.EventType.DELETED,
                ]:
            return True

        obj_annotations = node.metadata.annotations or {}
        old_object = typing.cast(kclient.V1Node, event.old_object)
        if old_object is None or old_object.metadata is None:
            old_annotations: typing.Dict = {}
        else:
            old_object = typing.cast(kclient.V1Node, event.old_object)
            old_annotations = old_object.metadata.annotations or {}
        # This is a bit tricky, we want to get notified, when the l2
        # migration lock annotation got removed.
        # We can only compare the prefix of the annotation, due to each agent
        # will add it's name to the annotation
        return (
            any(
                [context.ANNOTATION_L2_MIGRATION_LOCK in x
                 for x in old_annotations.keys()]
            )
            and not
            any(
                [context.ANNOTATION_L2_MIGRATION_LOCK in x
                 for x in obj_annotations.keys()]
            )
        )

    @abstractmethod
    def _get_agent(
            self,
            network_client: openstack.network.v2._proxy.Proxy,
            host: str,
            ) -> typing.Optional[openstack.network.v2.agent.Agent]:
        """
        each L2ProvidingAgentStateResource needs to implement this method,
        so we get the correct agent object from network API.
        """
        raise NotImplementedError

    # This funktion has similar purpose like _get_status() and _get_resource()
    # at APIStateResource, but we use them differently here, as we may need to
    # enable the agent at _update_status().
    # So here we only check the agents last_heartbeat_at and return the agent.
    def _check_agent(
            self,
            ctx: context.Context,
            client: openstack.connection.Connection,
            ) -> typing.Optional[openstack.network.v2.agent.Agent]:
        """
        This funktion has similar purpose like _get_status() and
        _get_resource() at APIStateResource, but we use them differently here,
        as we may need to enable the agent at _update_status().
        So here we only check the agents last_heartbeat_at and return the
        agent.
        """
        agent = self._get_agent(client.network, ctx.parent_name)
        if agent is None:
            return None

        # If the agent was last time updated before we were created
        # it is probably an old instance that has not been cleaned up.
        # In this case we just behave as if the agent was not known to neutron.
        # The used format string is taken from the neutron repository here,
        # but for reasons, the agent object returns a space instead of 'T'
        # between date and time.
        # https://opendev.org/openstack/neutron/src/branch/master/neutron/services/timestamp/timestamp_db.py#L25  # noqa: E501
        # ovn agent has miliseconds and timezone info at heartbeat, so split
        # that of
        # https://opendev.org/openstack/neutron/src/branch/master/neutron/plugins/ml2/drivers/ovn/agent/neutron_agent.py#L54  # noqa: E501
        # In case of ovn agent is down, no miliseconds are given, but '+00:00'
        # that we don't want to parse.
        updated_at = datetime.strptime(
            agent.last_heartbeat_at.split('.')[0].removesuffix('+00:00'),
            "%Y-%m-%d %H:%M:%S")
        created_at = ctx.creation_timestamp
        if updated_at < created_at:
            ctx.logger.warn(
                "l2 providing agent for %s was updated before this resource "
                "was created. Treating it as non existent", ctx.parent_name)
            return None

        return agent

    async def _maintenance_required_label(
            self,
            ctx: context.Context,
            op: str,
            require_maintenance: bool,
            ) -> None:
        # When the l2 providing agent got added, we set an label to the node,
        # so other operators know, it is safe to schedule agents/services on
        # the node, needing l2 support (l2-agent/ovn-controller).
        # When we change the maintenance label to True, other operators
        # can evict and remove their resources needing l2
        v1 = kclient.CoreV1Api(ctx.api_client)
        await v1.patch_node(
            ctx.parent_name,
            [
                {
                    "op": op,
                    "path": jsonpatch.JsonPointer.from_parts([
                        "metadata", "labels",
                        context.LABEL_L2_REQUIRE_MIGRATION]).path,
                    "value": str(require_maintenance),
                },
            ],
        )

    async def _maintenance_lock_gone(
            self,
            ctx: context.Context,
            name: str,
            ) -> typing.Collection[str]:
        # We need to wait, till all other agents/services needing
        # l2 are evicted and removed. After that, the
        # l2 providing agent can be deleted.
        v1 = kclient.CoreV1Api(ctx.api_client)
        lockers = []
        node_info = await v1.read_node(name)
        for annotation in node_info.metadata.annotations.keys():
            if context.ANNOTATION_L2_MIGRATION_LOCK in annotation:
                ctx.logger.debug(
                    "An agent still locks l2 migration, label: %s, node: %s",
                    annotation, name)
                lockers.append(
                    annotation[len(context.ANNOTATION_L2_MIGRATION_LOCK):]
                )
        if not lockers:
            ctx.logger.debug("Node %s has no l2 migration locks.", name)
        return lockers

    async def delete(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> None:
        await self._maintenance_required_label(
            ctx,
            "add",
            True,
            )

        additional_patch = {
            "state": ResourceStatusState.EVICTING.value,
        }
        await customresource.update_status(
            ctx.parent_intf,
            ctx.namespace,
            ctx.parent_name,
            additional_patch=additional_patch,
        )

        lockers = await self._maintenance_lock_gone(ctx, ctx.parent_name)
        if lockers:
            raise exceptions.ContinueableError(
                "Waiting for {} to release the l2 lock".format(
                    ", ".join(lockers)
                ),
                ready=False,
            )


class PolicyValidator(TemplatedJob):
    def __init__(
        self,
        *,
        policy_key: str = "policy",
        **kwargs: typing.Any
    ):
        super().__init__(**kwargs)
        self._policy_key = policy_key

    async def _get_template_parameters(
            self,
            ctx: context.Context,
            dependencies: dependencies.DependencyMap
            ) -> resources.TemplateParameters:
        params = await super()._get_template_parameters(ctx, dependencies)
        params["vars"]["policy"] = json.dumps(
            ctx.parent_spec.get(self._policy_key, {})
        )
        return params
