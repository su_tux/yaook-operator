#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
from datetime import datetime
import ddt
import unittest
import unittest.mock
from unittest.mock import sentinel
import uuid

import kubernetes_asyncio.client as kclient
import openstack.exceptions as openstack_exceptions
import openstack.network
import typing

from yaook.statemachine.resources.base import DependencyMap
import yaook.statemachine.context as context
import yaook.statemachine.resources.instancing as instancing
import yaook.statemachine.resources.openstack as resource
import yaook.statemachine.watcher as watcher

from ...statemachine.resources.utils import SingleObjectMock


class TestTemplatedRecreatingStatefulSet(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.trss = resource.TemplatedRecreatingStatefulSet(
            template="whocares.yml",
            versioned_dependencies=[],
            component=str(unittest.mock.sentinel.component),
        )
        self.parent_intf = unittest.mock.Mock()
        self.parent_intf.replace_status = unittest.mock.AsyncMock()
        self.parent_intf.read_status = unittest.mock.AsyncMock(return_value={})

        self.ctx = unittest.mock.Mock([])
        self.ctx.parent_intf = self.parent_intf
        self.ctx.namespace = sentinel.namespace
        self.ctx.parent_name = sentinel.parent_name

    async def test_sets_condition_instead_of_updating(self):
        with contextlib.ExitStack() as stack:
            get_resource_interface = stack.enter_context(
                unittest.mock.patch.object(
                    self.trss, "get_resource_interface",
                )
            )

            res = await self.trss._update_resource(self.ctx, None, None)

        self.assertTrue(res)
        get_resource_interface.assert_not_called()

        args, = self.parent_intf.replace_status.call_args_list
        self.assertEqual(args[0][0], sentinel.namespace)
        self.assertEqual(args[0][1], sentinel.parent_name)
        conditions = args[0][2]["status"]["conditions"]
        requires_recreation = [
            c for c in conditions if c["type"] == "RequiresRecreation"][0]
        self.assertEqual("True", requires_recreation["status"])


class TestL2AwareStatefulAgentResource(unittest.IsolatedAsyncioTestCase):
    class L2AwareStatfulAgentResourceTest(resource.L2AwareStatefulAgentResource):  # noqa: E501
        def get_listener(self):
            pass

    def setUp(self):
        self.wrapped = SingleObjectMock()
        self.scheduling_keys = ["foobar"]
        self.cr = self.L2AwareStatfulAgentResourceTest(
            scheduling_keys=self.scheduling_keys,
            wrapped_state=self.wrapped,
            component=unittest.mock.sentinel.component,
        )
        self.node_name = str(uuid.uuid4())

        self.event = unittest.mock.Mock([])
        self.event.type_ = watcher.EventType.MODIFIED
        self.event.object_ = unittest.mock.Mock([])
        self.event.object_.metadata = unittest.mock.Mock([])
        self.event.object_.metadata.labels = {
            'yaook.test': '',
            'foobar': '',  # scheduling_keys
        }
        self.event.object_.metadata.name = self.node_name
        self.event.old_object = unittest.mock.Mock()
        self.event.old_object.metadata.labels = {
            'yaook.test': '',
            'foobar': '',  # scheduling_keys
        }

    def _make_state_context(self):
        ctx = unittest.mock.Mock(["api_client"])
        ctx.parent_name = self.node_name
        ctx.logger = unittest.mock.Mock()
        return ctx

    async def test__get_nodes_in_maintenance_is_nodes_with_l2_label(self):
        nodes = {
            "node1": {
                "foobar": "$something",
                context.LABEL_L2_REQUIRE_MIGRATION: "False",
            },
            "node2": {
                "foobar": "$something",
                context.LABEL_L2_REQUIRE_MIGRATION: "True",
            },
            "node3": {
                "foobar": "$something",
                context.LABEL_L2_REQUIRE_MIGRATION: "cake",
            },
            "node4": {
                "foobar": "$something",
            },
            "node5": {
                "notfoobar": "$something",
                context.LABEL_L2_REQUIRE_MIGRATION: "False",
            },
        }

        def mock_get_selected_nodes_union(_, label_selectors):
            ret = {}
            for selector in label_selectors:
                for node, labels in nodes.items():
                    if selector.object_matches(labels):
                        ret[node] = kclient.V1Node(
                            metadata=kclient.V1ObjectMeta(name=node))
            return list(ret.values())

        with contextlib.ExitStack() as stack:
            get_selected_nodes_union = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.get_selected_nodes_union"))

            get_selected_nodes_union.side_effect = \
                mock_get_selected_nodes_union

            ret = await self.cr._get_nodes_in_maintenance(
                self._make_state_context())

        self.assertListEqual(
            ["node2", "node3", "node4"],
            ret,
        )

    def test__handle_node_event_returns_true_for_added(self):
        self.event.type_ = watcher.EventType.ADDED

        self.assertTrue(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_node_event_returns_true_for_deleted(self):
        self.event.type_ = watcher.EventType.DELETED

        self.assertTrue(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_node_event_returns_true_if_old_object_none(self):
        self.event.old_object = None

        self.assertTrue(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_node_event_ignores_labels_differ_returns_false(self):
        self.event.old_object.metadata.labels = {
            'yaook.test': 'test42',
            'foobar': '',  # scheduling_keys
        }

        self.assertFalse(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_node_event_returns_false_if_label_equal(self):
        self.assertFalse(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_node_event_returns_true_if_scheduling_keys_removed(self):
        self.event.old_object.metadata.labels = {
            'yaook.test': '',
            'foobar': '',  # scheduling_keys
        }
        self.event.object_.metadata.labels = {
            'yaook.test': '',
        }

        self.assertTrue(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_node_event_returns_true_if_l2_label_removed(self):
        self.event.old_object.metadata.labels = {
            'yaook.test': '',
            'foobar': '',  # scheduling_keys
            context.LABEL_L2_REQUIRE_MIGRATION: 'True'
        }

        self.assertTrue(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_event_returns_false_on_l2_label_remove_if_scheduling_keys_missing(self):  # noqa: E501
        self.event.old_object.metadata.labels = {
            'yaook.test': '',
            context.LABEL_L2_REQUIRE_MIGRATION: 'True'
        }
        self.event.object_.metadata.labels = {
            'yaook.test': '',
        }

        self.assertFalse(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_node_event_returns_true_if_l2_label_added(self):
        self.event.object_.metadata.labels = {
            'yaook.test': '',
            'foobar': '',  # scheduling_keys
            context.LABEL_L2_REQUIRE_MIGRATION: 'False'
        }

        self.assertTrue(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_event_returns_false_on_l2_label_add_if_scheduling_keys_missing(self):  # noqa: E501
        self.event.old_object.metadata.labels = {
            'yaook.test': '',
        }
        self.event.object_.metadata.labels = {
            'yaook.test': '',
            context.LABEL_L2_REQUIRE_MIGRATION: 'False'
        }

        self.assertFalse(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_node_event_returns_true_if_l2_label_changed(self):
        self.event.old_object.metadata.labels = {
            'yaook.test': '',
            'foobar': '',  # scheduling_keys
            context.LABEL_L2_REQUIRE_MIGRATION: 'False'
        }
        self.event.object_.metadata.labels = {
            'yaook.test': '',
            'foobar': '',  # scheduling_keys
            context.LABEL_L2_REQUIRE_MIGRATION: 'True'
        }

        self.assertTrue(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_event_returns_false_on_l2_label_change_if_scheduling_keys_missing(self):  # noqa: E501
        self.event.old_object.metadata.labels = {
            'yaook.test': '',
            context.LABEL_L2_REQUIRE_MIGRATION: 'False'
        }
        self.event.object_.metadata.labels = {
            'yaook.test': '',
            context.LABEL_L2_REQUIRE_MIGRATION: 'True'
        }

        self.assertFalse(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_event_returns_true_if_scheduling_keys_added(self):
        self.event.old_object.metadata.labels = {
            'yaook.test': '',
            context.LABEL_L2_REQUIRE_MIGRATION: 'False'
        }
        self.event.object_.metadata.labels = {
            'yaook.test': '',
            'foobar': '',  # scheduling_keys
            context.LABEL_L2_REQUIRE_MIGRATION: 'False'
        }

        self.assertTrue(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_event_returns_false_on_scheduling_key_change_if_l2_label_missing(self):  # noqa: E501
        self.event.old_object.metadata.labels = {
            'yaook.test': '',
        }
        self.event.object_.metadata.labels = {
            'yaook.test': '',
            'foobar': '',  # scheduling_keys
        }

        self.assertFalse(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))

    def test__handle_event_returns_false_on_scheduling_key_change_if_l2_label_true(self):  # noqa: E501
        self.event.old_object.metadata.labels = {
            'yaook.test': '',
            context.LABEL_L2_REQUIRE_MIGRATION: 'True'
        }
        self.event.object_.metadata.labels = {
            'yaook.test': '',
            'foobar': '',  # scheduling_keys
            context.LABEL_L2_REQUIRE_MIGRATION: 'True'
        }

        self.assertFalse(self.cr._handle_node_event(
            unittest.mock.sentinel.ctx,
            self.event,
        ))


class Testraise_response_error_if_any(unittest.TestCase):
    def test_does_not_raise_for_200_status(self):
        for i in range(200, 210):
            resp = unittest.mock.Mock([])
            resp.status_code = i
            resource.raise_response_error_if_any(resp)

    def test_does_not_raise_for_300_status(self):
        for i in range(300, 310):
            resp = unittest.mock.Mock([])
            resp.status_code = i
            resource.raise_response_error_if_any(resp)

    def test_raises_http_exception_with_body_if_available(self):
        for i in list(range(400, 410)) + list(range(500, 510)):
            resp = unittest.mock.Mock()
            resp.status_code = i
            resp.json.return_value = {"message": "foobar"}
            with self.assertRaisesRegex(
                    openstack_exceptions.HttpException,
                    "foobar") as exc:
                resource.raise_response_error_if_any(resp)
            self.assertEqual(exc.exception.status_code, i)
            resp.json.assert_called_once_with()

    def test_raises_http_exception_with_body_if_available_in_nested(self):
        for i in list(range(400, 410)) + list(range(500, 510)):
            resp = unittest.mock.Mock()
            resp.status_code = i
            resp.json.return_value = {"foo": {"message": "foobar"}}
            with self.assertRaisesRegex(
                    openstack_exceptions.HttpException,
                    "foobar") as exc:
                resource.raise_response_error_if_any(resp)
            self.assertEqual(exc.exception.status_code, i)
            resp.json.assert_called_once_with()

    def test_raises_generic_http_exception_if_body_has_no_message(self):
        for i in list(range(400, 410)) + list(range(500, 510)):
            resp = unittest.mock.Mock()
            resp.status_code = i
            resp.json.return_value = {"not_message": "foobar"}
            with self.assertRaises(openstack_exceptions.HttpException) as exc:
                resource.raise_response_error_if_any(resp)
            self.assertEqual(exc.exception.status_code, i)
            resp.json.assert_called_once_with()

    def test_raises_generic_http_exception_if_body_is_no_dict(self):
        for i in list(range(400, 410)) + list(range(500, 510)):
            resp = unittest.mock.Mock()
            resp.status_code = i
            resp.json.return_value = None
            with self.assertRaises(openstack_exceptions.HttpException) as exc:
                resource.raise_response_error_if_any(resp)
            self.assertEqual(exc.exception.status_code, i)
            resp.json.assert_called_once_with()

    def test_raises_generic_http_exception_if_json_raises_obscure_error(self):
        class FooException():
            pass
        for i in list(range(400, 410)) + list(range(500, 510)):
            resp = unittest.mock.Mock()
            resp.status_code = i
            resp.json.side_effect = FooException()
            with self.assertRaises(openstack_exceptions.HttpException) as exc:
                resource.raise_response_error_if_any(resp)
            self.assertEqual(exc.exception.status_code, i)
            resp.json.assert_called_once_with()


@ddt.ddt
class TestStatefulAgentResource(unittest.IsolatedAsyncioTestCase):
    class MockStatefulAgentResource(resource.StatefulAgentResource):
        def get_listener(self):
            pass

    def setUp(self):
        self.wrapped = SingleObjectMock()
        self.csr = self.MockStatefulAgentResource(
            scheduling_keys=["foo", "bar"],
            wrapped_state=self.wrapped,
            component=unittest.mock.sentinel.component,
        )

    def test__handle_event_returns_true_for_added(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.ADDED

        self.assertTrue(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_event_returns_true_for_deleted(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.DELETED

        self.assertTrue(
            self.csr._handle_event(unittest.mock.sentinel.ctx, ev)
        )

    def test__handle_event_returns_true_for_changed_state(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = {
            "status": {
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
            }
        }
        ev.object_ = {
            "metadata": {
                "generation": "0",
            },
            "status": {
                "observedGeneration": "0",
                "state": unittest.mock.sentinel.state2,
                "phase": context.Phase.UPDATED.value,
            }
        }

        self.assertTrue(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_event_returns_true_for_changed_phase(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = {
            "status": {
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.BACKING_OFF.value,
            }
        }
        ev.object_ = {
            "metadata": {
                "generation": "0",
            },
            "status": {
                "observedGeneration": "0",
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
            }
        }

        self.assertTrue(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_event_returns_false_if_phase_and_status_equal(self):  # NOQA
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = {
            "status": {
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
            }
        }
        ev.object_ = {
            "metadata": {
                "generation": "0",
            },
            "status": {
                "observedGeneration": "0",
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
            }
        }

        self.assertFalse(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_event_returns_false_if_phase_not_updated(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = {
            "status": {
                "state": unittest.mock.sentinel.state2,
                "phase": context.Phase.UPDATED.value,
            }
        }
        ev.object_ = {
            "metadata": {
                "generation": "0",
            },
            "status": {
                "observedGeneration": "0",
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATING.value,
            }
        }

        self.assertFalse(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_event_returns_false_if_generation_not_synced(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = {
            "status": {
                "state": unittest.mock.sentinel.state2,
                "phase": context.Phase.UPDATED.value,
            }
        }
        ev.object_ = {
            "metadata": {
                "generation": "1",
            },
            "status": {
                "observedGeneration": "0",
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
            }
        }

        self.assertFalse(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_event_returns_false_if_phase_and_status_equal_and_othervfields_differ(self):  # NOQA
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = {
            "status": {
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
                "conditions": [
                    {
                        "type": str(unittest.mock.sentinel.conditions1),
                        "status": "True",
                    }
                ],
            }
        }
        ev.object_ = {
            "metadata": {
                "generation": "0",
            },
            "status": {
                "observedGeneration": "0",
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
                "conditions": [
                    {
                        "type": str(unittest.mock.sentinel.conditions2),
                        "status": "True",
                    }
                ],
            }
        }

        self.assertFalse(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_event_returns_true_if_recreate_condition_gets_set(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = {
            "status": {
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
            }
        }
        ev.object_ = {
            "metadata": {
                "generation": "0",
            },
            "status": {
                "observedGeneration": "0",
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
                "conditions": [
                    {
                        "type": "RequiresRecreation",
                        "status": "True",
                    }
                ],
            }
        }

        self.assertTrue(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_event_returns_true_if_recreate_condition_gets_true(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = {
            "status": {
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
                "conditions": [
                    {
                        "type": "RequiresRecreation",
                        "status": "False",
                    }
                ],
            }
        }
        ev.object_ = {
            "metadata": {
                "generation": "0",
            },
            "status": {
                "observedGeneration": "0",
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
                "conditions": [
                    {
                        "type": "RequiresRecreation",
                        "status": "True",
                    }
                ],
            }
        }

        self.assertTrue(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_event_returns_false_if_recreate_condition_same(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = {
            "status": {
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
                "conditions": [
                    {
                        "type": "RequiresRecreation",
                        "status": "True",
                        "reason": sentinel.reason1,
                    }
                ],
            }
        }
        ev.object_ = {
            "metadata": {
                "generation": "0",
            },
            "status": {
                "observedGeneration": "0",
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
                "conditions": [
                    {
                        "type": "RequiresRecreation",
                        "status": "True",
                        "reason": sentinel.reason1,
                    }
                ],
            }
        }

        self.assertFalse(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_event_ignores_other_recreate_condition_changes(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = {
            "status": {
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
                "conditions": [
                    {
                        "type": "RequiresRecreation",
                        "status": "True",
                        "reason": sentinel.reason1,
                    }
                ],
            }
        }
        ev.object_ = {
            "metadata": {
                "generation": "0",
            },
            "status": {
                "observedGeneration": "0",
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
                "conditions": [
                    {
                        "type": "RequiresRecreation",
                        "status": "True",
                        "reason": sentinel.reason2,
                    }
                ],
            }
        }

        self.assertFalse(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__handle_event_tolerates_missing_fields(self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = {
            "status": {},
        }
        ev.object_ = {
            "metadata": {
                "generation": "0",
            },
            "status": {
                "observedGeneration": "0",
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
            }
        }

        self.assertTrue(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = {
            "status": {
                "state": unittest.mock.sentinel.state1,
            },
        }
        ev.object_ = {
            "metadata": {
                "generation": "0",
            },
            "status": {
                "observedGeneration": "0",
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
            }
        }

        self.assertTrue(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.MODIFIED
        ev.old_object = {
            "status": {
                "phase": context.Phase.UPDATED.value,
            },
        }
        ev.object_ = {
            "metadata": {
                "generation": "0",
            },
            "status": {
                "observedGeneration": "0",
                "state": unittest.mock.sentinel.state1,
                "phase": context.Phase.UPDATED.value,
            }
        }

        self.assertTrue(self.csr._handle_event(
            unittest.mock.sentinel.ctx,
            ev,
        ))

    def test__get_run_state_returns_shutting_down_if_deleting(self):
        self.assertEqual(
            self.csr._get_run_state(
                unittest.mock.sentinel.ctx,
                {
                    "metadata": {
                        "deletionTimestamp": unittest.mock.sentinel.panic,
                    },
                },
            ),
            instancing.ResourceRunState.SHUTTING_DOWN,
        )

    def test__get_run_state_returns_starting_if_phase_is_updating(self):
        self.assertEqual(
            self.csr._get_run_state(
                unittest.mock.sentinel.ctx,
                {
                    "metadata": {
                        "deletionTimestamp": None,
                        "generation": "1",
                    },
                    "status": {
                        "phase": "Updating",
                        "updatedGeneration": "0",
                    },
                },
            ),
            instancing.ResourceRunState.STARTING,
        )

    def test__get_run_state_returns_ready_if_phase_is_updating_and_generation_uptodate(self):  # noqa: E501
        self.assertEqual(
            self.csr._get_run_state(
                unittest.mock.sentinel.ctx,
                {
                    "metadata": {
                        "deletionTimestamp": None,
                        "generation": "1",
                    },
                    "status": {
                        "phase": "Updating",
                        "status": "Enabled",
                        "updatedGeneration": "1",
                    },
                },
            ),
            instancing.ResourceRunState.READY,
        )

    def test__get_run_state_returns_unknown_if_phase_is_backing_off(self):
        self.assertEqual(
            self.csr._get_run_state(
                unittest.mock.sentinel.ctx,
                {
                    "metadata": {
                        "deletionTimestamp": None,
                    },
                    "status": {
                        "phase": "BackingOff",
                    },
                },
            ),
            instancing.ResourceRunState.UNKNOWN,
        )

    def test__get_run_state_returns_starting_if_waiting_for_dependency(self):
        self.assertEqual(
            self.csr._get_run_state(
                unittest.mock.sentinel.ctx,
                {
                    "metadata": {
                        "deletionTimestamp": None,
                    },
                    "status": {
                        "phase": "WaitingForDependency",
                    },
                },
            ),
            instancing.ResourceRunState.STARTING,
        )

    def test__get_run_state_returns_starting_if_created(self):
        self.assertEqual(
            self.csr._get_run_state(
                unittest.mock.sentinel.ctx,
                {
                    "metadata": {
                        "deletionTimestamp": None,
                    },
                    "status": {
                        "phase": "Created",
                    },
                },
            ),
            instancing.ResourceRunState.STARTING,
        )

    def test__get_run_state_returns_starting_if_phase_missing(self):
        self.assertEqual(
            self.csr._get_run_state(
                unittest.mock.sentinel.ctx,
                {
                    "metadata": {
                        "deletionTimestamp": None,
                    },
                },
            ),
            instancing.ResourceRunState.STARTING,
        )

    def test__get_run_state_returns_starting_if_updated_but_not_in_nova(self):
        self.assertEqual(
            self.csr._get_run_state(
                unittest.mock.sentinel.ctx,
                {
                    "metadata": {
                        "deletionTimestamp": None,
                        "generation": "1",
                    },
                    "status": {
                        "phase": "Updated",
                        "state": "Creating",
                        "updatedGeneration": "1",
                    },
                },
            ),
            instancing.ResourceRunState.STARTING,
        )

    def test__get_run_state_returns_starting_if_updated_but_not_generation(self):  # noqa: E501
        self.assertEqual(
            self.csr._get_run_state(
                unittest.mock.sentinel.ctx,
                {
                    "metadata": {
                        "deletionTimestamp": None,
                        "generation": "1",
                    },
                    "status": {
                        "phase": "Updated",
                        "state": "Enabled",
                        "updatedGeneration": "0",
                    },
                },
            ),
            instancing.ResourceRunState.STARTING,
        )

    @ddt.data("Enabled", "Disabled", "Eviciting", "DisabledAndCleared")
    def test__get_run_state_returns_ready_if_updated_and_right_state(self, state):  # noqa: E501
        self.assertEqual(
            self.csr._get_run_state(
                unittest.mock.sentinel.ctx,
                {
                    "metadata": {
                        "deletionTimestamp": None,
                        "generation": "1",
                    },
                    "status": {
                        "phase": "Updated",
                        "state": state,
                        "updatedGeneration": "1",
                    },
                },
            ),
            instancing.ResourceRunState.READY,
        )

    def test__get_spec_state_returns_up_to_date_if_update_not_needed(self):
        with contextlib.ExitStack() as stack:
            needs_update = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "_needs_update",
            ))
            needs_update.return_value = False

            ctx = unittest.mock.Mock()
            ctx.instance = "node1"

            instance = {
                "status": {
                    "conditions": [
                    ],
                },
            }

            self.assertEqual(
                self.csr._get_spec_state(
                    ctx,
                    unittest.mock.sentinel.new,
                    instance,
                ),
                instancing.ResourceSpecState.UP_TO_DATE,
            )

        needs_update.assert_called_once_with(
            instance,
            unittest.mock.sentinel.new,
        )

    def test__get_spec_state_returns_stale_if_update_needed(self):
        with contextlib.ExitStack() as stack:
            needs_update = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "_needs_update",
            ))
            needs_update.return_value = True

            self.assertEqual(
                self.csr._get_spec_state(
                    unittest.mock.sentinel.ctx,
                    unittest.mock.sentinel.new,
                    unittest.mock.sentinel.old,
                ),
                instancing.ResourceSpecState.STALE,
            )

        needs_update.assert_called_once_with(
            unittest.mock.sentinel.old,
            unittest.mock.sentinel.new,
        )

    def test__get_spec_state_returns_stale_if_recreation_needed(self):
        with contextlib.ExitStack() as stack:
            needs_update = stack.enter_context(unittest.mock.patch.object(
                self.wrapped, "_needs_update",
            ))
            needs_update.return_value = False

            instance = {
                "status": {
                    "conditions": [
                        {
                            "type": "RequiresRecreation",
                            "status": "True",
                        }
                    ],
                },
            }

            self.assertEqual(
                self.csr._get_spec_state(
                    unittest.mock.sentinel.ctx,
                    unittest.mock.sentinel.new,
                    instance,
                ),
                instancing.ResourceSpecState.STALE,
            )

        needs_update.assert_called_once_with(
            instance,
            unittest.mock.sentinel.new,
        )

    def test__get_recreation_condition_returns_none_if_no_condition(self):
        instance = {
            "status": {
                "conditions": [
                    {
                        "type": "Somestrangetype",
                    }
                ],
            },
        }

        self.assertEqual(
            self.csr._get_recreation_condition(
                instance,
            ),
            None,
        )

    def test__get_recreation_condition_returns_true_if_true(self):
        instance = {
            "status": {
                "conditions": [
                    {
                        "type": "Somestrangetype",
                    },
                    {
                        "type": "RequiresRecreation",
                        "status": "True",
                    }
                ],
            },
        }

        self.assertTrue(
            self.csr._get_recreation_condition(
                instance,
            )
        )

    def test__get_recreation_condition_returns_true_if_some_other_status(self):
        instance = {
            "status": {
                "conditions": [
                    {
                        "type": "Somestrangetype",
                    },
                    {
                        "type": "RequiresRecreation",
                        "status": "mystatus",
                    }
                ],
            },
        }

        self.assertFalse(
            self.csr._get_recreation_condition(
                instance,
            )
        )

    def test__does_node_require_maintenance_returns_false(self):
        class MockCtxVars:
            def get(self):
                return set(['node1', 'node3'])

        ctx_mock = MockCtxVars()

        ctx = unittest.mock.Mock()
        ctx.instance = "node2"

        with unittest.mock.patch(
            "yaook.statemachine.resources.openstack.nodes_in_maintenance",
            new=ctx_mock
        ):
            self.assertEqual(
                False,
                self.csr._does_node_require_maintenance(ctx)
            )

    def test__does_node_require_maintenance_returns_true(self):
        class MockCtxVars:
            def get(self):
                return set(['node1', 'node2', 'node3'])

        ctx_mock = MockCtxVars()

        ctx = unittest.mock.Mock()
        ctx.instance = "node2"

        with unittest.mock.patch(
            "yaook.statemachine.resources.openstack.nodes_in_maintenance",
            new=ctx_mock
        ):
            self.assertEqual(
                True,
                self.csr._does_node_require_maintenance(ctx)
            )


    async def test__compile_full_state_prevents_creation_of_instances_with_maintenance(self):  # NOQA
        class MockCtxVars:
            def get(self):
                return set(['node2'])

        nodes_in_maintenance_mock = MockCtxVars()

        ctx = context.Context(None, None, None, None, None, None, None, None)

        def generate_body(ctx, deps):
            return {"for": ctx.instance}

        def generate_winner(ctx, *_):
            return (
                {"winner_for": ctx.instance},
                [],
            )

        with contextlib.ExitStack() as stack:
            select_winner = stack.enter_context(unittest.mock.patch.object(
                self.csr, "_select_winner",
            ))
            select_winner.side_effect = generate_winner

            make_body = stack.enter_context(unittest.mock.patch.object(
                self.csr, "_make_complete_body",
            ))
            make_body.side_effect = generate_body

            stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.openstack.nodes_in_maintenance",
                new=nodes_in_maintenance_mock
            ))

            state, to_delete = await self.csr._compile_full_state(
                ctx,
                {},
                {"node1": None, "node2": None},
                {
                    "node1": [
                        unittest.mock.sentinel.info1,
                        unittest.mock.sentinel.info2,
                        unittest.mock.sentinel.info3,
                    ],
                    "node2": [
                        unittest.mock.sentinel.info4,
                        unittest.mock.sentinel.info5,
                    ],
                },
            )

        self.assertDictEqual(
            state,
            {
                "node1": instancing.InstanceState(
                    winner={"winner_for": "node1"},
                    intended_body={"for": "node1"},
                    instance_data=None,
                    has_any_resources=True,
                    may_create_resources=True,
                ),
                "node2": instancing.InstanceState(
                    winner={"winner_for": "node2"},
                    intended_body={"for": "node2"},
                    instance_data=None,
                    has_any_resources=True,
                    may_create_resources=False,
                ),
            },
        )

    async def test__get_nodes_in_maintenance_is_empty(self):
        self.assertListEqual([],
                             await self.csr._get_nodes_in_maintenance(None))


class TestApiStateResource(unittest.IsolatedAsyncioTestCase):
    class MockApiStateResource(resource.APIStateResource):
        def _get_resource(self, client, ctx):
            pass

        def _update_status(self, ctx, connection_info, enabled):
            pass

        def _needs_l2_agent(self):
            return True

    def setUp(self):
        self.csr = self.MockApiStateResource(
            scheduling_keys=["foo", "bar"],
            endpoint_config=sentinel.endpoint_config,
            credentials_secret=sentinel.credentials_secret,
            job_endpoint_config=sentinel.job_endpoint_config,
            finalizer="test.yaook.cloud",
            eviction_job_template="some-job-template.yaml",
        )

        self.node_name = str(uuid.uuid4())

        self.core_v1 = unittest.mock.Mock([])
        self.core_v1.read_node = unittest.mock.AsyncMock()
        self.core_v1.read_node.return_value = kclient.V1Node(
            metadata=kclient.V1ObjectMeta(
                name=self.node_name,
            ),
            spec=kclient.V1NodeSpec(
                taints=[
                    kclient.V1Taint(
                        key=unittest.mock.sentinel.foo,
                        effect="NoSchedule",
                    ),
                    kclient.V1Taint(
                        key=unittest.mock.sentinel.bar,
                        effect="NoExecute",
                    )
                ]
            ),
            status=kclient.V1NodeStatus(),
        )
        self.CoreV1Api = unittest.mock.Mock([])
        self.CoreV1Api.return_value = self.core_v1

        self.background_job = unittest.mock.Mock([])
        self.background_job.is_ready = unittest.mock.AsyncMock()
        self.background_job.is_ready.return_value = True

        self.get_node_name = unittest.mock.Mock([])
        self.get_node_name.return_value = self.node_name

        self.__state_patches = [
            unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
                new=self.CoreV1Api,
            ),
            unittest.mock.patch.object(
                self.csr, "_background_job",
                new=self.background_job,
            ),
            unittest.mock.patch.object(
                self.csr, "_get_node_name",
                new=self.get_node_name,
            ),
        ]
        self._stack_ref = contextlib.ExitStack()
        self.stack = self._stack_ref.__enter__()

    def tearDown(self):
        self._stack_ref.__exit__(None, None, None)

    def _setup_state_patches(self):
        for p in self.__state_patches:
            self.stack.enter_context(p)

    def _make_state_context(self):
        ctx = unittest.mock.Mock(["api_client"])
        ctx.parent = {
            "metadata": {
                "name": self.node_name,
                "creationTimestamp": "2021-12-14T11:00:00Z",
            },
            "spec": {
                "state": "Enabled",
            },
            "status": {},
        }
        ctx.parent_name = self.node_name
        ctx.logger = unittest.mock.Mock()
        return ctx

    async def test__next_state_stays_in_enabled_state_if_all_is_well(self):  # NOQA
        self._setup_state_patches()
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.ENABLED.value
        status = resource.ResourceStatus(True, True, None)

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            status,
        )
        self.assertEqual(
            next_state,
            resource.ResourceStatusState.ENABLED,
        )
        self.assertIsNone(eviction_intent)

    async def test__next_state_stays_in_creating_if_status_is_none(self):  # NOQA
        self._setup_state_patches()
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.CREATING.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            None,
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.CREATING,
        )
        self.assertIsNone(eviction_intent)

    async def test__next_state_stays_in_creating_if_status_is_not_up(self):  # NOQA
        self._setup_state_patches()
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.CREATING.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.CREATING,
        )
        self.assertIsNone(eviction_intent)

    async def test__next_state_moves_from_creating_to_enabled_when_up_and_spec_is_enabled(self):  # NOQA
        self._setup_state_patches()
        ctx = self._make_state_context()
        ctx.parent["spec"]["state"] = \
            resource.ResourceSpecState.ENABLED.value
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.CREATING.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.ENABLED,
        )
        self.assertIsNone(eviction_intent)

    async def test__next_state_moves_from_creating_to_disabled_when_up_and_spec_is_disabled(self):  # NOQA
        self._setup_state_patches()
        ctx = self._make_state_context()
        ctx.parent["spec"]["state"] = \
            resource.ResourceSpecState.DISABLED.value
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.CREATING.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.DISABLED,
        )
        self.assertIsNone(eviction_intent)

    async def test__next_state_moves_from_enabled_to_disabled_if_spec_is_disabled(self):  # NOQA
        self._setup_state_patches()
        ctx = self._make_state_context()
        ctx.parent["spec"]["state"] = \
            resource.ResourceSpecState.DISABLED.value
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.ENABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.DISABLED,
        )
        self.assertIsNone(eviction_intent)

    async def test__next_state_moves_from_enabled_to_evicting_if_spec_is_disabled_and_cleared(self):  # NOQA
        self._setup_state_patches()
        ctx = self._make_state_context()
        ctx.parent["spec"]["state"] = \
            resource.ResourceSpecState.DISABLED_AND_CLEARED.value
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.ENABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.
                DESIRED_STATE,
            ),
        )

    async def test__next_state_moves_from_disabled_to_evicting_if_spec_is_disabled_and_cleared(self):  # NOQA
        self._setup_state_patches()
        ctx = self._make_state_context()
        ctx.parent["spec"]["state"] = \
            resource.ResourceSpecState.DISABLED_AND_CLEARED.value
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.DISABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.
                DESIRED_STATE,
            ),
        )

    async def test__next_state_stays_in_disabled_and_cleared_if_spec_matches(self):  # NOQA
        self._setup_state_patches()
        ctx = self._make_state_context()
        ctx.parent["spec"]["state"] = \
            resource.ResourceSpecState.DISABLED_AND_CLEARED.value
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.DISABLED_AND_CLEARED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.DISABLED_AND_CLEARED,
        )
        self.assertIsNone(
            eviction_intent,
        )

    async def test__next_state_moves_from_enabled_to_evicting_if_deleting(self):  # NOQA
        self._setup_state_patches()
        ctx = self._make_state_context()
        ctx.parent["metadata"]["deletionTimestamp"] = \
            unittest.mock.sentinel.panic
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.ENABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.DELETING,
            )
        )

    async def test__next_state_moves_from_disabled_to_evicting_if_deleting(self):  # NOQA
        self._setup_state_patches()
        ctx = self._make_state_context()
        ctx.parent["metadata"]["deletionTimestamp"] = \
            unittest.mock.sentinel.panic
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.DISABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.DELETING,
            )
        )

    async def test__next_state_stays_in_disabled_and_cleared_if_deleting(self):  # NOQA
        self._setup_state_patches()
        ctx = self._make_state_context()
        ctx.parent["metadata"]["deletionTimestamp"] = \
            unittest.mock.sentinel.panic
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.DISABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.DELETING,
            )
        )

    async def test__next_state_stays_in_creating_if_deleting_and_status_not_ready(self):  # NOQA
        self._setup_state_patches()
        ctx = self._make_state_context()
        ctx.parent["metadata"]["deletionTimestamp"] = \
            unittest.mock.sentinel.panic
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.CREATING.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.CREATING,
        )
        self.assertIsNone(
            eviction_intent,
        )

    async def test__next_state_moves_from_enabled_to_evicting_if_node_deleting(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.metadata.deletion_timestamp = \
            unittest.mock.sentinel.panic
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.ENABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.DELETING,
            )
        )

    async def test__next_state_moves_from_disabled_to_evicting_if_node_deleting(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.metadata.deletion_timestamp = \
            unittest.mock.sentinel.panic
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.DISABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.DELETING,
            )
        )

    async def test__next_state_stays_in_disabled_and_cleared_if_node_deleting(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.metadata.deletion_timestamp = \
            unittest.mock.sentinel.panic
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.DISABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.DELETING,
            )
        )

    async def test__next_state_moves_from_creating_to_evicting_if_node_deleting_and_status_not_ready(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.metadata.deletion_timestamp = \
            unittest.mock.sentinel.panic
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.CREATING.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.DELETING,
            )
        )

    async def test__next_state_moves_from_enabled_to_evicting_if_node_deleted(self):  # NOQA
        self._setup_state_patches()
        exc = kclient.exceptions.ApiException(status=404)
        self.core_v1.read_node.side_effect = exc
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.ENABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.NODE_DOWN,
            )
        )

    async def test__next_state_moves_from_disabled_to_evicting_if_node_deleted(self):  # NOQA
        self._setup_state_patches()
        exc = kclient.exceptions.ApiException(status=404)
        self.core_v1.read_node.side_effect = exc
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.DISABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.NODE_DOWN,
            )
        )

    async def test__next_state_stays_in_disabled_and_cleared_if_node_deleted(self):  # NOQA
        self._setup_state_patches()
        exc = kclient.exceptions.ApiException(status=404)
        self.core_v1.read_node.side_effect = exc
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.DISABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.NODE_DOWN,
            )
        )

    async def test__next_state_moves_from_creating_to_evicting_if_node_deleted_and_status_not_ready(self):  # NOQA
        self._setup_state_patches()
        exc = kclient.exceptions.ApiException(status=404)
        self.core_v1.read_node.side_effect = exc
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.CREATING.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.NODE_DOWN,
            )
        )

    async def test__next_state_moves_from_enabled_to_evicting_if_node_unreachable(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.spec.taints.append(
            kclient.V1Taint(
                key="node.kubernetes.io/unreachable",
                effect=unittest.mock.sentinel.foo,
            ),
        )
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.ENABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.NODE_DOWN,
            )
        )

    async def test__next_state_moves_from_disabled_to_evicting_if_node_unreachable(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.spec.taints.append(
            kclient.V1Taint(
                key="node.kubernetes.io/unreachable",
                effect=unittest.mock.sentinel.foo,
            ),
        )
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.DISABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.NODE_DOWN,
            )
        )

    async def test__next_state_stays_in_disabled_and_cleared_if_node_unreachable(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.spec.taints.append(
            kclient.V1Taint(
                key="node.kubernetes.io/unreachable",
                effect=unittest.mock.sentinel.foo,
            ),
        )
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.DISABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.NODE_DOWN,
            )
        )

    async def test__next_state_moves_from_creating_to_evicting_if_node_unreachable_and_status_not_ready(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.spec.taints.append(
            kclient.V1Taint(
                key="node.kubernetes.io/unreachable",
                effect=unittest.mock.sentinel.foo,
            ),
        )
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.CREATING.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.NODE_DOWN,
            )
        )

    async def test__next_state_moves_from_enabled_to_evicting_if_network_unavailable(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.spec.taints.append(
            kclient.V1Taint(
                key="node.kubernetes.io/network-unavailable",
                effect=unittest.mock.sentinel.foo,
            ),
        )
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.ENABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.NODE_DOWN,
            )
        )

    async def test__next_state_moves_from_disabled_to_evicting_if_network_unavailable(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.spec.taints.append(
            kclient.V1Taint(
                key="node.kubernetes.io/network-unavailable",
                effect=unittest.mock.sentinel.foo,
            ),
        )
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.DISABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.NODE_DOWN,
            )
        )

    async def test__next_state_stays_in_disabled_and_cleared_if_network_unavailable(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.spec.taints.append(
            kclient.V1Taint(
                key="node.kubernetes.io/network-unavailable",
                effect=unittest.mock.sentinel.foo,
            ),
        )
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.DISABLED.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=True, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.NODE_DOWN,
            )
        )

    async def test__next_state_moves_from_creating_to_evicting_if_network_unavailable_and_status_not_ready(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.spec.taints.append(
            kclient.V1Taint(
                key="node.kubernetes.io/network-unavailable",
                effect=unittest.mock.sentinel.foo,
            ),
        )
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.CREATING.value

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.CoreV1Api.assert_called_once_with(ctx.api_client)
        self.core_v1.read_node.assert_awaited_once_with(
            ctx.parent["metadata"]["name"],
        )

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.NODE_DOWN,
            )
        )

    async def test__next_state_stays_in_evicting_if_job_is_not_ready(self):  # NOQA
        self._setup_state_patches()
        self.background_job.is_ready.return_value = False
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.EVICTING.value
        ctx.parent["status"]["eviction"] = {
            "mode": "Respawn",
            "reason": "NodeDown",
        }

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.background_job.is_ready.assert_awaited_once_with(ctx)

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.EVICTING,
        )
        self.assertEqual(
            eviction_intent,
            resource.EvictionIntent(
                reason=resource.ResourceEvictingReason.NODE_DOWN,
            )
        )

    async def test__next_state_moves_from_evicting_to_disabled_and_cleared_if_spec_matches_and_job_done(self):  # NOQA
        self._setup_state_patches()
        self.background_job.is_ready.return_value = True
        ctx = self._make_state_context()
        ctx.parent["spec"]["state"] = \
            resource.ResourceSpecState.DISABLED_AND_CLEARED.value
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.EVICTING.value
        ctx.parent["status"]["eviction"] = {
            "mode": "Respawn",
            "reason": "NodeDown",
        }

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.background_job.is_ready.assert_awaited_once_with(ctx)

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.DISABLED_AND_CLEARED,
        )
        self.assertIsNone(
            eviction_intent,
        )

    async def test__next_state_moves_from_evicting_to_enabled_if_spec_matches_and_job_done(self):  # NOQA
        self._setup_state_patches()
        self.background_job.is_ready.return_value = True
        ctx = self._make_state_context()
        ctx.parent["spec"]["state"] = \
            resource.ResourceSpecState.ENABLED.value
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.EVICTING.value
        ctx.parent["status"]["eviction"] = {
            "mode": "Respawn",
            "reason": "NodeDown",
        }

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.background_job.is_ready.assert_awaited_once_with(ctx)

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.ENABLED,
        )
        self.assertIsNone(
            eviction_intent,
        )

    async def test__next_state_moves_from_evicting_to_enabled_if_spec_matches_and_job_done(self):  # NOQA
        self._setup_state_patches()
        self.background_job.is_ready.return_value = True
        ctx = self._make_state_context()
        ctx.parent["spec"]["state"] = \
            resource.ResourceSpecState.DISABLED.value
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.EVICTING.value
        ctx.parent["status"]["eviction"] = {
            "mode": "Respawn",
            "reason": "NodeDown",
        }

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.background_job.is_ready.assert_awaited_once_with(ctx)

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.DISABLED,
        )
        self.assertIsNone(
            eviction_intent,
        )

    async def test__next_state_moves_from_evicting_to_disabled_and_cleared_if_node_unreachable_and_job_done(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.spec.taints.append(
            kclient.V1Taint(
                key="node.kubernetes.io/unreachable",
                effect=unittest.mock.sentinel.foo,
            ),
        )
        self.background_job.is_ready.return_value = True
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.EVICTING.value
        ctx.parent["status"]["eviction"] = {
            "mode": "Respawn",
            "reason": "NodeDown",
        }

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.background_job.is_ready.assert_awaited_once_with(ctx)

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.DISABLED_AND_CLEARED,
        )
        self.assertIsNone(
            eviction_intent,
        )

    async def test__next_state_moves_from_evicting_to_disabled_and_cleared_if_node_deleting_and_job_done(self):  # NOQA
        self._setup_state_patches()
        self.core_v1.read_node.return_value.metadata.deletion_timestamp = \
            unittest.mock.sentinel.panic
        self.background_job.is_ready.return_value = True
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.EVICTING.value
        ctx.parent["status"]["eviction"] = {
            "mode": "Respawn",
            "reason": "NodeDown",
        }

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.background_job.is_ready.assert_awaited_once_with(ctx)

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.DISABLED_AND_CLEARED,
        )
        self.assertIsNone(
            eviction_intent,
        )

    async def test__next_state_moves_from_evicting_to_disabled_and_cleared_if_deleting_and_job_done(self):  # NOQA
        self._setup_state_patches()
        self.background_job.is_ready.return_value = True
        ctx = self._make_state_context()
        ctx.parent["metadata"]["deletionTimestamp"] = \
            unittest.mock.sentinel.panic
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.EVICTING.value
        ctx.parent["status"]["eviction"] = {
            "mode": "Respawn",
            "reason": "NodeDown",
        }

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.background_job.is_ready.assert_awaited_once_with(ctx)

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.DISABLED_AND_CLEARED,
        )
        self.assertIsNone(
            eviction_intent,
        )

    async def test__next_state_moves_from_evicting_to_disabled_and_cleared_if_node_deleted_and_job_done(self):  # NOQA
        self._setup_state_patches()
        exc = kclient.exceptions.ApiException(status=404)
        self.core_v1.read_node.side_effect = exc
        self.background_job.is_ready.return_value = True
        ctx = self._make_state_context()
        ctx.parent["status"]["state"] = \
            resource.ResourceStatusState.EVICTING.value
        ctx.parent["status"]["eviction"] = {
            "mode": "Respawn",
            "reason": "NodeDown",
        }

        next_state, eviction_intent = await self.csr._next_state(
            ctx,
            resource.ResourceStatus(up=False, enabled=True,
                                    disable_reason=None),
        )

        self.background_job.is_ready.assert_awaited_once_with(ctx)

        self.assertEqual(
            next_state,
            resource.ResourceStatusState.DISABLED_AND_CLEARED,
        )
        self.assertIsNone(
            eviction_intent,
        )

    async def test_delete_removes_annotation(self):
        ctx = self._make_state_context()
        ctx.parent_kind = "test"
        ctx.parent_name = "node1"

        with contextlib.ExitStack() as stack:
            update = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.openstack.APIStateResource.update",  # noqa: E501
                new=unittest.mock.AsyncMock([])
            ))
            update.return_value = None
            is_ready = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.openstack.BackgroundJob.is_ready",  # noqa: E501
                new=unittest.mock.AsyncMock([])
            ))
            is_ready.return_value = True
            patch_node = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api.patch_node",
                new=unittest.mock.AsyncMock([])
            ))
            await self.csr.delete(ctx, dependencies={})

        patch = {
            "op": "remove",
            # In the openstackresource we are using a jsonpatch and since we
            # don't want this here, the slash gets converted to a '~1
            "path": "/metadata/annotations/l2-lock.maintenance.yaook.cloud~1test",  # noqa: E501
            "value": "",
        }
        patch_node.assert_called_once_with(ctx.parent_name, [patch])


class TestNeutronWatcher(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        super().setUp()
        self.connection_parameters = {
            "key": sentinel.connection_parameters
        }
        self.watcher = resource.NeutronAgentWatcher(
            "NeutronXYAgent", "neutron-xy-agent",
            connection_parameters=self.connection_parameters,
            namespace="testnamespace")

    def test_to_kubernetes_reference(self):
        self.assertEqual(
            self.watcher.to_kubernetes_reference("testname"),
            kclient.V1ObjectReference(
                api_version="network.yaook.cloud/v1",
                kind="NeutronXYAgent",
                name="testname",
                namespace="testnamespace"
            )
        )

    @unittest.mock.patch("openstack.connect")
    def test__get_state(self, connect):
        agent1 = unittest.mock.Mock(["host"])
        agent1.host = "host1"
        agent2 = unittest.mock.Mock(["host"])
        agent2.host = "host2"
        client = unittest.mock.Mock(["network"])
        client.network = unittest.mock.Mock(["agents"])
        client.network.agents.return_value = [
            agent1,
            agent2,
        ]
        connect.return_value = client
        ret = self.watcher._get_state()

        self.assertEqual(
            {"host1": agent1, "host2": agent2},
            ret,
        )
        connect.assert_called_once_with(
            **self.connection_parameters,
        )
        client.network.agents.assert_called_once_with(
            binary="neutron-xy-agent"
        )

    @unittest.mock.patch("openstack.connect")
    def test__get_state_does_nothing_if_not_init(self, connect):
        self.watcher.connection_parameters = None

        ret = self.watcher._get_state()

        self.assertEqual(
            {},
            ret,
        )
        connect.assert_not_called()


class TestL2ProvidingAgentStateResource(unittest.IsolatedAsyncioTestCase):
    class DummyL2ProvidingAgentStateResource(
            resource.L2ProvidingAgentStateResource):
        async def update(self,
                         ctx: context.Context,
                         dependencies: DependencyMap,
                         ) -> None:
            """
            define update() here, so we can test _handle_event() at
            statemachine.
            """
            raise NotImplementedError

        def _get_agent(
                self,
                network_client: openstack.network.v2._proxy.Proxy,
                host: str,
                ) -> typing.Optional[openstack.network.v2.agent.Agent]:
            """
            each L2ProvidingAgentStateResource needs to implement this method,
            so we get the correct agent object from network API.
            """
            raise NotImplementedError

    def setUp(self):
        self.csr = self.DummyL2ProvidingAgentStateResource(
            endpoint_config=sentinel.endpoint_config,
            credentials_secret=sentinel.credentials_secret,
            finalizer="test.yaook.cloud",
        )

        self.node_name = str(uuid.uuid4())

        self.event = unittest.mock.Mock([])
        self.event.object_ = unittest.mock.Mock([])
        self.event.object_.metadata = unittest.mock.Mock([])
        self.event.object_.metadata.annotations = {
            'foo': 'bar',
            context.ANNOTATION_L2_MIGRATION_LOCK + 'foobar': '',
            'yaook.test': '',
        }
        self.event.object_.metadata.name = self.node_name
        self.event.old_object = unittest.mock.Mock()
        self.event.old_object.metadata.annotations = {
            'foo': 'bar',
            context.ANNOTATION_L2_MIGRATION_LOCK + 'foobar': '',
            'yaook.test': '',
        }
        self.event.type_ = watcher.EventType.MODIFIED

        self._stack_ref = contextlib.ExitStack()
        self.stack = self._stack_ref.__enter__()

    def tearDown(self):
        self._stack_ref.__exit__(None, None, None)

    def _setup_os_patches(self):
        for p in self.__os_patches:
            self.stack.enter_context(p)

    def _make_state_context(self):
        ctx = unittest.mock.Mock(["api_client"])
        ctx.parent = {
            "metadata": {
                "name": self.node_name,
                "creationTimestamp": "2021-12-14T11:00:00Z",
            },
            "spec": {
                "state": "Enabled",
            },
            "status": {},
        }
        ctx.parent_name = self.node_name
        ctx.logger = unittest.mock.Mock()
        ctx.namespace = "testnamespace"
        ctx.creation_timestamp = datetime.strptime(
            ctx.parent["metadata"]["creationTimestamp"],
            "%Y-%m-%dT%H:%M:%SZ")
        return ctx

    def test__handle_node_event_returns_true_for_added(self):
        self.event.type_ = watcher.EventType.ADDED

        self.assertTrue(self.csr._handle_event(
            self._make_state_context(),
            self.event,
        ))

    def test__handle_node_event_returns_true_for_deleted(self):
        self.event.type_ = watcher.EventType.DELETED

        self.assertTrue(self.csr._handle_event(
            self._make_state_context(),
            self.event,
        ))

    def test__handle_node_event_returns_true_for_removed_annotation(self):
        self.event.object_.metadata.annotations = {
            'foo': 'bar',
            'yaook.test': '',
        }

        self.assertTrue(self.csr._handle_event(
            self._make_state_context(),
            self.event,
        ))

    def test__handle_node_event_returns_false_if_l2_annotation_equal(self):
        self.assertFalse(self.csr._handle_event(
            self._make_state_context(),
            self.event,
        ))

    def test__handle_node_event_returns_false_for_added_l2_annotation(self):
        self.event.old_object.metadata.annotations = {
            'foo': 'bar',
            'yaook.test': '',
        }

        self.assertFalse(self.csr._handle_event(
            self._make_state_context(),
            self.event,
        ))

    def test__handle_node_event_returns_false_if_old_object_is_none(self):
        self.event.old_object = None

        self.assertFalse(self.csr._handle_event(
            self._make_state_context(),
            self.event,
        ))

    def test__handle_node_event_returns_false_if_other_annotation_changes(self):  # noqa: E501
        self.event.object_.metadata.annotations = {
            'foo': 'bar',
            context.ANNOTATION_L2_MIGRATION_LOCK + 'foobar': '',
            'yaook.test': '',
        }
        self.event.old_object.metadata.annotations = {
            context.ANNOTATION_L2_MIGRATION_LOCK + 'foobar': '',
            'yaook.test': '',
        }

        self.assertFalse(self.csr._handle_event(
            self._make_state_context(),
            self.event,
        ))

    def test__handle_node_event_tolerates_missing_fields(self):
        self.event.old_object.metadata.annotations = None

        self.assertFalse(self.csr._handle_event(
            self._make_state_context(),
            self.event,
        ))

        self.event.old_object.metadata = None

        self.assertFalse(self.csr._handle_event(
            self._make_state_context(),
            self.event,
        ))

        self.event.object_.metadata.annotations = None

        self.assertFalse(self.csr._handle_event(
            self._make_state_context(),
            self.event,
        ))

        self.event.object_.metadata = None

        self.assertFalse(self.csr._handle_event(
            self._make_state_context(),
            self.event,
        ))
