#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import contextlib
import dataclasses
import itertools
import unittest
import unittest.mock
from unittest.mock import sentinel

import aiohttp.client_exceptions

import kubernetes_asyncio.client

import yaook.statemachine.watcher as watcher


async def _collect(aiter):
    result = []
    async for item in aiter:
        result.append(item)
    return result


class TestWatchEvent(unittest.TestCase):
    def test_from_api_watch_event_parses_event_and_extracts_reference(self):
        raw_object = {
            "apiVersion": unittest.mock.sentinel.api_version,
            "kind": unittest.mock.sentinel.kind,
            "metadata": {
                "uid": unittest.mock.sentinel.uid,
                "namespace": unittest.mock.sentinel.namespace,
                "name": unittest.mock.sentinel.name,
            },
        }

        ev = {
            "type": "ADDED",
            "object": unittest.mock.sentinel.object_,
            "raw_object": raw_object,
        }

        result = watcher.WatchEvent.from_api_watch_event(ev)

        self.assertEqual(result.type_, watcher.EventType.ADDED)
        self.assertEqual(result.raw_object, raw_object)
        self.assertEqual(result.object_, unittest.mock.sentinel.object_)
        self.assertEqual(
            result.reference,
            kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            )
        )

    def test_from_api_watch_event_handles_absent_namespace(self):
        raw_object = {
            "apiVersion": unittest.mock.sentinel.api_version,
            "kind": unittest.mock.sentinel.kind,
            "metadata": {
                "uid": unittest.mock.sentinel.uid,
                "name": unittest.mock.sentinel.name,
            },
        }

        ev = {
            "type": "ADDED",
            "object": unittest.mock.sentinel.object_,
            "raw_object": raw_object,
        }

        result = watcher.WatchEvent.from_api_watch_event(ev)

        self.assertEqual(result.type_, watcher.EventType.ADDED)
        self.assertEqual(result.raw_object, raw_object)
        self.assertEqual(result.object_, unittest.mock.sentinel.object_)
        self.assertEqual(
            result.reference,
            kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=None,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            )
        )


class TestStatefulWatchEvent(unittest.TestCase):
    def test_from_watch_event_combines(self):
        ev = watcher.WatchEvent(
            type_=unittest.mock.sentinel.type_,
            reference=unittest.mock.sentinel.reference,
            raw_object=unittest.mock.sentinel.raw_object,
            object_=unittest.mock.sentinel.object_,
        )

        result = watcher.StatefulWatchEvent.from_watch_event(
            ev,
            unittest.mock.sentinel.old_raw_object,
            unittest.mock.sentinel.old_object,
        )

        self.assertEqual(result.type_, unittest.mock.sentinel.type_)
        self.assertEqual(result.raw_object, unittest.mock.sentinel.raw_object)
        self.assertEqual(result.object_, unittest.mock.sentinel.object_)
        self.assertEqual(result.reference, unittest.mock.sentinel.reference)
        self.assertEqual(result.old_raw_object,
                         unittest.mock.sentinel.old_raw_object)
        self.assertEqual(result.old_object, unittest.mock.sentinel.old_object)


@contextlib.contextmanager
def _patch_k8s_watch(api_client, watch_iter, stream):
    watch = unittest.mock.Mock(["stream"])
    watch.stream.return_value = watch_iter()

    with contextlib.ExitStack() as stack:
        Watch = stack.enter_context(unittest.mock.patch(
            "kubernetes_asyncio.watch.Watch",
        ))
        Watch.return_value = watch

        def assert_calls():
            Watch.assert_called_once_with()
            watch.stream.assert_called_once_with(stream)

        yield assert_calls


class Testwatch_events(unittest.IsolatedAsyncioTestCase):
    async def test_wraps_event_from_k8s_api(self):
        async def watch_iter():
            for i in range(3):
                yield {"ev": i}

        def result_generator(ev):
            return {"raw_event": ev}

        with contextlib.ExitStack() as stack:
            assert_calls = stack.enter_context(_patch_k8s_watch(
                unittest.mock.sentinel.api_client,
                watch_iter,
                unittest.mock.sentinel.stream,
            ))

            from_api_watch_event = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.WatchEvent.from_api_watch_event",
            ))
            from_api_watch_event.side_effect = result_generator

            stream_factory = unittest.mock.Mock([])
            stream_factory.return_value = unittest.mock.sentinel.stream

            stream = watcher.watch_events(
                unittest.mock.sentinel.api_client,
                stream_factory,
            )

            collected = await _collect(stream)

        assert_calls()
        stream_factory.assert_called_once_with(
            unittest.mock.sentinel.api_client,
        )

        self.assertSequenceEqual(
            collected,
            [
                {"raw_event": {"ev": 0}},
                {"raw_event": {"ev": 1}},
                {"raw_event": {"ev": 2}},
            ],
        )

    async def test_exits_cleanly_on_empty_response_line_exception(self):
        async def watch_iter():
            for i in range(2):
                yield {"ev": i}
            raise aiohttp.client_exceptions.ClientPayloadError

        def result_generator(ev):
            return {"raw_event": ev}

        with contextlib.ExitStack() as stack:
            assert_calls = stack.enter_context(_patch_k8s_watch(
                unittest.mock.sentinel.api_client,
                watch_iter,
                unittest.mock.sentinel.stream,
            ))

            from_api_watch_event = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.WatchEvent.from_api_watch_event",
            ))
            from_api_watch_event.side_effect = result_generator

            stream_factory = unittest.mock.Mock([])
            stream_factory.return_value = unittest.mock.sentinel.stream

            stream = watcher.watch_events(
                unittest.mock.sentinel.api_client,
                stream_factory,
            )

            collected = await _collect(stream)

        assert_calls()
        stream_factory.assert_called_once_with(
            unittest.mock.sentinel.api_client,
        )

        self.assertSequenceEqual(
            collected,
            [
                {"raw_event": {"ev": 0}},
                {"raw_event": {"ev": 1}},
            ],
        )

    async def test_raises_string_event_as_error(self):
        async def watch_iter():
            for i in range(2):
                yield {"ev": i}
            yield unittest.mock.sentinel.foo

        def result_generator(ev):
            return {"raw_event": ev}

        with contextlib.ExitStack() as stack:
            assert_calls = stack.enter_context(_patch_k8s_watch(
                unittest.mock.sentinel.api_client,
                watch_iter,
                unittest.mock.sentinel.stream,
            ))

            from_api_watch_event = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.WatchEvent.from_api_watch_event",
            ))
            from_api_watch_event.side_effect = result_generator

            stream_factory = unittest.mock.Mock([])
            stream_factory.return_value = unittest.mock.sentinel.stream

            stream = watcher.watch_events(
                unittest.mock.sentinel.api_client,
                stream_factory,
            )

            with self.assertRaisesRegex(
                    RuntimeError,
                    "replied with non-object: sentinel.foo"):
                await _collect(stream)

        assert_calls()
        stream_factory.assert_called_once_with(
            unittest.mock.sentinel.api_client,
        )
        self.assertSequenceEqual(
            [
                unittest.mock.call({"ev": 0}),
                unittest.mock.call({"ev": 1}),
            ],
            from_api_watch_event.mock_calls,
        )

    async def test_raises_error_type_as_error(self):
        async def watch_iter():
            for i in range(2):
                yield {"ev": i}
            yield {"type": "ERROR"}

        def result_generator(ev):
            return {"raw_event": ev}

        with contextlib.ExitStack() as stack:
            assert_calls = stack.enter_context(_patch_k8s_watch(
                unittest.mock.sentinel.api_client,
                watch_iter,
                unittest.mock.sentinel.stream,
            ))

            from_api_watch_event = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.WatchEvent.from_api_watch_event",
            ))
            from_api_watch_event.side_effect = result_generator

            stream_factory = unittest.mock.Mock([])
            stream_factory.return_value = unittest.mock.sentinel.stream

            stream = watcher.watch_events(
                unittest.mock.sentinel.api_client,
                stream_factory,
            )

            with self.assertRaisesRegex(
                    RuntimeError,
                    r"replied with error: \{'type': 'ERROR'\}"):
                await _collect(stream)

        assert_calls()
        stream_factory.assert_called_once_with(
            unittest.mock.sentinel.api_client,
        )
        self.assertSequenceEqual(
            [
                unittest.mock.call({"ev": 0}),
                unittest.mock.call({"ev": 1}),
            ],
            from_api_watch_event.mock_calls,
        )

    async def test_stops_on_api_exception_410(self):
        async def watch_iter():
            for i in range(2):
                yield {"ev": i}
            raise kubernetes_asyncio.client.exceptions.ApiException(status=410)

        def result_generator(ev):
            return {"raw_event": ev}

        with contextlib.ExitStack() as stack:
            stack.enter_context(_patch_k8s_watch(
                unittest.mock.sentinel.api_client,
                watch_iter,
                unittest.mock.sentinel.stream,
            ))

            from_api_watch_event = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.WatchEvent.from_api_watch_event",
            ))
            from_api_watch_event.side_effect = result_generator

            stream_factory = unittest.mock.Mock([])
            stream_factory.return_value = unittest.mock.sentinel.stream

            stream = watcher.watch_events(
                unittest.mock.sentinel.api_client,
                stream_factory,
            )

            output = await _collect(stream)
            self.assertSequenceEqual(
                [{"raw_event": {"ev": 0}},
                 {"raw_event": {"ev": 1}}],
                output
            )


class TestStatefulWatcher(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.stream_factory = unittest.mock.Mock([])
        self.stream_factory.return_value = unittest.mock.sentinel.stream
        self.sw = watcher.StatefulWatcher(
            self.stream_factory,
        )

    async def test_initial_added_event(self):
        ev = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object,
            object_=unittest.mock.sentinel.object_,
        )

        async def watch_event_generator():
            yield ev

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()
            item = await stream_iter.__anext__()

        watch_events.assert_called_once_with(
            unittest.mock.sentinel.api_client,
            self.stream_factory,
        )

        self.assertEqual(
            item,
            watcher.StatefulWatchEvent.from_watch_event(
                ev,
                {},
                None,
            )
        )

    async def test_added_followed_by_modified(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.MODIFIED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        ev3 = watcher.WatchEvent(
            type_=watcher.EventType.MODIFIED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v3,
            object_=unittest.mock.sentinel.object_v3,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2
            yield ev3

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()
            _ = await stream_iter.__anext__()
            item_v2 = await stream_iter.__anext__()
            item_v3 = await stream_iter.__anext__()

        watch_events.assert_called_once_with(
            unittest.mock.sentinel.api_client,
            self.stream_factory,
        )

        self.assertEqual(
            item_v2,
            watcher.StatefulWatchEvent.from_watch_event(
                ev2,
                unittest.mock.sentinel.raw_object_v1,
                unittest.mock.sentinel.object_v1,
            )
        )

        self.assertEqual(
            item_v3,
            watcher.StatefulWatchEvent.from_watch_event(
                ev3,
                unittest.mock.sentinel.raw_object_v2,
                unittest.mock.sentinel.object_v2,
            )
        )

    async def test_added_with_different_uids_does_not_link(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid1,
                namespace=unittest.mock.sentinel.namespace1,
                name=unittest.mock.sentinel.name1,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid2,
                namespace=unittest.mock.sentinel.namespace2,
                name=unittest.mock.sentinel.name2,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()
            item_v1 = await stream_iter.__anext__()
            item_v2 = await stream_iter.__anext__()

        watch_events.assert_called_once_with(
            unittest.mock.sentinel.api_client,
            self.stream_factory,
        )

        self.assertEqual(
            item_v1,
            watcher.StatefulWatchEvent.from_watch_event(
                ev1,
                {},
                None,
            )
        )

        self.assertEqual(
            item_v2,
            watcher.StatefulWatchEvent.from_watch_event(
                ev2,
                {},
                None,
            )
        )

    async def test_added_with_different_uids_but_same_name_info_assumes_corrupted_state(self):  # NOQA
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid1,
                namespace=unittest.mock.sentinel.namespace1,
                name=unittest.mock.sentinel.name1,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid2,
                namespace=unittest.mock.sentinel.namespace1,
                name=unittest.mock.sentinel.name1,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            with self.assertRaisesRegex(
                    RuntimeError,
                    "internal state corruption: "
                    "UIDs and namespace/name pairs do not refer to the same "
                    "object. Object by UID: _ObjectState.*. "
                    "Object by Name: _ObjectState.*"):
                await stream_iter.__anext__()

        watch_events.assert_called_once_with(
            unittest.mock.sentinel.api_client,
            self.stream_factory,
        )

    async def test_added_with_same_uids_but_different_name_info_assumes_corrupted_state(self):  # NOQA
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid1,
                namespace=unittest.mock.sentinel.namespace1,
                name=unittest.mock.sentinel.name1,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid1,
                namespace=unittest.mock.sentinel.namespace2,
                name=unittest.mock.sentinel.name2,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            with self.assertRaisesRegex(
                    RuntimeError,
                    "UIDs and namespace/name pairs do not refer to the same "
                    "object"):
                await stream_iter.__anext__()

        watch_events.assert_called_once_with(
            unittest.mock.sentinel.api_client,
            self.stream_factory,
        )

    async def test_added_deleted_sequence(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.DELETED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()
            _ = await stream_iter.__anext__()
            item_v2 = await stream_iter.__anext__()

        watch_events.assert_called_once_with(
            unittest.mock.sentinel.api_client,
            self.stream_factory,
        )

        self.assertEqual(
            item_v2,
            watcher.StatefulWatchEvent.from_watch_event(
                ev2,
                unittest.mock.sentinel.raw_object_v1,
                unittest.mock.sentinel.object_v1,
            )
        )

    async def test_added_deleted_added_with_same_uids_does_not_link(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.DELETED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        ev3 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v3,
            object_=unittest.mock.sentinel.object_v3,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2
            yield ev3

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()
            _ = await stream_iter.__anext__()
            _ = await stream_iter.__anext__()
            item_v3 = await stream_iter.__anext__()

        watch_events.assert_called_once_with(
            unittest.mock.sentinel.api_client,
            self.stream_factory,
        )

        self.assertEqual(
            item_v3,
            watcher.StatefulWatchEvent.from_watch_event(
                ev3,
                {},
                None,
            )
        )

    async def test_added_stop_added_does_not_link(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        async def watch_event_generator1():
            yield ev1

        async def watch_event_generator2():
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator1()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

            watch_events.return_value = watch_event_generator2()
            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()
            item_v2 = await stream_iter.__anext__()

        self.assertEqual(
            item_v2,
            watcher.StatefulWatchEvent.from_watch_event(
                ev2,
                {},
                None,
            )
        )

    def test_get_by_name_raises_KeyError(self):
        with self.assertRaises(KeyError):
            self.sw.get_by_name(unittest.mock.sentinel.namespace,
                                unittest.mock.sentinel.name)

    async def test_get_by_name_returns_known_objects_while_running(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.MODIFIED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()

            await stream_iter.__anext__()
            raw_object, object_ = self.sw.get_by_name(
                unittest.mock.sentinel.namespace,
                unittest.mock.sentinel.name,
            )
            self.assertEqual(
                raw_object,
                unittest.mock.sentinel.raw_object_v1,
            )
            self.assertEqual(
                object_,
                unittest.mock.sentinel.object_v1,
            )

            await stream_iter.__anext__()
            raw_object, object_ = self.sw.get_by_name(
                unittest.mock.sentinel.namespace,
                unittest.mock.sentinel.name,
            )
            self.assertEqual(
                raw_object,
                unittest.mock.sentinel.raw_object_v2,
            )
            self.assertEqual(
                object_,
                unittest.mock.sentinel.object_v2,
            )

    async def test_get_by_name_raises_KeyError_after_delete(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.MODIFIED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        ev3 = watcher.WatchEvent(
            type_=watcher.EventType.DELETED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v3,
            object_=unittest.mock.sentinel.object_v3,
        )

        ev4 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v4,
            object_=unittest.mock.sentinel.object_v4,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2
            yield ev3
            yield ev4

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()

            await stream_iter.__anext__()
            await stream_iter.__anext__()
            await stream_iter.__anext__()

            with self.assertRaises(KeyError):
                self.sw.get_by_name(
                    unittest.mock.sentinel.namespace,
                    unittest.mock.sentinel.name,
                )

    async def test_handles_delete_without_added_or_modified_without_corruption(self):  # noqa: E501
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.DELETED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v3,
            object_=unittest.mock.sentinel.object_v3,
        )

        async def watch_event_generator():
            yield ev1

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()

            await stream_iter.__anext__()

            with self.assertRaises(KeyError):
                self.sw.get_by_name(
                    unittest.mock.sentinel.namespace,
                    unittest.mock.sentinel.name,
                )

    async def test_get_by_name_while_running_with_different_objects(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid1,
                namespace=unittest.mock.sentinel.namespace1,
                name=unittest.mock.sentinel.name1,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid2,
                namespace=unittest.mock.sentinel.namespace2,
                name=unittest.mock.sentinel.name2,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()

            await stream_iter.__anext__()
            await stream_iter.__anext__()

            raw_object, object_ = self.sw.get_by_name(
                unittest.mock.sentinel.namespace1,
                unittest.mock.sentinel.name1,
            )
            self.assertEqual(
                raw_object,
                unittest.mock.sentinel.raw_object_v1,
            )
            self.assertEqual(
                object_,
                unittest.mock.sentinel.object_v1,
            )
            raw_object, object_ = self.sw.get_by_name(
                unittest.mock.sentinel.namespace2,
                unittest.mock.sentinel.name2,
            )
            self.assertEqual(
                raw_object,
                unittest.mock.sentinel.raw_object_v2,
            )
            self.assertEqual(
                object_,
                unittest.mock.sentinel.object_v2,
            )

    async def test_get_by_name_raises_KeyError_after_exit(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid1,
                namespace=unittest.mock.sentinel.namespace1,
                name=unittest.mock.sentinel.name1,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid2,
                namespace=unittest.mock.sentinel.namespace2,
                name=unittest.mock.sentinel.name2,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()

            await stream_iter.__anext__()
            await stream_iter.__anext__()

            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

        with self.assertRaises(KeyError):
            self.sw.get_by_name(
                unittest.mock.sentinel.namespace1,
                unittest.mock.sentinel.name1,
            )

        with self.assertRaises(KeyError):
            self.sw.get_by_name(
                unittest.mock.sentinel.namespace2,
                unittest.mock.sentinel.name2,
            )

    async def test_stream_events_cannot_be_run_in_parallel(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid1,
                namespace=unittest.mock.sentinel.namespace1,
                name=unittest.mock.sentinel.name1,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        def watch_event_generator_generator():
            for i in itertools.count():
                async def generator():
                    yield ev1
                yield generator()

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.side_effect = watch_event_generator_generator()

            stream1 = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream2 = self.sw.stream_events(unittest.mock.sentinel.api_client)

            stream2_iter = stream2.__aiter__()
            stream1_iter = stream1.__aiter__()

            await stream2_iter.__anext__()

            with self.assertRaises(watcher.CorruptionAvoided):
                await stream1_iter.__anext__()

    async def test_stream_events_can_be_run_sequentially(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid1,
                namespace=unittest.mock.sentinel.namespace1,
                name=unittest.mock.sentinel.name1,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        def watch_event_generator_generator():
            for i in itertools.count():
                async def generator():
                    yield ev1
                yield generator()

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.side_effect = watch_event_generator_generator()

            stream1 = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream2 = self.sw.stream_events(unittest.mock.sentinel.api_client)

            stream2_iter = stream2.__aiter__()
            stream1_iter = stream1.__aiter__()

            await stream2_iter.__anext__()

            with self.assertRaises(StopAsyncIteration):
                await stream2_iter.__anext__()

            await stream1_iter.__anext__()

    async def test_use_of_stream_events_cancels_properly(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid1,
                namespace=unittest.mock.sentinel.namespace1,
                name=unittest.mock.sentinel.name1,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        def watch_event_generator_generator():
            async def generator():
                await asyncio.sleep(10)
                yield ev1
            yield generator()
            for i in itertools.count():
                async def generator():
                    yield ev1
                yield generator()

        async def user():
            async for ev in self.sw.stream_events(
                    unittest.mock.sentinel.api_client):
                pass

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.side_effect = watch_event_generator_generator()

            task = asyncio.create_task(user())
            # force scheduling
            await asyncio.sleep(0)
            task.cancel()
            with self.assertRaises(asyncio.CancelledError):
                await task

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()

    async def test_get_all_returns_empty_iterable(self):
        self.assertCountEqual(
            self.sw.get_all(),
            [],
        )

    async def test_get_all_returns_known_objects_while_running(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.MODIFIED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()

            await stream_iter.__anext__()
            self.assertCountEqual(
                self.sw.get_all(),
                [
                    (unittest.mock.sentinel.raw_object_v1,
                     unittest.mock.sentinel.object_v1),
                ]
            )

            await stream_iter.__anext__()
            self.assertCountEqual(
                self.sw.get_all(),
                [
                    (unittest.mock.sentinel.raw_object_v2,
                     unittest.mock.sentinel.object_v2),
                ]
            )

    async def test_get_all_does_not_return_object_after_delete(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.MODIFIED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        ev3 = watcher.WatchEvent(
            type_=watcher.EventType.DELETED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v3,
            object_=unittest.mock.sentinel.object_v3,
        )

        ev4 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid,
                namespace=unittest.mock.sentinel.namespace,
                name=unittest.mock.sentinel.name,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v4,
            object_=unittest.mock.sentinel.object_v4,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2
            yield ev3
            yield ev4

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()

            await stream_iter.__anext__()
            await stream_iter.__anext__()
            await stream_iter.__anext__()

            self.assertCountEqual(
                self.sw.get_all(),
                [],
            )

    async def test_get_all_while_running_with_different_objects(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid1,
                namespace=unittest.mock.sentinel.namespace1,
                name=unittest.mock.sentinel.name1,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid2,
                namespace=unittest.mock.sentinel.namespace2,
                name=unittest.mock.sentinel.name2,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()

            await stream_iter.__anext__()
            await stream_iter.__anext__()

            self.assertCountEqual(
                self.sw.get_all(),
                [
                    (unittest.mock.sentinel.raw_object_v1,
                     unittest.mock.sentinel.object_v1),
                    (unittest.mock.sentinel.raw_object_v2,
                     unittest.mock.sentinel.object_v2),
                ],
            )

    async def test_get_all_returns_empty_iterable_after_exit(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid1,
                namespace=unittest.mock.sentinel.namespace1,
                name=unittest.mock.sentinel.name1,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v1,
            object_=unittest.mock.sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=unittest.mock.sentinel.uid2,
                namespace=unittest.mock.sentinel.namespace2,
                name=unittest.mock.sentinel.name2,
                api_version=unittest.mock.sentinel.api_version,
                kind=unittest.mock.sentinel.kind,
            ),
            raw_object=unittest.mock.sentinel.raw_object_v2,
            object_=unittest.mock.sentinel.object_v2,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(unittest.mock.sentinel.api_client)
            stream_iter = stream.__aiter__()

            await stream_iter.__anext__()
            await stream_iter.__anext__()

            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

        self.assertCountEqual(
            self.sw.get_all(),
            [],
        )


class TestStatefulWatcher_WithStash(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.stream_factory = unittest.mock.Mock([])
        self.stream_factory.return_value = unittest.mock.sentinel.stream
        self.sw = watcher.StatefulWatcher(
            self.stream_factory,
            restore_state_on_restart=True,
        )

    async def test_get_all_returns_empty_iterable_after_exit(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid1,
                namespace=sentinel.namespace1,
                name=sentinel.name1,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v1,
            object_=sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid2,
                namespace=sentinel.namespace2,
                name=sentinel.name2,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v2,
            object_=sentinel.object_v2,
        )

        async def watch_event_generator():
            yield ev1
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator()

            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()

            await stream_iter.__anext__()
            await stream_iter.__anext__()

            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

        self.assertCountEqual(
            self.sw.get_all(),
            [],
        )

    async def test_added_stop_added_converts_to_modified(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v1,
            object_=sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v2,
            object_=sentinel.object_v2,
        )

        async def watch_event_generator1():
            yield ev1

        async def watch_event_generator2():
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator1()

            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

            watch_events.return_value = watch_event_generator2()
            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            item_v2 = await stream_iter.__anext__()

        self.assertEqual(
            item_v2,
            watcher.StatefulWatchEvent.from_watch_event(
                dataclasses.replace(ev2, type_=watcher.EventType.MODIFIED),
                old_raw_object=sentinel.raw_object_v1,
                old_object=sentinel.object_v1,
            )
        )

    async def test_added_stop_added_suppresses_on_raw_object_equality(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v1,
            object_=sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v1,
            object_=sentinel.object_v2,
        )

        async def watch_event_generator1():
            yield ev1

        async def watch_event_generator2():
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator1()

            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

            watch_events.return_value = watch_event_generator2()
            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

    async def test_added_stop_added_modified_does_not_crash(self):
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v1,
            object_=sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v2,
            object_=sentinel.object_v2,
        )

        ev3 = watcher.WatchEvent(
            type_=watcher.EventType.MODIFIED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v3,
            object_=sentinel.object_v3,
        )

        async def watch_event_generator1():
            yield ev1

        async def watch_event_generator2():
            yield ev2
            yield ev3

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator1()

            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

            watch_events.return_value = watch_event_generator2()
            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            item_v3 = await stream_iter.__anext__()

        self.assertEqual(
            item_v3,
            watcher.StatefulWatchEvent.from_watch_event(
                ev3,
                old_raw_object=sentinel.raw_object_v2,
                old_object=sentinel.object_v2,
            )
        )

    async def test_added_stop_modified_with_different_uid_injects_delete(self):
        raw_object_v1 = {
            "apiVersion": sentinel.api_version,
            "kind": sentinel.kind,
            "metadata": {
                "uid": sentinel.uid1,
            },
            "spec": sentinel.raw_spec_v1,
        }
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid1,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=raw_object_v1,
            object_=sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.MODIFIED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid2,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v2,
            object_=sentinel.object_v2,
        )

        async def watch_event_generator1():
            yield ev1

        async def watch_event_generator2():
            yield ev2

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator1()

            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

            watch_events.return_value = watch_event_generator2()
            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            item_del = await stream_iter.__anext__()
            item_v2 = await stream_iter.__anext__()

        self.assertEqual(
            item_del,
            watcher.StatefulWatchEvent.from_watch_event(
                watcher.WatchEvent(
                    type_=watcher.EventType.DELETED,
                    reference=kubernetes_asyncio.client.V1ObjectReference(
                        name=sentinel.name,
                        namespace=sentinel.namespace,
                        uid=sentinel.uid1,
                        api_version=sentinel.api_version,
                        kind=sentinel.kind,
                    ),
                    raw_object=raw_object_v1,
                    object_=sentinel.object_v1,
                ),
                old_raw_object=raw_object_v1,
                old_object=sentinel.object_v1,
            )
        )

        self.assertEqual(
            item_v2,
            watcher.StatefulWatchEvent.from_watch_event(
                ev2,
                old_raw_object={},
                old_object=None,
            )
        )

    async def test_stream_events_injects_deletion_on_first_non_added_event(self):  # noqa: E501
        raw_object_v1 = {
            "apiVersion": sentinel.api_version,
            "kind": sentinel.kind,
            "metadata": {
                "uid": sentinel.uid1,
            },
            "spec": sentinel.raw_spec_v1,
        }
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid1,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=raw_object_v1,
            object_=sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid2,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=raw_object_v1,
            object_=sentinel.object_v1,
        )
        ev3 = watcher.WatchEvent(
            type_=watcher.EventType.DELETED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid2,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v2,
            object_=sentinel.object_v2,
        )

        async def watch_event_generator1():
            yield ev1

        async def watch_event_generator2():
            yield ev2
            yield ev3

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator1()

            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

            watch_events.return_value = watch_event_generator2()
            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            # the added event we don’t care about
            _ = await stream_iter.__anext__()
            # the injected deletion
            item_del = await stream_iter.__anext__()

        self.assertEqual(
            item_del,
            watcher.StatefulWatchEvent.from_watch_event(
                watcher.WatchEvent(
                    type_=watcher.EventType.DELETED,
                    reference=kubernetes_asyncio.client.V1ObjectReference(
                        name=sentinel.name,
                        namespace=sentinel.namespace,
                        uid=sentinel.uid1,
                        api_version=sentinel.api_version,
                        kind=sentinel.kind,
                    ),
                    raw_object=raw_object_v1,
                    object_=sentinel.object_v1,
                ),
                old_raw_object=raw_object_v1,
                old_object=sentinel.object_v1,
            )
        )

    async def test_stream_events_drops_stash_on_first_non_added_event(self):  # noqa: E501
        raw_object_v1 = {
            "apiVersion": sentinel.api_version,
            "kind": sentinel.kind,
            "metadata": {
                "uid": sentinel.uid1,
            },
            "spec": sentinel.raw_spec_v1,
        }
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid1,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=raw_object_v1,
            object_=sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid2,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=raw_object_v1,
            object_=sentinel.object_v1,
        )

        ev3 = watcher.WatchEvent(
            type_=watcher.EventType.DELETED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid2,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v2,
            object_=sentinel.object_v2,
        )

        ev4 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid1,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v2,
            object_=sentinel.object_v2,
        )

        async def watch_event_generator1():
            yield ev1

        async def watch_event_generator2():
            yield ev2
            yield ev3
            yield ev4

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator1()

            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

            watch_events.return_value = watch_event_generator2()
            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            # the added event we don’t care about
            await stream_iter.__anext__()
            # the injected deletion
            await stream_iter.__anext__()
            # the real deletion
            await stream_iter.__anext__()
            # and this one is the second added event which uses the same uid
            # as the stash; as there was a deletion in between the stash should
            # have been dropped
            item_ev4 = await stream_iter.__anext__()

        self.assertEqual(
            item_ev4,
            watcher.StatefulWatchEvent.from_watch_event(
                ev4,
                old_raw_object={},
                old_object=None,
            )
        )

    async def test_get_by_name_uid_answers_based_on_stash_if_running(self):  # noqa: E501
        raw_object_v1 = {
            "apiVersion": sentinel.api_version,
            "kind": sentinel.kind,
            "metadata": {
                "uid": sentinel.uid1,
            },
            "spec": sentinel.raw_spec_v1,
        }
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid1,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=raw_object_v1,
            object_=sentinel.object_v1,
        )

        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid2,
                namespace=sentinel.namespace,
                name=sentinel.name2,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v2,
            object_=sentinel.object_v2,
        )

        ev3 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid1,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=raw_object_v1,
            object_=sentinel.object_v1,
        )

        ev4 = watcher.WatchEvent(
            type_=watcher.EventType.DELETED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid2,
                namespace=sentinel.namespace,
                name=sentinel.name2,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v2,
            object_=sentinel.object_v2,
        )

        async def watch_event_generator1():
            yield ev1

        async def watch_event_generator2():
            yield ev2
            yield ev3
            yield ev4

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator1()

            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

            watch_events.return_value = watch_event_generator2()
            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            # Until now we only saw the first object, so we are in process of
            # replaying from the stash

            item_by_name, _ = self.sw.get_by_name(
                sentinel.namespace, sentinel.name)

            self.assertEqual(item_by_name, raw_object_v1)

            # Once we check the next object we'll get the values again
            await stream_iter.__anext__()

            item_by_name, _ = self.sw.get_by_name(
                sentinel.namespace, sentinel.name)

        self.assertEqual(item_by_name, raw_object_v1)

    async def test_get_by_name_uid_answers_based_on_stash_if_not_running(self):  # noqa: E501
        raw_object_v1 = {
            "apiVersion": sentinel.api_version,
            "kind": sentinel.kind,
            "metadata": {
                "uid": sentinel.uid1,
            },
            "spec": sentinel.raw_spec_v1,
        }
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid1,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=raw_object_v1,
            object_=sentinel.object_v1,
        )

        async def watch_event_generator1():
            yield ev1

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator1()

            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

            item_by_name, _ = self.sw.get_by_name(
                sentinel.namespace, sentinel.name)

        self.assertEqual(item_by_name, raw_object_v1)

    async def test_stream_events_does_not_preserve_state_for_restart_on_corruption(self):  # noqa: E501
        ev1 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid1,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v1,
            object_=sentinel.object_v1,
        )

        # this event causes corruption
        ev2 = watcher.WatchEvent(
            type_=watcher.EventType.DELETED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid1,
                namespace=sentinel.namespace2,
                name=sentinel.name2,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object2_v1,
            object_=sentinel.object2_v1,
        )

        ev3 = watcher.WatchEvent(
            type_=watcher.EventType.ADDED,
            reference=kubernetes_asyncio.client.V1ObjectReference(
                uid=sentinel.uid1,
                namespace=sentinel.namespace,
                name=sentinel.name,
                api_version=sentinel.api_version,
                kind=sentinel.kind,
            ),
            raw_object=sentinel.raw_object_v2,
            object_=sentinel.object_v2,
        )

        async def watch_event_generator1():
            # inject valid object
            yield ev1
            # cause corruption
            yield ev2

        async def watch_event_generator2():
            # re-add valid object which should now not be rewritten
            yield ev3

        with contextlib.ExitStack() as stack:
            watch_events = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.watcher.watch_events",
                new=unittest.mock.Mock(),
            ))
            watch_events.return_value = watch_event_generator1()

            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            await stream_iter.__anext__()
            with self.assertRaisesRegex(RuntimeError,
                                        "internal state corruption"):
                await stream_iter.__anext__()
            with self.assertRaises(StopAsyncIteration):
                await stream_iter.__anext__()

            watch_events.return_value = watch_event_generator2()
            stream = self.sw.stream_events(sentinel.api_client)
            stream_iter = stream.__aiter__()
            item_v2 = await stream_iter.__anext__()

        self.assertEqual(
            item_v2,
            watcher.StatefulWatchEvent.from_watch_event(
                ev3,
                old_raw_object={},
                old_object=None,
            )
        )


class TestExternalWatcher(unittest.IsolatedAsyncioTestCase):
    class MockExternalWatcher(watcher.ExternalWatcher):
        def __init__(self):
            super().__init__()
            self.get_state_mock = unittest.mock.Mock()
            self.get_state_mock.return_value = {
                "cr1": {"name": unittest.mock.sentinel.cr1_event}
            }
            self.to_dict_mock = unittest.mock.Mock()
            self.to_kubernetes_reference_mock = unittest.mock.Mock()

        async def get_state(self):
            return self.get_state_mock()

        def to_dict(self, obj):
            return self.to_dict_mock(obj)

        def to_kubernetes_reference(self, name):
            return self.to_kubernetes_reference_mock(name)

    class AbortException(Exception):
        pass

    def setUp(self):
        self.ew = TestExternalWatcher.MockExternalWatcher()

    # def tearDown(self):
    #     for patch in self.__patches:
    #         patch.stop()
    #     super().tearDown()

    # def stopWatcher(self):
    #     self.__patches = [
    #         unittest.mock.patch(
    #             "asyncio.sleep",
    #             new=unittest.mock.Mock(
    #                 side_effect=TestExternalWatcher.AbortException),
    #         )
    #     ]
    #     for patch in self.__patches:
    #         patch.start()

    async def test_stream_events_cannot_be_run_in_parallel(self):
        stream1 = self.ew.stream_events()
        stream2 = self.ew.stream_events()

        stream1_iter = stream1.__aiter__()
        stream2_iter = stream2.__aiter__()

        await stream1_iter.__anext__()

        with self.assertRaises(watcher.CorruptionAvoided):
            await stream2_iter.__anext__()

    async def test_stream_events_creates_added_events(self):
        self.ew.to_kubernetes_reference_mock.return_value = sentinel.reference
        self.ew.to_dict_mock.return_value = sentinel.raw_object

        stream1 = self.ew.stream_events()
        stream1_iter = stream1.__aiter__()
        event = await stream1_iter.__anext__()

        self.assertEqual(event.type_, watcher.EventType.ADDED)
        self.assertEqual(event.reference, sentinel.reference)
        self.assertEqual(event.raw_object, sentinel.raw_object)

        self.ew.to_kubernetes_reference_mock.assert_called_once_with("cr1")
        self.ew.to_dict_mock.assert_called_once_with(
            {"name": unittest.mock.sentinel.cr1_event})

    @unittest.mock.patch("asyncio.sleep")
    async def test_stream_events_creates_modified_events(self, sleep):
        def passthrough_name(x):
            return x["name"]

        self.ew.to_kubernetes_reference_mock.return_value = sentinel.reference
        self.ew.to_dict_mock.side_effect = passthrough_name
        self.ew.get_state_mock.side_effect = [
            {"cr1": {"name": sentinel.cr1_event}},
            {"cr1": {"name": sentinel.cr1_event_other}}
        ]

        stream1 = self.ew.stream_events()
        stream1_iter = stream1.__aiter__()
        event = await stream1_iter.__anext__()

        self.assertEqual(event.type_, watcher.EventType.ADDED)
        self.assertEqual(event.reference, sentinel.reference)
        self.assertEqual(event.raw_object, sentinel.cr1_event)

        self.ew.to_kubernetes_reference_mock.assert_called_once_with("cr1")
        self.ew.to_dict_mock.assert_called_once_with(
            {"name": sentinel.cr1_event})

        self.ew.to_kubernetes_reference_mock.reset_mock()
        self.ew.to_dict_mock.reset_mock()
        event = await stream1_iter.__anext__()

        self.assertEqual(event.type_, watcher.EventType.MODIFIED)
        self.assertEqual(event.reference, sentinel.reference)
        self.assertEqual(event.raw_object, sentinel.cr1_event_other)
        self.assertEqual(event.old_raw_object, sentinel.cr1_event)

        self.ew.to_kubernetes_reference_mock.assert_called_once_with("cr1")
        self.ew.to_dict_mock.assert_any_call(
            {"name": sentinel.cr1_event})
        self.ew.to_dict_mock.assert_any_call(
            {"name": sentinel.cr1_event_other})

    @unittest.mock.patch("asyncio.sleep")
    async def test_stream_events_creates_deleted_events(self, sleep):
        def passthrough_name(x):
            return x["name"]

        self.ew.to_kubernetes_reference_mock.return_value = sentinel.reference
        self.ew.to_dict_mock.side_effect = passthrough_name
        self.ew.get_state_mock.side_effect = [
            {"cr1": {"name": sentinel.cr1_event}},
            {}
        ]

        stream1 = self.ew.stream_events()
        stream1_iter = stream1.__aiter__()
        event = await stream1_iter.__anext__()

        self.assertEqual(event.type_, watcher.EventType.ADDED)
        self.assertEqual(event.reference, sentinel.reference)
        self.assertEqual(event.raw_object, sentinel.cr1_event)

        self.ew.to_kubernetes_reference_mock.assert_called_once_with("cr1")
        self.ew.to_dict_mock.assert_called_once_with(
            {"name": sentinel.cr1_event})

        self.ew.to_kubernetes_reference_mock.reset_mock()
        self.ew.to_dict_mock.reset_mock()
        event = await stream1_iter.__anext__()

        self.assertEqual(event.type_, watcher.EventType.DELETED)
        self.assertEqual(event.reference, sentinel.reference)
        self.assertEqual(event.raw_object, {})
        self.assertEqual(event.old_raw_object, sentinel.cr1_event)

        self.ew.to_kubernetes_reference_mock.assert_called_once_with("cr1")
        self.ew.to_dict_mock.assert_any_call(
            {"name": sentinel.cr1_event})
