#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import ddt
import unittest
import unittest.mock
import uuid
import openstack
import openstack.exceptions
import random

# TODO(resource-refactor): clean up imports
import yaook.statemachine.resources.openstack as resource
import yaook.op.neutron_l3.eviction as eviction
import yaook.statemachine.customresource as customresource

NAMESPACE = f"namespace-{uuid.uuid4()}"
NAME = f"name-{uuid.uuid4()}"


def repeatable_generator(iterable):
    def generator(*args, **kwargs):
        yield from iterable

    return generator


class TestL3AgentInfo(unittest.TestCase):
    def test_get_raises_lookup_error_if_not_found(self):
        client = unittest.mock.Mock(["agents"])
        client.agents.return_value = []
        with self.assertRaisesRegex(
            LookupError, "correct on correct not found"
        ):
            resource.get_network_agent(
                client,
                host="correct",
                binary="correct",
            )

    def test_disable_invokes_api(self):
        client = unittest.mock.Mock(["put"])
        agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
        agent.id = unittest.mock.sentinel.agent_id
        with contextlib.ExitStack() as stack:
            raise_ = stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.resources.openstack.raise_response_error_if_any",  # noqa E501
                )
            )
            resource.disable_network_agent(client, agent)
        client.put.assert_called_once_with(
            f"/agents/{agent.id}",
            json={
                "agent": {
                    "admin_state_up": False,
                },
            },
        )
        raise_.assert_called_once_with(client.put())


@ddt.ddt
class TestEvictor(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.intf = unittest.mock.Mock(
            "yaook.statemachine.interfaces.ResourceInterfaceWithStatus",
        )
        self.connection_info = {
            "auth": unittest.mock.sentinel.auth,
            "interface": unittest.mock.sentinel.interface,
        }
        self.e = eviction.Evictor(
            interface=self.intf,
            namespace=NAMESPACE,
            node_name=NAME,
            max_parallel_migrations=10,
            respect_azs=False,
            verify_seconds=0,
            handle_non_migrated_routers_as_unhandleable=True,
            connection_info=self.connection_info,
            reason="some-reason",
        )
        self.os_connection = unittest.mock.Mock()
        self.agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
        self.agent.id = unittest.mock.sentinel.agent_id
        self.agent.host = NAME
        self.agent.binary = "neutron-l3-agent"
        self.agent.is_alive = True
        self.agent.is_admin_state_up = True
        self.connect_mock = unittest.mock.Mock([])
        self.connect_mock.return_value = self.os_connection
        self.get_network_agent_mock = unittest.mock.Mock([])
        self.get_network_agent_mock.return_value = self.agent
        self.disable_network_agent_mock = unittest.mock.Mock([])
        self._migration_status_mock = unittest.mock.Mock([])
        self.list_l3_agents_hosting_router_mock = unittest.mock.Mock([])

        self.iterate_mock = unittest.mock.Mock([])
        self.__os_patches = [
            unittest.mock.patch.object(
                self.e, "_migration_status", new=self._migration_status_mock
            ),
            unittest.mock.patch.object(self.e, "_dump_status"),
        ]
        self.__iterate_patches = [
            unittest.mock.patch.object(
                self.e, "_connect", new=self.connect_mock
            ),
            unittest.mock.patch(
                "yaook.statemachine.resources.openstack.get_network_agent",
                new=self.get_network_agent_mock,
            ),
            unittest.mock.patch(
                "yaook.statemachine.resources.openstack.disable_network_agent",
                new=self.disable_network_agent_mock,
            ),
            unittest.mock.patch.object(
                self.e, "_iterate", new=self.iterate_mock
            ),
        ]
        self.__patches = []

    def tearDown(self):
        for p in self.__patches:
            p.stop()

    def _patch_os(self):
        for p in self.__os_patches:
            p.start()
            self.__patches.append(p)

    def _patch_iterate(self):
        for p in self.__iterate_patches:
            p.start()
            self.__patches.append(p)

    def _mock_router(self, *, id_=None, status="ACTIVE"):
        router = unittest.mock.Mock([])
        router.id = str(uuid.uuid4() if id_ is None else id_)
        router.status = status
        router.is_admin_state_up = True
        return router

    def _mock_agent(
        self,
        *,
        id_=None,
        ha_state="Active",
        az=None,
        is_alive=True,
        is_admin_state_up=True,
    ):
        agent = unittest.mock.Mock([])
        agent.id = str(uuid.uuid4() if id_ is None else id_)
        agent.ha_state = ha_state
        agent.availability_zone = str(uuid.uuid4() if az is None else az)
        agent.is_admin_state_up = is_admin_state_up
        agent.is_alive = is_alive
        return agent

    def create_agent_with_routers(
        self,
        agent_dict,
        routers,
        count,
        az=None,
        is_alive=True,
        is_admin_state_up=True,
    ):
        agent = self._mock_agent(
            az=az, is_alive=is_alive, is_admin_state_up=is_admin_state_up
        )
        selected = set()
        for i in range(count):
            selected.add(routers[i].id)

        h_agent = eviction.HashableAgent(agent)
        agent_dict.update({h_agent: selected})

        return h_agent

    def test__connect(self):
        with contextlib.ExitStack() as stack:
            connect = stack.enter_context(
                unittest.mock.patch(
                    "openstack.connect",
                )
            )
            result = self.e._connect()
        connect.assert_called_once_with(
            auth=unittest.mock.sentinel.auth,
            interface=unittest.mock.sentinel.interface,
        )
        self.assertEqual(result, connect())

    async def test__move_router_verify(self):
        router = unittest.mock.Mock(openstack.network.v2.router.Router)
        router.id = str(uuid.uuid4())
        src_agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
        src_agent.id = str(uuid.uuid4())
        src_agent.ha_state = "active"
        dst_agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
        dst_agent.id = str(uuid.uuid4())
        dst_agent.ha_state = "standby"

        with contextlib.ExitStack() as stack:
            stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_connect",
                    new=self.connect_mock,
                )
            )
            asleep = stack.enter_context(
                unittest.mock.patch(
                    "asyncio.sleep",
                )
            )
            _get_active_agents_for_router = stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_get_active_agents_for_router",
                )
            )
            _get_active_agents_for_router.side_effect = [
                [src_agent],
                [],  # 1s
                [],  # 2s
                [],  # 3s
                [],  # 4s
                [dst_agent],  # 5s
            ]
            status = await self.e._move_router(
                router, src_agent, dst_agent, 10
            )
            _get_active_agents_for_router.assert_called_with(
                self.os_connection, router
            )
            self.assertEqual(status, eviction.EvictResult.MIGRATED)
            self.assertEqual(_get_active_agents_for_router.call_count, 6)
            self.assertEqual(asleep.call_count, 5)

            self.os_connection.network.add_router_to_agent.assert_called_once_with(  # NOQA: E501
                dst_agent, router
            )
            self.os_connection.network.remove_router_from_agent.assert_called_once_with(  # NOQA: E501
                src_agent, router
            )

    async def test__move_router_raises_neutron_error(self):
        router = unittest.mock.Mock(openstack.network.v2.router.Router)
        router.id = str(uuid.uuid4())
        src_agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
        src_agent.id = str(uuid.uuid4())
        src_agent.ha_state = "active"
        dst_agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
        dst_agent.id = str(uuid.uuid4())
        dst_agent.ha_state = "standby"

        with contextlib.ExitStack() as stack:
            stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_connect",
                    new=self.connect_mock,
                )
            )
            os_add_router_to_agent = stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "os_add_router_to_agent",
                )
            )
            os_add_router_to_agent.return_value = {
                "NeutronError": {
                    "type": "NeutronDbObjectDuplicateEntry",
                    "message": (
                        "Failed to create a duplicate RouterL3AgentBinding:"
                        " for attribute(s) ['router_id', 'binding_index', '']"
                        f" with value(s) {router.id}-3"
                    ),
                    "detail": "",
                }
            }

            with self.assertRaises(eviction.NeutronAPIError):
                await self.e._move_router(router, src_agent, dst_agent, 0)

    async def test__router_is_not_active_on_agent(self):
        router = unittest.mock.Mock(openstack.network.v2.router.Router)
        router.id = str(uuid.uuid4())
        active_agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
        active_agent.id = str(uuid.uuid4())
        active_agent.ha_state = "active"
        standby_agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
        standby_agent.id = str(uuid.uuid4())
        standby_agent.ha_state = "standby"

        with contextlib.ExitStack() as stack:
            stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_connect",
                    new=self.connect_mock,
                )
            )
            _get_active_agents_for_router = stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_get_active_agents_for_router",
                )
            )
            _get_active_agents_for_router.return_value = [active_agent]
            result = await self.e._router_is_not_active_on_agent(
                router, active_agent
            )
            self.assertEqual(result, eviction.EvictResult.PENDING)

            result = await self.e._router_is_not_active_on_agent(
                router, standby_agent
            )
            self.assertEqual(result, eviction.EvictResult.MIGRATED)

    async def test__get_active_agents_for_router(self):
        agents = [
            unittest.mock.Mock(openstack.network.v2.agent.Agent)
            for _ in range(5)
        ]
        agents[0].ha_state = "standby"
        agents[1].ha_state = "standby"
        agents[2].ha_state = "active"
        agents[3].ha_state = "active"
        agents[4].ha_state = "standby"
        router = unittest.mock.Mock(openstack.network.v2.router.Router)
        router.id = str(uuid.uuid4())
        self.os_connection.network.routers_hosting_l3_agents.return_value = (
            agents
        )
        active_agents = self.e._get_active_agents_for_router(
            self.os_connection, router
        )
        self.assertEqual(active_agents, [agents[2], agents[3]])

    async def test__is_agent_in_list(self):
        not_in_list_agent = unittest.mock.Mock(
            openstack.network.v2.agent.Agent
        )
        not_in_list_agent.id = str(uuid.uuid4())
        agents = [
            unittest.mock.Mock(openstack.network.v2.agent.Agent)
            for _ in range(5)
        ]
        for agent in agents:
            agent.id = str(uuid.uuid4())

        self.assertEqual(self.e._is_agent_in_list(agents[4], agents), True)
        self.assertEqual(
            self.e._is_agent_in_list(not_in_list_agent, agents), False
        )

    async def test__collect_agents_and_routers(self):
        routers_for_agent = {
            self._mock_agent(): [
                self._mock_router() for _ in range(random.randint(1, 25))
            ]
            for _ in range(4)
        }

        routers_for_agent.update({self._mock_agent(): []})
        expected = {
            eviction.HashableAgent(k): set([x.id for x in v])
            for k, v in routers_for_agent.items()
        }

        self.os_connection.network.agents.return_value = list(
            routers_for_agent.keys()
        )
        self.os_connection.network.agent_hosted_routers = (
            lambda agent, is_ha=None: routers_for_agent[agent]
        )

        mapping = self.e._collect_agents_and_routers(self.os_connection)

        self.assertDictEqual(mapping, expected)

    async def test__select_agent(self):
        target_router = self._mock_router()
        routers = [self._mock_router() for _ in range(25)]
        routers_for_agent = {}
        best_agent = self.create_agent_with_routers(
            routers_for_agent, routers, 1
        )
        full_agent = self.create_agent_with_routers(
            routers_for_agent, routers, 25
        )
        okayish_agent = self.create_agent_with_routers(
            routers_for_agent, routers, 10
        )
        self.create_agent_with_routers(routers_for_agent, [target_router], 1)
        self.create_agent_with_routers(
            routers_for_agent, routers, 1, is_alive=False
        )

        possible_agents = self.e._select_agent(
            target_router, routers_for_agent, False
        )
        self.assertEqual(
            possible_agents, [best_agent, okayish_agent, full_agent]
        )

    async def test__select_agent_availability_zone(self):
        target_router = self._mock_router()
        routers_for_agent = {}
        only_agent = self.create_agent_with_routers(
            routers_for_agent, [], 0, az="az-free"
        )
        self.create_agent_with_routers(
            routers_for_agent, [target_router], 1, az="az1"
        )
        self.create_agent_with_routers(routers_for_agent, [], 0, az="az1")
        self.create_agent_with_routers(
            routers_for_agent, [target_router], 1, az="az2"
        )
        self.create_agent_with_routers(routers_for_agent, [], 0, az="az2")

        possible_agents = self.e._select_agent(
            target_router, routers_for_agent, True
        )
        self.assertEqual(possible_agents, [only_agent])

    async def test__select_agent_no_agent(self):
        target_router = self._mock_router()
        routers_for_agent = {}
        self.create_agent_with_routers(
            routers_for_agent, [target_router], 1, az="az1"
        )
        self.create_agent_with_routers(
            routers_for_agent, [target_router], 1, az="az2"
        )
        self.create_agent_with_routers(
            routers_for_agent, [], 0, az="az3", is_admin_state_up=False
        )

        possible_agents = self.e._select_agent(
            target_router, routers_for_agent, False
        )
        self.assertEqual(possible_agents, [])

    async def test_iterate_success(self):
        routers = [self._mock_router() for _ in range(10)]
        router_id_lookup = {router.id: router for router in routers}
        routers_for_agent = {}
        src_agent = self.create_agent_with_routers(
            routers_for_agent, routers, 10
        )
        dst_agent1 = self.create_agent_with_routers(routers_for_agent, [], 0)
        dst_agent2 = self.create_agent_with_routers(routers_for_agent, [], 0)

        with contextlib.ExitStack() as stack:
            stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_connect",
                    new=self.connect_mock,
                )
            )
            get_agent = stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_get_agent",
                )
            )
            get_agent.return_value = src_agent
            collect_agents_and_routers = stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_collect_agents_and_routers",
                )
            )
            collect_agents_and_routers.return_value = routers_for_agent
            stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.resources.openstack."
                    "raise_response_error_if_any",
                )
            )
            disable_network_agent = stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.resources.openstack."
                    "disable_network_agent",
                )
            )
            disable_network_agent.return_value = None

            self.os_connection.network.find_router.side_effect = (
                lambda router_id: router_id_lookup[router_id]
            )

            status = await self.e.iterate()
            self.assertEqual(status.migrated_router, 10)
            self.assertEqual(status.pending_router, 0)
            self.assertEqual(status.unhandleable_router, 0)
            self.assertFalse(status.unhandleable_agent)
            self.assertTrue(self.e._recheck)
            self.assertEqual(len(routers_for_agent[dst_agent1]), 5)
            self.assertEqual(len(routers_for_agent[dst_agent2]), 5)
            disable_network_agent.assert_called_once_with(
                self.os_connection.network, src_agent.agent
            )

            # as the code pops the src_agent from the dict we need to add
            # it again
            routers_for_agent[src_agent] = set()
            src_agent.agent.is_admin_state_up = False

            status = await self.e.iterate()
            self.assertFalse(self.e._recheck)
            self.assertTrue(await self.e.is_migration_done(status))

    async def test_iterate_handle_non_migrated_routers_as_unhandleable_false(
        self
    ):
        self.e.handle_non_migrated_routers_as_unhandleable = False
        self.e.max_parallel_migrations = 2
        routers = [self._mock_router() for _ in range(10)]
        router_id_lookup = {router.id: router for router in routers}
        routers_for_agent = {}
        src_agent = self.create_agent_with_routers(
            routers_for_agent, routers, 10
        )
        self.create_agent_with_routers(routers_for_agent, routers, 10)
        self.create_agent_with_routers(routers_for_agent, routers, 10)

        with contextlib.ExitStack() as stack:
            stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_connect",
                    new=self.connect_mock,
                )
            )
            get_agent = stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_get_agent",
                )
            )
            get_agent.return_value = src_agent
            collect_agents_and_routers = stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_collect_agents_and_routers",
                )
            )
            collect_agents_and_routers.return_value = routers_for_agent
            stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.resources.openstack."
                    "raise_response_error_if_any",
                )
            )
            disable_network_agent = stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.resources.openstack."
                    "disable_network_agent",
                )
            )
            disable_network_agent.return_value = None
            get_active_agents_for_router = stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_get_active_agents_for_router",
                )
            )
            get_active_agents_for_router.return_value = [
                h_agent.agent for h_agent in routers_for_agent.keys()
            ]

            self.os_connection.network.find_router.side_effect = (
                lambda router_id: router_id_lookup[router_id]
            )

            status = await self.e.iterate()
            self.assertEqual(status.migrated_router, 0)
            self.assertEqual(status.pending_router, 10)
            self.assertEqual(status.unhandleable_router, 0)
            self.assertFalse(status.unhandleable_agent)
            self.assertTrue(self.e._recheck)
            disable_network_agent.assert_called_once_with(
                self.os_connection.network, src_agent.agent
            )
            get_active_agents_for_router.return_value = []

            # as the code pops the src_agent from the dict we need to add
            # it again
            routers_for_agent[src_agent] = set(
                [router.id for router in routers]
            )
            src_agent.agent.is_admin_state_up = False

            status = await self.e.iterate()
            self.assertEqual(status.migrated_router, 10)
            self.assertFalse(self.e._recheck)
            self.assertTrue(await self.e.is_migration_done(status))

    async def test_iterate_success_target_fallback_mode(self):
        routers = [self._mock_router() for _ in range(3)]
        router_id_lookup = {router.id: router for router in routers}
        routers_for_agent = {}
        src_agent = self.create_agent_with_routers(
            routers_for_agent, routers, 3
        )
        pending_router = routers[-1]

        dst_agent1 = self.create_agent_with_routers(
            routers_for_agent, [pending_router], 1
        )
        dst_agent2 = self.create_agent_with_routers(
            routers_for_agent, [pending_router], 1
        )

        with contextlib.ExitStack() as stack:
            self.e.handle_non_migrated_routers_as_unhandleable = False
            stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_connect",
                    new=self.connect_mock,
                )
            )
            get_agent = stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_get_agent",
                )
            )
            get_agent.return_value = src_agent
            collect_agents_and_routers = stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_collect_agents_and_routers",
                )
            )
            collect_agents_and_routers.return_value = routers_for_agent
            stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.resources.openstack."
                    "raise_response_error_if_any",
                )
            )
            disable_network_agent = stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.resources.openstack."
                    "disable_network_agent",
                )
            )
            disable_network_agent.return_value = None

            router_is_not_active_on_agent = stack.enter_context(
                unittest.mock.patch.object(
                    self.e,
                    "_router_is_not_active_on_agent",
                )
            )

            router_is_not_active_on_agent.side_effect = [
                eviction.EvictResult.PENDING,
                eviction.EvictResult.PENDING,
                eviction.EvictResult.MIGRATED,
            ]

            self.os_connection.network.find_router.side_effect = (
                lambda router_id: router_id_lookup[router_id]
            )

            status = await self.e.iterate()
            self.assertEqual(status.migrated_router, 2)
            self.assertEqual(status.pending_router, 1)
            self.assertEqual(status.unhandleable_router, 0)
            self.assertFalse(status.unhandleable_agent)
            self.assertEqual(len(routers_for_agent[dst_agent1]), 2)
            self.assertEqual(len(routers_for_agent[dst_agent2]), 2)
            disable_network_agent.assert_called_once_with(
                self.os_connection.network, src_agent.agent
            )
            src_agent.agent.is_admin_state_up = False

            # as the code pops the src_agent from the dict we need to add
            # it again
            routers_for_agent[src_agent] = set([pending_router.id])
            status = await self.e.iterate()
            self.assertEqual(status.pending_router, 1)

            routers_for_agent[src_agent] = set([pending_router.id])
            status = await self.e.iterate()
            self.assertEqual(status.migrated_router, 1)
            self.assertEqual(status.pending_router, 0)
            self.assertEqual(status.unhandleable_router, 0)
            self.assertFalse(status.unhandleable_agent)
            self.assertFalse(self.e._recheck)
            self.assertTrue(await self.e.is_migration_done(status))

    async def test_post_status_updates_custom_resource_status(self):
        status = unittest.mock.Mock()
        status.as_json_object.return_value = {"foo": "bar"}

        with contextlib.ExitStack() as stack:
            update_status = stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.customresource.update_status",
                )
            )
            await self.e.post_status(status)

        update_status.assert_awaited_once_with(
            self.intf,
            NAMESPACE,
            NAME,
            conditions=unittest.mock.ANY,
        )
        _, _, kwargs = update_status.mock_calls[0]
        conditions = kwargs["conditions"]
        self.assertCountEqual(
            conditions,
            [
                customresource.ConditionUpdate(
                    type_=resource.ResourceCondition.EVICTED.value,
                    status=resource.ResourceEvictingStatus.EVICTING.value,
                    reason="some-reason",
                    message='{"foo": "bar"}',
                ),
                customresource.ConditionUpdate(
                    type_=resource.ResourceCondition.ENABLED.value,
                    status="False",
                    reason="some-reason",
                    message="Eviction in progress",
                ),
            ],
        )

    async def test_is_migration_done_recheck(self):
        status = eviction.EvictionStatus(
            migrated_router=0,
            pending_router=0,
            unhandleable_router=0,
            unhandleable_agent=False,
        )
        self.e._recheck = True
        self.assertFalse(await self.e.is_migration_done(status))
        self.e._recheck = False
        self.assertTrue(await self.e.is_migration_done(status))

    @ddt.data(
        eviction.EvictionStatus(
            migrated_router=0,
            pending_router=0,
            unhandleable_router=0,
            unhandleable_agent=True,
        ),
        eviction.EvictionStatus(
            migrated_router=0,
            pending_router=0,
            unhandleable_router=1,
            unhandleable_agent=False,
        ),
        eviction.EvictionStatus(
            migrated_router=0,
            pending_router=1,
            unhandleable_router=0,
            unhandleable_agent=False,
        ),
    )
    async def test_is_migration_done_false(self, status):
        self.e._recheck = False
        self.assertFalse(await self.e.is_migration_done(status))
