#
# Copyright (c) 2022 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest.mock
import uuid
import os

import yaook.op.common as op_common
import yaook.op.neutron_ovn_bgp as neutron_ovn_bgp
import yaook.statemachine as sm
import yaook.statemachine.resources.openstack as resource
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces

import kubernetes_asyncio.client as kclient

from ... import testutils


NAMESPACE = "test-namespace"
NODE_NAME = "node2.test"
CONFIG_KEY = "testbgp"
NAME = NODE_NAME + "." + CONFIG_KEY
CONFIG_FILE_NAME = "neutron.conf"
CONFIG_PATH = "/etc/neutron"
IMAGE_KEY = "YAOOK_NEUTRON_OVN_BGP_AGENT_OP_JOB_IMAGE"
LOCAL_AS = 1111
PEER_AS = 1234
PEER_IP = "2.2.2.2"
POLICY_FILE_NAME = "policy.json"
POLICY_RULE_KEY = "context_is_admin"
POLICY_RULE_VALUE = "role:admin"
NEUTRON_POLICY = {"policy": {POLICY_RULE_KEY: POLICY_RULE_VALUE}}


class TestNeutronOVNBGPAgentSTS(testutils.CustomResourceTestCase):
    # Needs to be disabled as the neutron-bgp-operator relies on SSA to
    # make patches to its own resource. Since our testutils do not implement
    # that this test can not pass.
    run_needs_update_test = False
    run_topology_spread_test = False
    # ovn-bgp-agent is able to reload the cert life, so no reload container
    # needed
    run_service_reload_test = False

    def _get_neutron_ovn_bgp_sts_yaml(self):
        return {
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
                "generation": 33,
            },
            "spec": {
                "hostname": NODE_NAME,
                "addressScopes": ["1234", "567"],
                "lockName": "bgp-" + CONFIG_KEY,
                "driver": "ovn_stretched_l2_bgp_driver",
                "bgpNodeAnnotationSuffix": CONFIG_KEY,
                "bridgeName": "br-test",
                "localAS": LOCAL_AS,
                "issuerRef": {
                    "name": "ca-issuer"
                },
                "peers": {
                    "switchA": {
                        "IP": PEER_IP,
                        "AS": PEER_AS,
                    },
                },
                "syncInterval": 64,
                "resources": testutils.generate_resources_dict(
                    "ovn-bgp-agent",
                    "frr-bgpd",
                    "frr-zebra",
                ),
            },
            "status": {
                "state": "Creating",
            },
        }

    def setUp(self):
        super().setUp()
        self._env_backup = os.environ.pop(
            IMAGE_KEY, None
        )
        os.environ[IMAGE_KEY] = "dummy-image"
        self.scheduling_key = \
            op_common.SchedulingKey.NETWORK_NEUTRON_OVN_AGENT.value

        self.bgp_status_mock = unittest.mock.Mock([])
        self.bgp_status_mock.return_value = resource.ResourceStatus(
            up=True,
            enabled=True,
            disable_reason=None,
        )

        self.__l2lock_patches = [
            unittest.mock.patch(
                "yaook.op.neutron_ovn_bgp.resources.OVNBGPLock.reconcile",
            ),
        ]

        self.start_l2lock_patches()

    def start_l2lock_patches(self):
        for p in self.__l2lock_patches:
            p.start()

    def stop_l2lock_patches(self):
        for p in self.__l2lock_patches:
            p.stop()

    def tearDown(self):
        self.stop_l2lock_patches()
        if self._env_backup is not None:
            os.environ[IMAGE_KEY] = \
                self._env_backup
        else:
            del os.environ[IMAGE_KEY]
        super().tearDown()

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.labels_bgp = {
            self.scheduling_key: str(uuid.uuid4()),
            context.LABEL_L2_REQUIRE_MIGRATION: 'False'
        }
        self.default_node_setup = {
            "node1.test": {},
            NODE_NAME: self.labels_bgp,
        }
        self.bgp_nodes = [NODE_NAME]
        self._configure_cr(
            neutron_ovn_bgp.NeutronOVNBGPAgent,
            self._get_neutron_ovn_bgp_sts_yaml(),
        )

        self._mock_labelled_nodes(self.default_node_setup, annotations={
            context.ANNOTATION_BGP_INTERFACE_IP + CONFIG_KEY: '10.2.4.42/24',
            context.ANNOTATION_BGP_ROUTER_ID + CONFIG_KEY: '1.1.1.1',
        })

    async def test_bgp_stateful_set_pinned_to_node(self):
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulset = sm.stateful_set_interface(self.api_client)

        bgp_sfs, = await statefulset.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "bgp_agent"},
        )

        self.assertEqual(
            bgp_sfs.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.
            node_selector_terms[0].match_fields[0].to_dict(),
            {
                "key": "metadata.name",
                "operator": "In",
                "values": [NODE_NAME],
            },
        )

    async def test_creates_service(self):
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service, = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "bgp_service"}
        )

        self.assertIsNotNone(service)

    async def test_create_bgp_sfs(self):
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        sfs_interface = interfaces.stateful_set_interface(self.api_client)

        sfs, = await sfs_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "bgp_agent",
            },
        )

        self.assertEqual(
            sfs.spec.selector.match_labels["state.yaook.cloud/parent-name"],
            NAME,
        )

    async def test_configures_bgp(self):
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        parent_uid = self.ctx.parent_uid[:8]

        ovn_bgp_agent_conf = '\n'.join([
            '[DEFAULT]',
            'address_scopes=1234,567',
            'driver=ovn_stretched_l2_bgp_driver',
            'bgp_router_id=1.1.1.1',
            f'bgp_vrf=vrf-{parent_uid}',
            f'bgp_nic=vrfnic-{parent_uid}',
            'bgp_AS=1111',
            'ovsdb_connection=unix:/run/openvswitch/db.sock',
            'ovn_sb_private_key=/etc/ssl/private/tls.key',
            'ovn_sb_certificate=/etc/ssl/private/tls.crt',
            'ovn_sb_ca_cert=/etc/ssl/private/ca.crt',
            'reconcile_interval=64',
            ''
        ])

        bgp_interface = 'bgp-' + self.ctx.parent_uid[:11]

        bridge_interface_mapping = '\n'.join([
            'OVS_BRIDGENAME="br-test"',
            f'BGP_INTERFACE="{ bgp_interface }"',
            'BGP_LOCAL_PEER_IP="10.2.4.42/24"',
            ''
        ])

        frr_conf = '\n'.join([
            'log stdout debugging',
            'frr version 8.1',
            'frr defaults traditional',
            'hostname node2.test',
            'no ipv6 forwarding',
            'no ip forwarding',
            '!',
            'debug bgp neighbor-events',
            'debug bgp updates',
            '!',
            f'router bgp { LOCAL_AS }',
            ' bgp router-id 1.1.1.1',
            ' neighbor pg peer-group',
            f' neighbor { PEER_IP } remote-as { PEER_AS }',
            f' neighbor { PEER_IP } peer-group pg',
            f' neighbor { PEER_IP } disable-connected-check',
            ' address-family ipv6 unicast',
            '  redistribute kernel',
            '  neighbor pg activate',
            '  neighbor pg route-map IMPORT in',
            '  neighbor pg route-map EXPORT out',
            ' exit-address-family',
            ' !',
            ' address-family ipv4 unicast',
            '  redistribute kernel',
            '  neighbor pg activate',
            '  neighbor pg route-map IMPORT in',
            '  neighbor pg route-map EXPORT out',
            ' exit-address-family',
            ' !',
            'route-map EXPORT deny 100',
            '!',
            'route-map EXPORT permit 1',
            f'  match interface vrfnic-{parent_uid}',
            '!',
            'route-map IMPORT deny 1',
            '!',
            'line vty',
            '!',
        ])

        configmap_interface = interfaces.config_map_interface(self.api_client)

        conf, = await configmap_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "bgp_config",
            },
        )

        self.assertEqual(
            conf.data["ovn-bgp-agent.conf"],
            ovn_bgp_agent_conf
        )
        self.assertEqual(
            conf.data["bridge-interface-mapping"],
            bridge_interface_mapping
        )
        self.assertEqual(
            conf.data["frr.conf"],
            frr_conf
        )

    async def test_update(self):
        self._make_all_certificates_succeed_immediately()
        agent_interface = \
            interfaces.neutron_ovn_bgp_agent_interface(self.api_client)
        agent = await agent_interface.list_(
            NAMESPACE,
        )
        await self.cr.sm.ensure(self.ctx)
        agent, = await agent_interface.list_(
            NAMESPACE,
        )
        self.assertEqual(agent['status']['state'], 'Creating')

    async def test_lockl2_set_annotation(self):
        node_labels = self.default_node_setup
        for node in node_labels.keys():
            node_labels[node][context.LABEL_L2_REQUIRE_MIGRATION] = \
                'False'
        self._make_all_dependencies_complete_immediately()
        self.stop_l2lock_patches()
        await self.cr.sm.ensure(self.ctx)

        v1 = kclient.CoreV1Api(self.api_client)
        nodes = await v1.list_node(
            label_selector=self.scheduling_key
        )

        self.assertEqual(
            nodes.items[0].metadata.annotations,
            {
                context.ANNOTATION_BGP_INTERFACE_IP + CONFIG_KEY:
                    '10.2.4.42/24',
                sm.context.ANNOTATION_BGP_ROUTER_ID + CONFIG_KEY:
                    '1.1.1.1',
                context.ANNOTATION_L2_MIGRATION_LOCK
                + self.ctx.parent_spec["lockName"]: '',
            }
        )
        self.assertTrue(
            (nodes.items[0].metadata.labels or {}).get(
                context.LABEL_L2_REQUIRE_MIGRATION, 'True'
            ) == 'False'
        )
        self.start_l2lock_patches()

    async def test_lockl2_annotation_removed_at_delete(self):
        node_labels = self.default_node_setup
        self._make_all_dependencies_complete_immediately()
        self.stop_l2lock_patches()
        await self.cr.sm.ensure(self.ctx)

        v1 = kclient.CoreV1Api(self.api_client)
        nodes = await v1.list_node(
            label_selector=self.scheduling_key
        )

        self.assertEqual(
            nodes.items[0].metadata.annotations,
            {
                context.ANNOTATION_BGP_INTERFACE_IP + CONFIG_KEY:
                    '10.2.4.42/24',
                sm.context.ANNOTATION_BGP_ROUTER_ID + CONFIG_KEY:
                    '1.1.1.1',
                context.ANNOTATION_L2_MIGRATION_LOCK
                + self.ctx.parent_spec["lockName"]: '',
            }
        )
        self.assertTrue(
            (nodes.items[0].metadata.labels or {}).get(
                context.LABEL_L2_REQUIRE_MIGRATION, 'True'
            ) == 'False'
        )

        for node in node_labels.keys():
            node_labels[node][context.LABEL_L2_REQUIRE_MIGRATION] = \
                'True'
        self._mock_labelled_nodes(node_labels, annotations={
            context.ANNOTATION_BGP_INTERFACE_IP + CONFIG_KEY: '10.2.4.42/24',
            context.ANNOTATION_BGP_ROUTER_ID + CONFIG_KEY: '1.1.1.1',
        })

        ovn_bgp_agent = self.client_mock.get_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            NAMESPACE, NAME,
        )
        ovn_bgp_agent["metadata"]["deletionTimestamp"] = "1990-01-01"
        agent = self.ctx.parent
        agent['metadata'].update(ovn_bgp_agent['metadata'])
        self.client_mock.put_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            NAMESPACE, NAME, agent,
        )
        self.ctx = self._make_context()  # needed to set the updated spec

        await self.cr.sm.ensure(self.ctx)

        nodes = await v1.list_node(
            label_selector=self.scheduling_key
        )

        self.assertEqual(
            nodes.items[0].metadata.annotations,
            {
                context.ANNOTATION_BGP_INTERFACE_IP + CONFIG_KEY:
                    '10.2.4.42/24',
                sm.context.ANNOTATION_BGP_ROUTER_ID + CONFIG_KEY:
                    '1.1.1.1',
            }
        )
        self.assertTrue(
            context.ANNOTATION_L2_MIGRATION_LOCK
            + self.ctx.parent_spec["lockName"] not in
            nodes.items[0].metadata.annotations)
        self.assertTrue(
            (nodes.items[0].metadata.labels or {}).get(
                context.LABEL_L2_REQUIRE_MIGRATION, 'True'
            ) == 'True'
        )

        self.start_l2lock_patches()

    async def test_creates_containers_with_resources(self):
        node = self.client_mock.get_object("", "v1", "nodes", None, NODE_NAME)
        node["metadata"]["deletionTimestamp"] = "1990-01-01"
        self.client_mock.put_object("", "v1", "nodes", None, NODE_NAME, node)

        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        statefulsets = interfaces.stateful_set_interface(self.api_client)

        bgp_sts, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "bgp_agent"}
        )

        self.assertEqual(
            testutils.container_resources(bgp_sts, 0),
            testutils.unique_resources("ovn-bgp-agent")
        )
        self.assertEqual(
            testutils.container_resources(bgp_sts, 1),
            testutils.unique_resources("frr-bgpd")
        )
        self.assertEqual(
            testutils.container_resources(bgp_sts, 2),
            testutils.unique_resources("frr-zebra")
        )
