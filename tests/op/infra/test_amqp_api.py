#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import unittest
import unittest.mock

from unittest.mock import sentinel

import kubernetes_asyncio.client as kclient

import yaook.common.config
import yaook.op.infra.amqp_cr as amqp_cr
import yaook.statemachine as sm

from ... import testutils


NAME = "amqp-server-xyz"
NAMESPACE = "infra"


class TestRabbitMQK8sDomainLayer(unittest.IsolatedAsyncioTestCase):

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test_get_layer_inserts_correct_domain(self,
                                                    get_cluster_domain):
        get_cluster_domain.return_value = str(sentinel.domain_name)
        ctx = unittest.mock.Mock()

        layer = amqp_cr.RabbitMQK8sDomainLayer()

        self.assertEqual(
            await layer.get_layer(ctx),
            {"rabbitmq": yaook.common.config.CUTTLEFISH.declare([{
                "cluster_formation": {
                    "k8s.host":
                        f"kubernetes.default.svc.{sentinel.domain_name}",
                },
            }])},)


class TestAMQPServer(testutils.CustomResourceTestCase):
    run_needs_update_test = False

    def tearDown(self):
        for patch in self.__patches:
            patch.stop()
        super().tearDown()

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._configure_cr(
            amqp_cr.AMQPServer,
            {
                "apiVersion": "infra.yaook.cloud/v1",
                "kind": "AMQPServer",
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "replicas": 3,
                    "frontendIssuerRef": {
                        "name": "frontend-issuer",
                    },
                    "backendCAIssuerRef": {
                        "name": "backend-ca-issuer",
                    },
                    "imageRef": "example.com/rabbitmq:1.2.3",
                    "storageSize": "32Gi",
                    "storageClassName": "foobar",
                    "resources": testutils.generate_resources_dict(
                        "rabbitmq",
                    ),
                },
            }
        )
        self.run_podspec_tests = False
        self.update = unittest.mock.AsyncMock()
        self.__patches = [
            unittest.mock.patch.object(
                self.cr.amqp_policies, "update",
                new=self.update,
            )
        ]
        for patch in self.__patches:
            patch.start()

    async def test_rejects_bitnami_image(self):
        curr = self.client_mock.get_object(
            "infra.yaook.cloud", "v1", "amqpservers",
            NAMESPACE, NAME,
        )
        curr["spec"]["imageRef"] = "bitnami/foo"
        self.client_mock.put_object(
            "infra.yaook.cloud", "v1", "amqpservers",
            NAMESPACE, NAME,
            curr,
        )

        ctx = self._make_context()
        await self.cr.reconcile(ctx, 0)
        curr = self.client_mock.get_object(
            "infra.yaook.cloud", "v1", "amqpservers",
            NAMESPACE, NAME,
        )
        condition = [
            cond
            for cond in curr["status"]["conditions"]
            if cond["type"] == "Converged"
        ][0]
        self.assertEqual(condition["reason"], "ConfigurationInvalid")

    async def test_creates_service_for_ready_endpoints_matching_statefulset(self):  # noqa:E501
        self._make_all_certificates_succeed_immediately()
        self._make_all_issuers_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = sm.service_interface(self.api_client)
        statefulsets = sm.stateful_set_interface(self.api_client)

        mq_sts, = await statefulsets.list_(NAMESPACE)

        ready_service, = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "ready_service",
            },
        )

        self.assertFalse(
            ready_service.spec.publish_not_ready_addresses,
        )
        self.assertDictEqual(
            ready_service.spec.selector,
            mq_sts.spec.template.metadata.labels,
        )

    async def test_creates_public_for_ready_endpoints_matching_statefulset(self):  # noqa:E501
        self._make_all_certificates_succeed_immediately()
        self._make_all_issuers_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = sm.service_interface(self.api_client)
        statefulsets = sm.stateful_set_interface(self.api_client)

        mq_sts, = await statefulsets.list_(NAMESPACE)

        ready_service, = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "public_service",
            },
        )

        self.assertFalse(
            ready_service.spec.publish_not_ready_addresses,
        )
        self.assertDictEqual(
            ready_service.spec.selector,
            mq_sts.spec.template.metadata.labels,
        )

    async def test_creates_frontend_certificate(self):
        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "frontend_certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "frontend_certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            "frontend-issuer",
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )

    async def test_creates_replication_ca_certificate(self):
        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "replication_ca_certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    "replication_ca_certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            "backend-ca-issuer",
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )
        self.assertTrue(cert["spec"]["isCA"])

    async def test_creates_replication_ca_issuer(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_issuers_ready_immediately()
        await self.cr.sm.ensure(self.ctx)
        issuers = sm.issuer_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        issuer, = await issuers.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "replication_ca",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    "replication_ca_certificate_secret",
            }
        )

        self.assertEqual(
            issuer["spec"]["ca"]["secretName"],
            cert_secret.metadata.name,
        )

    async def test_creates_replication_certificate_with_own_ca(self):
        self._make_all_issuers_ready_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        issuers = sm.issuer_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        issuer, = await issuers.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "replication_ca",
            }
        )
        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "replication_certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    "replication_certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            issuer["metadata"]["name"],
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )
        self.assertFalse(cert["spec"].get("isCA", False))

    async def test_creates_mq_statefulset(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_issuers_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)
        services = sm.service_interface(self.api_client)

        ready_service, = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "ready_service",
            },
        )

        mq, = await statefulsets.list_(NAMESPACE)

        rabbitmq_container = mq.spec.template.spec.containers[0]

        simple_env_map = {
            envdef.name: envdef.value
            for envdef in rabbitmq_container.env
            if envdef.value is not None
        }

        self.assertEqual(
            rabbitmq_container.image,
            "example.com/rabbitmq:1.2.3",
        )
        self.assertEqual(
            rabbitmq_container.resources.to_dict(),
            testutils.unique_resources("rabbitmq"),
        )
        self.assertEqual(
            simple_env_map["K8S_SERVICE_NAME"],
            ready_service.metadata.name,
        )
        self.assertEqual(
            simple_env_map["YAOOK_CLUSTER_DOMAIN"],
            "test.dns.domain",
        )
        self.assertEqual(
            simple_env_map["RABBITMQ_NODE_NAME"],
            "rabbit@$(MY_POD_NAME).$(K8S_SERVICE_NAME)."
            "$(MY_POD_NAMESPACE).svc.$(YAOOK_CLUSTER_DOMAIN)",
        )
        self.assertEqual(
            simple_env_map["RABBITMQ_DEFAULT_USER"],
            "yaook-sys-maint",
        )

        volume_claim_template, = mq.spec.volume_claim_templates
        self.assertEqual(
            volume_claim_template.spec.storage_class_name,
            "foobar",
        )
        self.assertEqual(
            volume_claim_template.spec.resources.requests["storage"],
            "32Gi",
        )

    async def test_amqp_statefulset_mounts_certs_and_config(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_issuers_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        frontend_cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "frontend_certificate_secret",
            },
        )
        replication_cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "replication_certificate_secret",
            },
        )

        mq, = await statefulsets.list_(NAMESPACE)

        cert_mountpoint = testutils.find_volume_mountpoint(
            mq.spec.template.spec,
            "certificates",
            "rabbitmq",
        )

        self.assertEqual(cert_mountpoint, "/etc/rabbitmq/certs")

    async def test_amqp_statefulset_sets_ca_cert_expiry(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        certificates = sm.certificates_interface(self.api_client)
        statefulsets = sm.stateful_set_interface(self.api_client)

        replication_ca_cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "replication_ca_certificate",
            },
        )
        amqp, = await statefulsets.list_(NAMESPACE)

        annotations = amqp.spec.template.metadata.annotations

        self.assertEqual(
            replication_ca_cert["status"]["notBefore"],
            annotations["operator.yaook.cloud/replication-ca-cert-not-before"],
        )

    async def test_service_monitor_references_service_and_secret(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_issuers_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        servicemonitors = sm.servicemonitor_interface(self.api_client)
        services = sm.service_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        sms, = await servicemonitors.list_(NAMESPACE)
        ready_service, = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "ready_service",
            },
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "frontend_certificate_secret",
            }
        )

        servicelabels = sms["spec"]["selector"]["matchLabels"]
        certificatename = (sms["spec"]["endpoints"][0]["tlsConfig"]
                           ["ca"]["secret"]["name"])

        self.assertEqual(
            "prometheus",
            sms["spec"]["endpoints"][0]["port"]
        )

        self.assertEqual(
            servicelabels,
            ready_service.metadata.labels
        )

        self.assertEqual(
            certificatename,
            cert_secret.metadata.name
        )

    async def test_amqp_config_map_tls_verfication_is_enabled(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        configmaps = sm.config_map_interface(self.api_client)

        amqp_inter_node_tls_config, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "amqp_inter_node_tls_config",
            },
        )

        self.assertIn('secure_renegotiate, true',
                      amqp_inter_node_tls_config.data['inter_node_tls.config'])
        self.assertIn('verify, verify_peer',
                      amqp_inter_node_tls_config.data['inter_node_tls.config'])
        self.assertIn('fail_if_no_peer_cert, true',
                      amqp_inter_node_tls_config.data['inter_node_tls.config'])
        self.assertNotIn('secure_renegotiate, false',
                         amqp_inter_node_tls_config.data[
                             'inter_node_tls.config'
                         ])
        self.assertNotIn('verify, verify_none',
                         amqp_inter_node_tls_config.data[
                             'inter_node_tls.config'
                         ])
        self.assertNotIn('fail_if_no_peer_cert, false',
                         amqp_inter_node_tls_config.data[
                             'inter_node_tls.config'
                         ])


class TestGeneratedAmpqServiceMonitor(unittest.IsolatedAsyncioTestCase):
    def test___init___declares_dependencies(self):
        gsms = amqp_cr.GeneratedAmpqServiceMonitor(
            "name", sentinel.service,
            sentinel.certificate, ["endpoint"])

        self.assertIn(sentinel.service, gsms._dependencies)

    async def test__make_body_exclude_memory_allocation(self):
        service = unittest.mock.Mock()
        service.get = unittest.mock.AsyncMock()
        service.get.return_value = kclient.V1ObjectReference(
            name="servicename")
        sentinel.ctx.parent_spec = unittest.mock.Mock()
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "serviceMonitor": {
                "additionalLabels": {
                    "foo": "bar"
                    }
                }
            }

        service.labels = unittest.mock.Mock()
        service.labels.return_value = ["labela", "labelb"]
        certificate = unittest.mock.Mock()
        certificate.get = unittest.mock.AsyncMock()
        certificate.get.return_value = kclient.V1ObjectReference(
            name="certificatename")

        gssm = amqp_cr.GeneratedAmpqServiceMonitor(
            "name",
            service,
            certificate,
            ["endpointname"])

        self.assertEqual(
            {
                "apiVersion": "monitoring.coreos.com/v1",
                "kind": "ServiceMonitor",
                "metadata": {
                    "labels": {
                        "foo": "bar"
                    },
                    "name": "name",
                },
                "spec": {
                    "selector": {
                        "matchLabels": [
                            "labela",
                            "labelb"
                        ]
                    },
                    "endpoints": [
                        {
                            "port": "endpointname",
                            "scheme": "https",
                            "tlsConfig": {
                                "ca": {
                                    "secret": {
                                        "name": "certificatename",
                                        "key": "tls.crt",
                                    },
                                },
                                "serverName": "servicename",
                            },
                            "relabelings": [
                                {
                                    "action": "replace",
                                    "separator": ':',
                                    "sourceLabels": [
                                        "__meta_kubernetes_pod_name",
                                        "__meta_kubernetes_pod_container"
                                        "_port_number",
                                    ],
                                    "targetLabel": "instance",
                                },
                            ],
                            "metricRelabelings": [
                                {
                                    "action": "drop",
                                    "regex": "erlang_vm_allocators",
                                    "sourceLabels": [
                                        "__name__",
                                    ],
                                }
                            ]
                        }
                    ],
                },
            },
            await gssm._make_body(ctx, sentinel.deps)
        )


class TestAMQPServerDisableInterNodeTls(testutils.CustomResourceTestCase):
    run_needs_update_test = False

    def tearDown(self):
        for patch in self.__patches:
            patch.stop()
        super().tearDown()

    async def asyncSetUp(self):
        await super().asyncSetUp()

        self._env_backup = os.environ.pop(
            "YAOOK_INFRA_OP_AMQP_INTER_NODE_TLS_VERIFICATION", None
        )
        os.environ["YAOOK_INFRA_OP_AMQP_INTER_NODE_TLS_VERIFICATION"] = "false"

        self._configure_cr(
            amqp_cr.AMQPServer,
            {
                "apiVersion": "infra.yaook.cloud/v1",
                "kind": "AMQPServer",
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "replicas": 3,
                    "frontendIssuerRef": {
                        "name": "frontend-issuer",
                    },
                    "backendCAIssuerRef": {
                        "name": "backend-ca-issuer",
                    },
                    "imageRef": "example.com/rabbitmq:1.2.3",
                    "storageSize": "32Gi",
                    "storageClassName": "foobar",
                },
            }
        )
        self.run_podspec_tests = False
        self.update = unittest.mock.AsyncMock()
        self.__patches = [
            unittest.mock.patch.object(
                self.cr.amqp_policies, "update",
                new=self.update,
            )
        ]
        for patch in self.__patches:
            patch.start()

    async def test_amqp_config_map_tls_verfication_is_disabled(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        configmaps = sm.config_map_interface(self.api_client)

        amqp_inter_node_tls_config, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "amqp_inter_node_tls_config",
            },
        )

        self.assertIn('secure_renegotiate, false',
                      amqp_inter_node_tls_config.data['inter_node_tls.config'])
        self.assertIn('verify, verify_none',
                      amqp_inter_node_tls_config.data['inter_node_tls.config'])
        self.assertIn('fail_if_no_peer_cert, false',
                      amqp_inter_node_tls_config.data['inter_node_tls.config'])
        self.assertNotIn('secure_renegotiate, true',
                         amqp_inter_node_tls_config.data[
                             'inter_node_tls.config'
                         ])
        self.assertNotIn('verify, verify_peer',
                         amqp_inter_node_tls_config.data[
                             'inter_node_tls.config'
                         ])
        self.assertNotIn('fail_if_no_peer_cert, true',
                         amqp_inter_node_tls_config.data[
                             'inter_node_tls.config'
                         ])
