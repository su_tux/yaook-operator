#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import unittest
import unittest.mock
import uuid
import openstack
import openstack.exceptions
# TODO(resource-refactor): clean up imports
import yaook.statemachine.resources.openstack as resource
import yaook.op.neutron_bgp.eviction as eviction
import yaook.statemachine.customresource as customresource
NAMESPACE = f"namespace-{uuid.uuid4()}"
NAME = f"name-{uuid.uuid4()}"


def repeatable_generator(iterable):
    def generator(*args, **kwargs):
        yield from iterable
    return generator


class TestBGPDRAgentInfo(unittest.TestCase):
    def test_get_raises_lookup_error_if_not_found(self):
        client = unittest.mock.Mock(["agents"])
        client.agents.return_value = []
        with self.assertRaisesRegex(
                LookupError,
                "correct on correct not found"):
            resource.get_network_agent(client,
                                       host="correct",
                                       binary="correct",
                                       )

    def test_disable_invokes_api(self):
        client = unittest.mock.Mock(["put"])
        agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
        agent.id = unittest.mock.sentinel.agent_id
        with contextlib.ExitStack() as stack:
            raise_ = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.openstack.raise_response_error_if_any",  # noqa E501
            ))
            resource.disable_network_agent(client, agent)
        client.put.assert_called_once_with(
            f"/agents/{agent.id}",
            json={
                "agent": {
                    "admin_state_up": False,
                },
            },
        )
        raise_.assert_called_once_with(client.put())


class TestEvictor(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.intf = unittest.mock.Mock(
            "yaook.statemachine.interfaces.ResourceInterfaceWithStatus",
        )
        self.connection_info = {
            "auth": unittest.mock.sentinel.auth,
            "interface": unittest.mock.sentinel.interface,
        }
        self.e = eviction.Evictor(
            interface=self.intf,
            namespace=NAMESPACE,
            node_name=NAME,
            connection_info=self.connection_info,
            reason="some-reason",
        )
        self.os_connection = unittest.mock.Mock()
        self.agent = unittest.mock.Mock(openstack.network.v2.agent.Agent)
        self.agent.id = unittest.mock.sentinel.agent_id
        self.agent.host = NAME
        self.agent.binary = "neutron-bgp-dragent"
        self.agent.is_alive = True
        self.agent.is_admin_state_up = True
        self.connect_mock = unittest.mock.Mock([])
        self.connect_mock.return_value = self.os_connection
        self.get_network_agent_mock = unittest.mock.Mock([])
        self.get_network_agent_mock.return_value = self.agent
        self.disable_network_agent_mock = unittest.mock.Mock([])

        self.__iterate_patches = [
            unittest.mock.patch.object(self.e, "_connect",
                                       new=self.connect_mock),
            unittest.mock.patch(
                "yaook.statemachine.resources.openstack.get_network_agent",
                new=self.get_network_agent_mock,
            ),
            unittest.mock.patch(
                "yaook.statemachine.resources.openstack.disable_network_agent",
                new=self.disable_network_agent_mock,
            ),
        ]
        self.__patches = []

    def tearDown(self):
        for p in self.__patches:
            p.stop()

    def _patch_iterate(self):
        for p in self.__iterate_patches:
            p.start()
            self.__patches.append(p)

    def test__connect(self):
        with contextlib.ExitStack() as stack:
            connect = stack.enter_context(unittest.mock.patch(
                "openstack.connect",
            ))
            result = self.e._connect()
        connect.assert_called_once_with(
            auth=unittest.mock.sentinel.auth,
            interface=unittest.mock.sentinel.interface,
        )
        self.assertEqual(result, connect())

    def test__os_iterate_disables_network_agent(self):
        self._patch_iterate()
        self.e._os_iterate()
        self.connect_mock.assert_called_once_with()
        self.get_network_agent_mock.assert_called_once_with(
            self.os_connection.network,
            host=NAME,
            binary="neutron-bgp-dragent",
        )
        self.disable_network_agent_mock.assert_called_once_with(
            self.os_connection.network,
            self.agent,
        )

    async def test_iterate_runs_os_iterate_in_executor(self):
        loop = unittest.mock.Mock([])
        loop.run_in_executor = unittest.mock.AsyncMock()
        loop.run_in_executor.return_value = unittest.mock.sentinel.result
        with contextlib.ExitStack() as stack:
            get_event_loop = stack.enter_context(unittest.mock.patch(
                "asyncio.get_event_loop",
            ))
            get_event_loop.return_value = loop
            _os_iterate = stack.enter_context(unittest.mock.patch.object(
                self.e, "_os_iterate",
            ))
            result = await self.e.iterate()
        self.assertEqual(result, unittest.mock.sentinel.result)
        loop.run_in_executor.assert_awaited_once_with(
            None,
            _os_iterate,
        )
        get_event_loop.assert_called_once_with()

    async def test_post_status_updates_custom_resource_status(self):
        status = unittest.mock.Mock()
        status.as_json_object.return_value = {"foo": "bar"}
        with contextlib.ExitStack() as stack:
            update_status = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.customresource.update_status",
            ))
            await self.e.post_status(status)
        update_status.assert_awaited_once_with(
            self.intf,
            NAMESPACE,
            NAME,
            conditions=unittest.mock.ANY,
        )
        _, _, kwargs = update_status.mock_calls[0]
        conditions = kwargs["conditions"]
        self.assertCountEqual(
            conditions,
            [
                customresource.ConditionUpdate(
                    type_=resource.ResourceCondition.EVICTED.value,
                    status=resource.ResourceEvictingStatus.EVICTING.
                    value,
                    reason="some-reason",
                    message='{"foo": "bar"}',
                ),
                customresource.ConditionUpdate(
                    type_=resource.ResourceCondition.ENABLED.value,
                    status="False",
                    reason="some-reason",
                    message="Eviction in progress",
                ),
            ],
        )
