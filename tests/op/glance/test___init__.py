#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest
import unittest.mock

import yaook.common.config
import yaook.statemachine as sm
import yaook.op.glance as glance
import kubernetes_asyncio.client as kclient


class TestCephConfigLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.ccl = glance.CephConfigLayer()

    def test_is_cue_layer(self):
        self.assertIsInstance(self.ccl, sm.CueLayer)

    async def test_injects_ceph_config_in_cue(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends":
                {"ceph": {
                    "keyringReference": "glance-client-key",
                    "keyringUsername": "glance",
                    "keyringPoolname": "glance-pool",
                }}
        }

        result = await self.ccl.get_layer(ctx)

        self.assertIsInstance(
            result["glance"].contents[-2]["glance_store"],
            yaook.common.config.CueConfigReference
        )

    async def test_raises_if_multiple_backends(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends":
                {
                    "ceph": {},
                    "file": {},
                    "s3": {}
                }
        }

        with self.assertRaises(sm.ConfigurationInvalid):
            await self.ccl.get_layer(ctx)


class TestFileConfigLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.fcl = glance.FileConfigLayer()

    def test_is_cue_layer(self):
        self.assertIsInstance(self.fcl, sm.CueLayer)

    async def test_injects_file_config_in_cue(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends":
                {"file": {
                    "storageSize": "10G",
                    "storageClassName": "testclass"
                }}
        }

        result = await self.fcl.get_layer(ctx)

        self.assertIsInstance(
            result["glance"].contents[-1]["glance_store"],
            yaook.common.config.CueConfigReference
        )

    async def test_raises_if_multiple_backends(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends":
                {
                    "ceph": {},
                    "file": {},
                    "s3": {}
                }
        }

        with self.assertRaises(sm.ConfigurationInvalid):
            await self.fcl.get_layer(ctx)


class TestS3ConfigLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.s3cl = glance.S3ConfigLayer()

    def test_is_cue_layer(self):
        self.assertIsInstance(self.s3cl, sm.CueLayer)

    @unittest.mock.patch(
        "yaook.statemachine.interfaces.ResourceInterface.read")
    async def test_injects_s3_config_in_cue(self, read):
        credentials = sm.api_utils.encode_secret_data({
            "access": "IAMACCESS",
            "secret": "geheimnis"
        })

        read.return_value = kclient.V1Secret(data=credentials)

        ctx = unittest.mock.Mock(["parent_spec", "api_client", "namespace"])
        ctx.parent_spec = {
            "backends":
                {"s3": {
                    "endpoint": "https://s3.generic.endpoint",
                    "bucket": "tekcub",
                    "addressingStyle": "virtual",
                    "credentialRef": {
                        "name": "creds"
                    }
                }}
        }

        result = await self.s3cl.get_layer(ctx)

        self.assertIsInstance(
            result["glance"].contents[-2]["glance_store"],
            yaook.common.config.CueConfigReference
        )

    async def test_raises_if_multiple_backends(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends":
                {
                    "ceph": {},
                    "file": {},
                    "s3": {}
                }
        }

        with self.assertRaises(sm.ConfigurationInvalid):
            await self.s3cl.get_layer(ctx)


class TestUseCeph(unittest.IsolatedAsyncioTestCase):

    def test_use_ceph(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends":
                {"ceph": {}}}
        self.assertTrue(glance._use_ceph(ctx))

    def test_use_ceph_default(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends": {}}
        self.assertFalse(glance._use_ceph(ctx))


class TestUseFile(unittest.IsolatedAsyncioTestCase):

    def test_use_file(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends":
                {"file": {}}}
        self.assertTrue(glance._use_file(ctx))

    def test_use_file_default(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends": {}}
        self.assertFalse(glance._use_file(ctx))


class TestUseS3(unittest.IsolatedAsyncioTestCase):

    def test_use_s3(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends":
                {"s3": {}}}
        self.assertTrue(glance._use_s3(ctx))

    def test_use_s3_default(self):
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "backends": {}}
        self.assertFalse(glance._use_s3(ctx))
