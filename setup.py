#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from setuptools import setup, find_namespace_packages

setup(
    name="yaook-operators",
    version="0.0.1",
    packages=find_namespace_packages(include=["yaook.*"]),
    install_requires=[
        "kubernetes-asyncio==24.2.3",
        "jsonpatch==1.32",
        "Jinja2==3.1.2",
        "cryptography==40.0.2",
        "environ-config==23.2.0",
        "nose==1.3.7",
        "ddt==1.6.0",
        "openstacksdk==1.2.0",
        "oslo_config==9.1.1",
        "oslo_policy==4.1.1",
        "pyOpenSSL==23.1.1",
        "python-dxf==11.1.1",
        "python-ironicclient==4.10.0",
        "semver==2.13.0",
        "pymysql==1.0.3",
        "pyyaml==6.0",
        "python-novaclient==18.3.0",
        "opentelemetry-api==1.17.0",
        "opentelemetry-sdk==1.17.0",
        "opentelemetry-exporter-jaeger==1.18.0",
        "lru-dict==1.1.8",
    ],
    include_package_data=True,
)
